//#define STOP_RIGHT_AFTER_SCANNING_TETS
//#define CGAL_MESH_3_VERBOSE
#define NOT_PREC_ARITHM

#include <iostream>
#undef NDEBUG//comment it to disable assert checks
#include <assert.h>

#include <vector>
#include <list>

#include "geometry.h"
//#include "use_tetgen.h"

//#include "write_c3t3_to_vtk_xml_file.h"

#include "IO/Complex_3_in_triangulation_3_to_vtk.h"
//this shit is for exporting to vtu file
#include <vtk/vtkVersion.h>
#include <vtk/vtkSmartPointer.h>
//#include <vtk/vtkTetra.h>
//#include <vtk/vtkCellArray.h>
//#include <vtk/vtkXMLUnstructuredGridReader.h>
//#include <vtk/vtkDataSetMapper.h>
//#include <vtk/vtkActor.h>
//#include <vtk/vtkRenderer.h>
//#include <vtk/vtkRenderWindow.h>
//#include <vtk/vtkRenderWindowInteractor.h>
#include <vtk/vtkXMLUnstructuredGridWriter.h>
#include <vtk/vtkUnstructuredGrid.h>
//#include <vtk/vtkPointData.h>
//#include <vtk/vtkVertexGlyphFilter.h>

#include "fixed_danilov_IO.h"

#include <CGAL/exude_mesh_3.h>

#include "histogram.h"

/*
#include <CGAL/Mesh_3/search_for_connected_components_in_labeled_image.h>

template <typename Point>
struct Get_point
{
  const double vx, vy, vz;
  Get_point(const CGAL::Image_3* image)
    : vx(image->vx())
    , vy(image->vy())
    , vz(image->vz())
  {}
  Point operator()(const std::size_t i,
                   const std::size_t j,
                   const std::size_t k) const
  {
    return Point(double(i) * vx, double(j) * vy, double(k) * vz);
  }
};

temporarly saved
void gen_initial_mesh(C3t3 & c3t3, Network & net, const Mesh_domain & domain,
                      const CGAL::Image_3 & image)
{
  
  typedef typename C3t3::Triangulation       Tr;
  typedef typename Tr::Weighted_point        Weighted_point;
  typedef typename Tr::Bare_point            Bare_point;
  typedef typename Tr::Segment               Segment_3;
  typedef typename Tr::Geom_traits::Vector_3 Vector_3;
  typedef typename Tr::Vertex_handle         Vertex_handle;
  typedef typename Tr::Cell_handle           Cell_handle;
  
  Tr& tr = c3t3.triangulation();
  typename Tr::Geom_traits::Construct_point_3 wp2p =
    tr.geom_traits().construct_point_3_object();
  typename Tr::Geom_traits::Construct_weighted_point_3 p2wp =
    tr.geom_traits().construct_weighted_point_3_object();
  const double max_v = (std::max)((std::max)(image.vx(),
                                             image.vy()),
                                  image.vz());
  typedef std::vector<std::pair<Bare_point, std::size_t> > Seeds;
  Seeds seeds;
  Get_point<Bare_point> get_point(&image);
  std::cout << "Searching for connected components..." << std::endl;
  CGAL::Identity<unsigned char> no_transformation;
  search_for_connected_components_in_labeled_image(image,
                                                   std::back_inserter(seeds),
                                                   CGAL::Emptyset_iterator(),
                                                   no_transformation,
                                                   get_point,
                                                   (unsigned char)0);
  std::cout << "  " << seeds.size() << " components were found." << std::endl;
  std::cout << "Construct initial points..." << std::endl;
  for(typename Seeds::const_iterator it = seeds.begin(), end = seeds.end();
      it != end; ++it)
  {
    const double radius = double(it->second + 1)* max_v;
    CGAL::Random_points_on_sphere_3<Bare_point> points_on_sphere_3(radius);
    typename Mesh_domain::Construct_intersection construct_intersection =
      domain.construct_intersection_object();
    std::vector<Vector_3> directions;
    if(it->second < 2) {
      // shoot in six directions
      directions.push_back(Vector_3(-radius, 0, 0));
      directions.push_back(Vector_3(+radius, 0, 0));
      directions.push_back(Vector_3(0, -radius, 0));
      directions.push_back(Vector_3(0, +radius, 0));
      directions.push_back(Vector_3(0, 0, -radius));
      directions.push_back(Vector_3(0, 0, +radius));
    } else {
      for(int i = 0; i < 20; ++i)
      {
        // shoot 20 random directions
        directions.push_back(*points_on_sphere_3++ - CGAL::ORIGIN);
      }
    }
    BOOST_FOREACH(const Vector_3& v, directions)
    {
      const Bare_point test = it->first + v;
      const typename Mesh_domain::Intersection intersect =
        construct_intersection(Segment_3(it->first, test));
      if (CGAL::cpp11::get<2>(intersect) != 0)
      {
        Weighted_point pi = p2wp(CGAL::cpp11::get<0>(intersect));
        std::cout << pi << std::endl;
//  typename Mesh_domain::Is_in_domain is_in_dom =
  //    domain.is_in_domain_object();

        // This would cause trouble to optimizers
        // check pi will not be hidden
        typename Tr::Locate_type lt;
        Cell_handle c;
        int li, lj;
        Cell_handle pi_cell = tr.locate(pi, lt, li, lj);
        if(lt != Tr::OUTSIDE_AFFINE_HULL) {
          switch (tr.dimension())
          { //skip dimension 0
          case 1:
            if (tr.side_of_power_segment(pi_cell, pi, true) != CGAL::ON_BOUNDED_SIDE)
              continue;
            break;
          case 2:
            if (tr.side_of_power_circle(pi_cell, 3, pi, true) != CGAL::ON_BOUNDED_SIDE)
              continue;
            break;
          case 3:
            if (tr.side_of_power_sphere(pi_cell, pi, true) != CGAL::ON_BOUNDED_SIDE)
              continue;
          }
        }
        //check pi is not inside a protecting ball
        std::vector<Vertex_handle> conflict_vertices;
        if (tr.dimension() == 3)
        {
          tr.vertices_on_conflict_zone_boundary(pi, pi_cell
            , std::back_inserter(conflict_vertices));
        }
        else
        {
          for (typename Tr::Finite_vertices_iterator vit = tr.finite_vertices_begin();
               vit != tr.finite_vertices_end(); ++vit)
          {
            if (vit->point().weight() > 0.)
              conflict_vertices.push_back(vit);
          }
        }
        bool pi_inside_protecting_sphere = false;
        BOOST_FOREACH(Vertex_handle cv, conflict_vertices)
        {
          if (cv->point().weight() == 0.)
            continue;
          if (CGAL::compare_squared_distance(pi.point(), wp2p(cv->point()), cv->point().weight())
              != CGAL::LARGER)
          {
            pi_inside_protecting_sphere = true;
            break;
          }
        }
        if (pi_inside_protecting_sphere)
          continue;
        const typename Mesh_domain::Index index = CGAL::cpp11::get<1>(intersect);
        Vertex_handle v = tr.insert(pi);
        // `v` could be null if `pi` is hidden by other vertices of `tr`.
        

        assert(domain.is_in_domain_object().operator()(pi.point()));
CGAL_assertion(v != Vertex_handle());
        c3t3.set_dimension(v, 2); // by construction, points are on surface
        c3t3.set_index(v, index);
      }
    }
  }
  std::cout << "  " << tr.number_of_vertices() << " initial points." << std::endl;
  if ( c3t3.triangulation().dimension() != 3 )
  {
    std::cout << "  not enough points: triangulation.dimension() == "
              << c3t3.triangulation().dimension() << std::endl;
  }  
}
*/

#if 0
void gen_initial_mesh_vessels_points(C3t3 & c3t3, Network & net, const Mesh_domain & domain)
{
  //this version is only for vessels
  typedef typename C3t3::Triangulation       Tr;
  typedef typename Tr::Weighted_point        Weighted_point;
  typedef typename Tr::Bare_point            Bare_point;
  typedef typename Tr::Ray                   Ray_3;
  typedef typename Tr::Geom_traits::Vector_3 Vector_3;
  typedef typename Tr::Vertex_handle         Vertex_handle;
  
  Tr& tr = c3t3.triangulation();
  typename Tr::Geom_traits::Construct_weighted_point_3 p2wp =
    tr.geom_traits().construct_weighted_point_3_object();
  typedef std::vector< std::pair<Bare_point, Plane> > Seeds;
  std::vector<Bare_point> simple_seeds;
  Seeds seeds;
  net.get_seeds(seeds, simple_seeds);

  typename Mesh_domain::Is_in_domain is_in_dom =
      domain.is_in_domain_object();

  for (auto &it : simple_seeds) {
    if (is_in_dom(it)) {
      Vertex_handle v = tr.insert(p2wp(it));
      CGAL_assertion(v != Vertex_handle());
      c3t3.set_dimension(v, 3); // these points are inside vessel domain
      c3t3.set_index(v, *is_in_dom(it));
    } else {
      std::cout << "Failed to detect domain " << it << std::endl;
    }
  }

  for (auto &it : seeds) {
    auto & pp = it.first;
    if (is_in_dom(pp)) {
      Vertex_handle v = tr.insert(p2wp(pp));
      CGAL_assertion(v != Vertex_handle());
      c3t3.set_dimension(v, 3); // these points are inside vessel domain
      c3t3.set_index(v, *is_in_dom(pp));
    } else {
      std::cout << "!!! Failed to detect domain"  << pp << std::endl;
    }
  }

  for (auto &it: seeds) {
//this is main thing
    typename Mesh_domain::Construct_intersection construct_intersection =
      domain.construct_intersection_object();
    Plane & plane = it.second;
    std::vector<Vector_3> directions;

    Vector v1 = plane.base1();
    v1/=sqrt(v1.squared_length());
    Vector v2 = plane.base2();
    v2/=sqrt(v2.squared_length());
    double alpha = 2*M_PI/6;
    double a = 0;
    for (int i = 0; i < 6; i++) {
      directions.push_back(sin(a)*v1 + cos(a)*v2);
      a += alpha;
    }

    for (const Vector_3& vv: directions) {
      const typename Mesh_domain::Intersection intersect =
        construct_intersection(Ray_3(it.first, vv));

      assert(CGAL::cpp11::get<2>(intersect) != 0);
      Weighted_point pi = p2wp(CGAL::cpp11::get<0>(intersect));
      // This would cause trouble to optimizers
      // check pi will not be hidden
      /*
      typename Tr::Locate_type lt;
      Cell_handle c;
      int li, lj;
      Cell_handle pi_cell = tr.locate(pi, lt, li, lj);
      if(lt != Tr::OUTSIDE_AFFINE_HULL) {
        switch (tr.dimension())
        { //skip dimension 0
        case 1:
          if (tr.side_of_power_segment(pi_cell, pi, true) != CGAL::ON_BOUNDED_SIDE)
            continue;
          break;
        case 2:
          if (tr.side_of_power_circle(pi_cell, 3, pi, true) != CGAL::ON_BOUNDED_SIDE)
            continue;
          break;
        case 3:
          if (tr.side_of_power_sphere(pi_cell, pi, true) != CGAL::ON_BOUNDED_SIDE)
            continue;
        }
      }*/
      const typename Mesh_domain::Index index = CGAL::cpp11::get<1>(intersect);
      Vertex_handle v = tr.insert(pi);
      // `v` could be null if `pi` is hidden by other vertices of `tr`.
      CGAL_assertion(v != Vertex_handle());
      c3t3.set_dimension(v, 2); // by construction, points are on surface
      c3t3.set_index(v, index);
    }
  }
  std::cout << "  " << tr.number_of_vertices() << " initial points." << std::endl;
  if ( c3t3.triangulation().dimension() != 3 ) {
    std::cout << "  not enough points: triangulation.dimension() == "
              << c3t3.triangulation().dimension() << std::endl;
  }
}

void gen_initial_mesh_vessels_points_segments(C3t3 & c3t3, Network & net, const Mesh_domain & domain)
{
  //this version is only for vessels
  typedef typename C3t3::Triangulation       Tr;
  typedef typename Tr::Weighted_point        Weighted_point;
//  typedef typename Tr::Bare_point            Bare_point;
  typedef typename Tr::Segment               Segment_3;
  typedef typename Tr::Geom_traits::Vector_3 Vector_3;
  typedef typename Tr::Vertex_handle         Vertex_handle;
  
  Tr& tr = c3t3.triangulation();
  typename Tr::Geom_traits::Construct_weighted_point_3 p2wp =
    tr.geom_traits().construct_weighted_point_3_object();
  typedef std::vector< std::pair<Point_n_Rad, Plane> > Seeds;
  std::vector<Point_n_Rad> simple_seeds;
  Seeds seeds;
  net.get_seeds(seeds, simple_seeds);

  typename Mesh_domain::Is_in_domain is_in_dom =
      domain.is_in_domain_object();

  for (auto &it : simple_seeds) {
    if (is_in_dom(it.p)) {
      Vertex_handle v = tr.insert(p2wp(it.p));
      CGAL_assertion(v != Vertex_handle());
      c3t3.set_dimension(v, 3); // these points are inside vessel domain
      c3t3.set_index(v, *is_in_dom(it.p));
    } else {
      std::cout << "Failed to detect domain " << it.p << std::endl;
    }
  }

  for (auto &it : seeds) {
    auto & pp = it.first.p;
    if (is_in_dom(pp)) {
      Vertex_handle v = tr.insert(p2wp(pp));
      CGAL_assertion(v != Vertex_handle());
      c3t3.set_dimension(v, 3); // these points are inside vessel domain
      c3t3.set_index(v, *is_in_dom(pp));
    } else {
      std::cout << "!!! Failed to detect domain"  << pp << std::endl;
    }
  }

  for (auto &it: seeds) {
//this is main thing
    typename Mesh_domain::Construct_intersection construct_intersection =
      domain.construct_intersection_object();
    Plane & plane = it.second;
    std::vector<Vector_3> directions;

    Vector v1 = plane.base1();
    v1/=sqrt(v1.squared_length());
    Vector v2 = plane.base2();
    v2/=sqrt(v2.squared_length());
    double alpha = 2*M_PI/6;
    double a = 0;
    for (int i = 0; i < 6; i++) {
      directions.push_back(sin(a)*v1 + cos(a)*v2);
      a += alpha;
    }

    for (const Vector_3& vv: directions) {
      typename Mesh_domain::Intersection intersect;
      double coef;
      for (coef = 0.1; coef < 8; coef+=0.01) {
        double rad = coef*it.first.r;
        intersect = construct_intersection(Segment_3(it.first.p, it.first.p+rad*vv));
        if (CGAL::cpp11::get<2>(intersect) != 0)
          break;
      }
      assert (coef < 8);
      Weighted_point pi = p2wp(CGAL::cpp11::get<0>(intersect));
      // This would cause trouble to optimizers
      // check pi will not be hidden
      /*
      typename Tr::Locate_type lt;
      Cell_handle c;
      int li, lj;
      Cell_handle pi_cell = tr.locate(pi, lt, li, lj);
      if(lt != Tr::OUTSIDE_AFFINE_HULL) {
        switch (tr.dimension())
        { //skip dimension 0
        case 1:
          if (tr.side_of_power_segment(pi_cell, pi, true) != CGAL::ON_BOUNDED_SIDE)
            continue;
          break;
        case 2:
          if (tr.side_of_power_circle(pi_cell, 3, pi, true) != CGAL::ON_BOUNDED_SIDE)
            continue;
          break;
        case 3:
          if (tr.side_of_power_sphere(pi_cell, pi, true) != CGAL::ON_BOUNDED_SIDE)
            continue;
        }
      }*/
      const typename Mesh_domain::Index index = CGAL::cpp11::get<1>(intersect);
      Vertex_handle v = tr.insert(pi);
      // `v` could be null if `pi` is hidden by other vertices of `tr`.
      CGAL_assertion(v != Vertex_handle());
      c3t3.set_dimension(v, 2); // by construction, points are on surface
      c3t3.set_index(v, index);
    }
  }
  std::cout << "  " << tr.number_of_vertices() << " initial points." << std::endl;
  if ( c3t3.triangulation().dimension() != 3 ) {
    std::cout << "  not enough points: triangulation.dimension() == "
              << c3t3.triangulation().dimension() << std::endl;
  }
}
#endif

void super_meshgen(const char * domain_file, const char * cleaned_domain, const char * network_file, double size, double ves_size, double prec, double surf_prec)
{
  //domain_file - your 3d image
  //cleaned_domain - the same image but with vessels painted out
  //network_file - vessels graph
  //size - size of elements in mesh
  //ves_size - size of elements in vessels(probably the same as size)
  //prec - precision of representation of surfaces
  //surf_prec - precision of representation of surfaces

  //read files
  //you should provide the offset for vessel graph
  Network net(network_file, 95.499, 134.754, 1266.21);
  std::cout << "READ NETWORK complete" << std::endl;
  //prepare network
  //we'll simplify it a bit to make everything work

  net.gen_topology(10);
//  net.check_boundaries(); still dunno
  double diff_sizes = 2;
//  std::cout << "Please set up diff_sizes (big_rad > diff*small => set big_rad)" << std::endl;
//  std::cin >> diff_sizes;
  net.fix_sharp_ends(diff_sizes);
//  std::cout << "Please set up rad_add: " << std::endl;
  double rad_add = 1;
//  std::cin >> rad_add;
  net.check_ends(rad_add); //must be used
//  net.cut_last_nodes(cc); this function is useless
  net.reduction(1);
//cheating 
//std::cout << "Please set up radii decrease factor (>=1): " << std::endl;
//  double rad_f;
//  std::cin >> rad_f;
//net.decrease_radii(rad_f);
net.cut_radii();

  //open domain
  Danilov_reader_n_exporter dre;
  CGAL::Image_3 image = dre.nifti_reader(domain_file);

std::cout << "Voxel size: " <<  image.vx() << "x" << image.vy() << "x" << image.vz() << std::endl; 

  Danilov_reader_n_exporter dre_cleaned;
  CGAL::Image_3 cleaned = dre_cleaned.nifti_reader(cleaned_domain);


  const int vessel_index = 5;
int limit_points = 100000;//max number of points in tubes
//std::cout << "enter limit_points:" << std::endl;
//std::cin >> limit_points;
std::cout << "enter coeff for special parameter: " << std::endl;
double spec_coeff;
std::cin >> spec_coeff;
  Vessel vessel(limit_points, spec_coeff*ves_size*ves_size/4, vessel_index); 
  Super_domain domain(image, vessel, cleaned, 1e-4);
  vessel.set_domain(domain);

bool mode = 1;//use this mode!!!!!!

//std::cout << "enter 0 for old, 1 for cone mode: ";
//std::cin >> mode;
  vessel.construct_vessel(net, mode);//0 for old mode, 1 for cone new mode

  //TODO:
  //initial_points!
 
  //generate initial mesh
  C3t3 c3t3;
  //initial_points generation must be replaced!!!!11
  vessel.gen_initial_points(net, c3t3);

 // export2vtu(c3t3, "out_initial_dump.vtu");
  //refine initial mesh using tuned cgal routines
  Super_mesh_criteria criteria(Super_mesh_facet_criteria(30/*10*/, size, surf_prec/*3*/, 0.000001, ves_size, prec,
                                                     vessel_index, vessel),
                             Super_mesh_cell_criteria(3, size, vessel_index));

//  Mesh_criteria criteria(facet_angle=1, facet_size=6, facet_distance=1,
  //                       cell_radius_edge_ratio=100, cell_size=6);

  CGAL::refine_mesh_3(c3t3, domain, criteria, no_lloyd(), no_odt(), no_perturb(), no_exude());
 
  //export mesh to .vtu file
  dre.vtu_exporter(c3t3, "out_super.vtu");  

std::cout << "running exuder..." << std::endl;
  auto retvalue2 = CGAL::exude_mesh_3(c3t3);//, cc, tt, 0);//, sliver_bound);
  dre.vtu_exporter(c3t3, "out_super_with_exuder.vtu");
  switch (retvalue2) {

  case CGAL::BOUND_REACHED:
  std::cout << "Exuder returned BOUND_REACHED" << std::endl;
  break;

  case CGAL::TIME_LIMIT_REACHED:
  std::cout << "Exuder returned TIME_LIMIT_REACHED" << std::endl;
  break;

  case CGAL::CANT_IMPROVE_ANYMORE:
  std::cout << "Exuder returned CANT_IMPROVE_ANYMORE" << std::endl;
  break;

  default:
  std::cout << "Exuder return unknown status" << std::endl;

  }

  remove_slivers_near_vessels(c3t3, domain, 5);
  dre.vtu_exporter(c3t3, "out_very_super.vtu");
  export_q_factors("super-quality-all.dat", c3t3);
  export_histfile("super-dihedral-all.dat", c3t3);
  export_q_factors("super-quality-ves.dat", c3t3, 5);
  export_histfile("super-dihedral-ves.dat", c3t3, 5);
}

int main(int argc, char** argv)
{
#ifndef CGAL_MESH_3_NO_LONGER_CALLS_DO_INTERSECT_3
  //domain class doesn't have this method
  //so please make sure you won't be here!
  assert(0);
#endif
  //assert(0);
  if (argc != 8) {
    std::cout << "Usage: " << argv[0] << " nifti-file" << " cleaned-domain" << " network-vtp" << " size"  << " vessel-size" <<  " ves-prec"  << " surface-prec"<< std::endl;
    return 0;
  }
//  double eps = atof(argv[3]);
//  double coef = atof(argv[4]);
//  double rad_add = atof(argv[5]);
  double size = atof(argv[4]);
  double ves_size = atof(argv[5]);
  double prec = atof(argv[6]);
  double surf_prec = atof(argv[7]);
  assert(size == ves_size);//because probably you wish it
  super_meshgen(argv[1], argv[2], argv[3], size, ves_size, prec, surf_prec);
  return 0;
}
