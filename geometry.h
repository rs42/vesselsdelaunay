#pragma once
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
//#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Cartesian_converter.h>


#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include "new_criteria_3.h"

//#include <CGAL/Mesh_3/initialize_triangulation_from_labeled_image.h>

#include <CGAL/Labeled_image_mesh_domain_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/refine_mesh_3.h>
#include <CGAL/Image_3.h>

#include <CGAL/Surface_mesh.h>
#include <CGAL/convex_hull_3.h>

#include "render_vessels.h"

// Domain
#ifndef NOT_PREC_ARITHM 

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
//typedef CGAL::Exact_predicates_inexact_constructions_kernel EK;
typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt EK;
#else
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K EK;
#endif

//typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef CGAL::Cartesian_converter<K,EK>                         K_to_EK;
typedef CGAL::Cartesian_converter<EK,K>                         EK_to_K;


typedef CGAL::Labeled_image_mesh_domain_3<CGAL::Image_3,K> Mesh_domain;
typedef EK::FT FT;
//typedef EK::FT EFT;

typedef CGAL::Sequential_tag Concurrency_tag;

// Triangulation
typedef CGAL::Mesh_triangulation_3<Mesh_domain,CGAL::Default,Concurrency_tag>::type Tr;
typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;



class iTubeRestorer;
class Vessel;
// Criteria
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;
typedef CGAL::New_mesh_criteria_3<Tr, Mesh_domain, iTubeRestorer> New_mesh_criteria;
typedef CGAL::New_mesh_facet_criteria_3<Tr, Mesh_domain, iTubeRestorer> New_mesh_facet_criteria;
typedef CGAL::New_mesh_cell_criteria_3<Tr, Mesh_domain> New_mesh_cell_criteria;

// To avoid verbose function and named parameters call
using namespace CGAL::parameters;



typedef EK::Point_3 Point;
typedef K::Point_3 OPoint;
//typedef EK::Point_3 EPoint;
//typedef EK::Weighted_point_3 Weighted_point;
typedef K::Weighted_point_3 Weighted_point;
typedef K::Segment_3 OSegment;
typedef K::Vector_3 OVector;

typedef EK::Vector_3 Vector;
typedef EK::Plane_3 Plane;
typedef EK::Segment_3 Segment;
typedef EK::Line_3 Line;
/*
typedef EK::Vector_3 EVector;
typedef EK::Plane_3 EPlane;
typedef EK::Segment_3 ESegment;
typedef EK::Line_3 ELine;
*/


#include <CGAL/Surface_mesh.h>
#include <CGAL/Polygon_mesh_processing/self_intersections.h>
#include <CGAL/Side_of_triangle_mesh.h>
#include <CGAL/AABB_polyhedral_oracle.h>//maybe it's all we need??
#include <CGAL/intersections.h>
namespace PMP = CGAL::Polygon_mesh_processing;


typedef CGAL::Surface_mesh<Point> Surface_mesh;
typedef Surface_mesh::Vertex_index Vertex_index;
typedef Surface_mesh::Face_index Face_index;
typedef Surface_mesh::Edge_index Edge_index;

typedef CGAL::AABB_polyhedral_oracle<Surface_mesh, EK, EK> AABB_oracle;

struct Point_n_Rad
{
  Point p;
  double r;
  Point_n_Rad()
  :p(), r(0)
  {}
  Point_n_Rad(double *x, double& r)
  :p(x[0], x[1], x[2]), r(r)
  {}

};

inline double pnr_dist(const Point_n_Rad & p1, const Point_n_Rad & p2)
{
  return CGAL::sqrt(CGAL::to_double(CGAL::squared_distance(p1.p, p2.p))) - p1.r - p2.r;
}


struct Node;

#include <stack>
#include <utility>

struct Polyline
{
//for complicated stuff
  Plane plane_start, plane_end;
  bool plane_start_ready = 0, plane_end_ready = 0;
  bool is_start_stub, is_end_stub; //so it means if the point is really stub
  Point_n_Rad point_start, point_end;

  std::vector<Point> points_plane_start, points_plane_end;
  std::vector<Vertex_index> points_start_indices, points_end_indices;

//end complicated stuff


//looks like old stuff
//  std::vector<int> indexes_in_tetgen_start;
//  std::vector<Point> points_in_tetgen_start;
//  std::vector<int> indexes_in_tetgen_end;
//  std::vector<Point> points_in_tetgen_end;
//end looks like old stuff

//  bool is_valid;
  std::vector<int> points_id;
  std::vector<int> origin_points_id;
  //FT squared_len_simple(const std::vector<Point_n_Rad> & pv) const
 // {
   // return CGAL::squared_distance(pv[points_id.front()].p, pv[points_id.back()].p);
 // }
  double len_full(const std::vector<Point_n_Rad> & pv) const
  {
    double s = 0;
    for (unsigned i = 0; i+1 < points_id.size(); i++) {
      s += sqrt( CGAL::to_double( CGAL::squared_distance( pv[points_id[i]].p, pv[points_id[i+1]].p ) ) );
    }
    return s;
  }

  void reduction(const std::vector<Point_n_Rad> &pv, double epsilon)
  {//Douglas-Peucker algorithm
    //save at origin_points_id
    origin_points_id = points_id;

    std::vector<int> new_points_id;
    std::stack<std::pair<int,int>> istack;
    // в стек заносим первый и последний индексы
    istack.push( {0, points_id.size()-1} );
 // в массиве по заданному индексу храним оставлять точку (true) или нет (false)
    std::vector<int> keep_point(points_id.size(), 1);
    while (!istack.empty()) {
      int startIndex = istack.top().first;
      int endIndex = istack.top().second;
      istack.pop();
   
      double dMax = 0;
      int index = startIndex;
      for (int i = startIndex + 1; i < endIndex; i++) {
        if (keep_point[i]) {
          double d = sqrt( CGAL::to_double(
                           CGAL::squared_distance(Segment(pv[points_id[startIndex]].p, pv[points_id[endIndex]].p), pv[points_id[i]].p)));
          if (d > dMax) {
            index = i;
            dMax = d;
          }
        }
      }
      if (dMax >= epsilon) {
        istack.push({startIndex, index});
        istack.push({index, endIndex});
      } else {
        for (int j = startIndex + 1; j < endIndex; j++)
          keep_point[j] = false;
      }
    }
  
    for (unsigned i = 0; i < points_id.size(); i++) {
      if (keep_point[i]) new_points_id.push_back(points_id[i]);
    }


    points_id = new_points_id;
/*    
    //CHECK THIS
    if (new_points_id.size() > 2) {
      points_id = new_points_id;
    } else {
      assert(new_points_id.size() == 2);//only first and last
      int m_p_id = points_id[points_id.size()/2];
      assert(m_p_id != points_id.front() && m_p_id != points_id.back());
      points_id.clear();
      points_id.push_back(new_points_id[0]);
      points_id.push_back(m_p_id);
      points_id.push_back(new_points_id[1]);
    }
    */
  }
};

struct Node
{
 // MAYBE ITS BETTER TO TIE TOGETHER? LIKE IN std::pair???
  std::vector<std::list<Node>::iterator> neighb_nodes_it;
  std::vector<std::list<Polyline>::iterator> polylines_it;

  std::vector<int> points_id;
  std::vector<bool> is_first;
 
  void change_node_it(std::list<Polyline>::iterator poly,
                      std::list<Node>::iterator ni)
  {
    for (unsigned b = 0; b < points_id.size(); b++) {
      if (poly == polylines_it[b]) {
        neighb_nodes_it[b] = ni;
        return;
      }
    }
    assert(0);
  }

  void change_poly(std::list<Polyline>::iterator prev,
                   std::list<Polyline>::iterator nnew,
                   std::list<Node>::iterator node,
                   int p_i)
  {
    assert(points_id.size() > 0);
    for (unsigned b = 0; b < points_id.size(); b++) {
      if (prev == polylines_it[b]) {
        assert(points_id[b] == p_i);
        neighb_nodes_it[b] = node;
        polylines_it[b] = nnew;
        return;
      }
    }
    assert(0);
  }
/*
  void change_pointId(std::list<Polyline>::iterator poly, int i)
  {
    for (unsigned b = 0; b < points_id.size(); b++) {
      if (poly == polylines_it[b]) {
        points_id[b] = i;
        return;
      }
    }
    assert(0);
  }*/
  void remove_polyline(std::list<Polyline>::iterator pr)
  {
    for (unsigned i = 0; i < polylines_it.size(); i++) {
      if (polylines_it[i] == pr) {
        neighb_nodes_it[i] = neighb_nodes_it.back();
        neighb_nodes_it.pop_back();
        polylines_it[i] = polylines_it.back();
        polylines_it.pop_back();
        points_id[i] = points_id.back();
        points_id.pop_back();
        return;
      }
    }
    assert(0);
  }
  std::list<Node>::iterator getNode(std::list<Polyline>::iterator pr)
  {
    for (unsigned i = 0; i < polylines_it.size(); i++) {
      if (polylines_it[i] == pr) {
        return neighb_nodes_it[i];
      }
    }
    assert(0);
    throw 0;
  }
  void getPolyInfo(unsigned i, std::list<Node>::iterator &ni,
                   std::list<Polyline>::iterator &pr, int & point_id)
  {
    assert(i < points_id.size());
    ni = neighb_nodes_it[i];
    pr = polylines_it[i];
    point_id = points_id[i];
  }
  void check(std::list<Node>::iterator nod, std::list<Polyline>::iterator pr)
  {
    for (unsigned i = 0; i < points_id.size(); i++) {
      if (pr == polylines_it[i]) {
        assert(nod == neighb_nodes_it[i]);
        return;
      }
    }
    assert(0);
  }
  Node(std::list<Node>::iterator ni, std::list<Polyline>::iterator pi, int b)
  {
    neighb_nodes_it.push_back(ni);
    polylines_it.push_back(pi);
    points_id.push_back(b);
  }
  Node()
  {}
};

struct Network
{
  std::vector<Point_n_Rad> points;
  std::list<Polyline> polylines;
  std::list<Node> nodes;

  Network(const char* ifile);
  Network(const char* ifile, double, double, double);

  //why we need this
 /*
  void prepare_for_new_mode()
  {//this is how we simplify ends and make them cylinders instead of arbitrary cones
   //tune it if it too harsh
    for (auto &poly: polylines) {
      if (poly.points_id.size() == 3) {
        const int & p1 = poly.points_id[0];
        const int & p2 = poly.points_id[1];
        const int & p3 = poly.points_id[2];
        const double rad = std::min( std::min(points[p1].r, points[p2].r) , points[p3].r);
        points[p1].r = points[p2].r = points[p3].r = rad;
      } else {
        const int sz = poly.points_id.size();
        assert(sz > 3);
        const int & p1 = poly.points_id[0];
        const int & p2 = poly.points_id[1];
        points[p1].r = points[p2].r = std::min(points[p1].r, points[p2].r);
        const int & p3 = poly.points_id[sz-1];
        const int & p4 = poly.points_id[sz-2];
        points[p3].r = points[p4].r = std::min(points[p3].r, points[p4].r);
      }
    }
  }

  void decrease_radii(double factor)
  {
    for (auto &p: points)
      p.r/=factor;
    //for (auto &p: points)
    //  p.r = factor;//!!!!!!!!
  }

  void cut_radii()
  {
    double max_rad = 0;
    for (auto &p: points)
      max_rad = (p.r > max_rad ? p.r : max_rad);
    std::cout << "max_rad: " << max_rad << std::endl;
    std::cout << "cut radii to the threshold, define value:";
    double v;
    std::cin >> v;
    for (auto &p: points)
      p.r = (p.r > v ? v : p.r);
  }
*/
  //probably we still need this
  //because for now there's no function to build stub as a cone
  void fix_sharp_ends(double diff)
  {//use before reduction
    for (auto &n: nodes) {
      if (n.points_id.size() == 1) {
        Polyline & poly = *n.polylines_it[0];
        bool is_first = (n.points_id[0] == poly.points_id[0]);
        const double rad_node = points[n.points_id[0]].r;
        int index_next_node = (is_first ? 1: poly.points_id.size()-2);
        const double rad_next = points[poly.points_id[index_next_node]].r;
        if (rad_next > diff*rad_node) {
          std::cout << "FIXING RADS: " << rad_node << " will be " << rad_next << std::endl;
          points[n.points_id[0]].r = rad_next;
        }
      }
    }
  }

  void reduction(double eps)
  {
    for (auto &p: polylines)
      p.reduction(points, eps);
  }
  //why we need to dismiss smth
  void gen_topology(double dismiss_feature_size = 0);
  void get_seeds(std::vector<std::pair<Point, Plane>> &, std::vector<Point> &);  
  void get_seeds(std::vector<std::pair<Point_n_Rad, Plane>> & seeds,
                 std::vector<Point_n_Rad> &s_seeds);

  Point & get_Point(int i)
  {
    return points[i].p;
  }

  void find_two_nearest_points(const Point& p, Point_n_Rad &a, Point_n_Rad &b) const;

  double find_nearest_node(const OPoint &p, double& rad) const
  {
    auto best_node_it = nodes.begin();
    double squared_mindist =
             CGAL::squared_distance(p, EK_to_K()(points[nodes.front().points_id[0]].p));
    for (auto node_it = nodes.begin(); node_it != nodes.end(); ++node_it) {
      double tmp = CGAL::squared_distance(p, EK_to_K()(points[node_it->points_id[0]].p));
      if (squared_mindist > tmp) {
        squared_mindist = tmp;
        best_node_it = node_it;
      }
    }
    //now find point with biggest radius
    double max_rad = 0;
    for (auto & i : best_node_it->points_id) {
      if (points[i].r > max_rad) {
        max_rad = points[i].r;
      }
    }
    rad = max_rad;
    return squared_mindist;
  }
  /*why we need this
  void cut_last_nodes(double part)
  {
    for (auto &n: nodes) {
      if (n.points_id.size() > 2) continue;
      assert(part < 0.5);
      auto & pol = *(n.polylines_it[0]);
      auto num_to_delete = part * pol.points_id.size();
      int & node_i = n.points_id[0];
      std::vector<int> new_i;
      if (node_i == pol.points_id.front()) {
        node_i = pol.points_id[num_to_delete];
        for (auto i = num_to_delete; i < pol.points_id.size(); i++)
          new_i.push_back(pol.points_id[i]);
      } else if (node_i == pol.points_id.back()) {
        node_i = pol.points_id[pol.points_id.size() - num_to_delete-1];
        for (auto i = 0; i < pol.points_id.size() - num_to_delete; i++)
          new_i.push_back(pol.points_id[i]);
      } else {
        assert(0);
      }
      pol.points_id = new_i;
    }
  }
  */
  void make_node_next(Node & n, Polyline & pol, const int & n_index)
  {
    //find out if node is start or end...
    if (n_index == pol.points_id.front()) {
      //start
      std::vector<int> new_indexes;
      new_indexes.insert(new_indexes.end(), std::next(pol.points_id.begin()), pol.points_id.end());
      pol.points_id = new_indexes;
      n.points_id[0] = pol.points_id.front();
    } else if (n_index == pol.points_id.back()) {
      //end
      pol.points_id.pop_back();
      n.points_id[0] = pol.points_id.back();
    } else {
      assert(0);
    }
  }
  /*why we need this
  void check_ends(double rad_add)
  {
    int counter = 0;
    bool ok_flag = false;
    while (!ok_flag) {
    ok_flag = true;
    
    for (auto &n:nodes) {
      if (n.points_id.size() > 2) continue;
      auto & pol = *(n.polylines_it[0]);
      assert(pol.points_id.size() > 3);
      int point_i = n.points_id[0];
      auto & p_n = points[point_i];
      //now check if there is intersections...
      for (auto &another_pol : polylines) {
        if (&pol == &another_pol) continue;
        for (auto &p2_i : another_pol.points_id) {
          auto & p2 = points[p2_i];
          if (pnr_dist(p_n, p2) - rad_add > 0)
            continue;
          ok_flag = false;
          counter++;
          //delete this node
          make_node_next(n, pol, point_i);
          //stupid code....
          goto uniform_approach;
        }
      }
      uniform_approach:
      ;
    }

    //end while
    }
    std::cout << "DELETED by CHECK_ENDS: " << counter << std::endl;
  }
  */
};

void Network::get_seeds
   (std::vector<std::pair<Point, Plane>> & seeds, std::vector<Point> &s_seeds)
{
  for (auto &node : nodes) {
    s_seeds.push_back(points[node.points_id[0]].p);
  }
  Point_n_Rad p1, p2, p3;
  for (auto &polyline : polylines) {
    assert(polyline.points_id.size() > 1);
    p1 = points[ polyline.points_id[0] ];
    p2 = points[ polyline.points_id[1] ];

    for (unsigned i = 1; i < polyline.points_id.size() - 1; i++) {
      p3 = points[ polyline.points_id[i+1] ];

      Vector norm = (p2.p-p1.p) * CGAL::inverse(CGAL::sqrt((p2.p-p1.p).squared_length())) + (p3.p-p2.p) * CGAL::inverse(CGAL::sqrt((p3.p-p2.p).squared_length()));

      seeds.emplace_back(p2.p, Plane(p2.p, norm));
      p1 = p2;
      p2 = p3;
    }
  }
}
void Network::get_seeds(std::vector<std::pair<Point_n_Rad, Plane>> & seeds,
                        std::vector<Point_n_Rad> &s_seeds)
{
  for (auto &node : nodes) {
    s_seeds.push_back(points[node.points_id[0]]);
  }
  Point_n_Rad p1, p2, p3;
  for (auto &polyline : polylines) {
    assert(polyline.points_id.size() > 1);
    p1 = points[ polyline.points_id[0] ];
    p2 = points[ polyline.points_id[1] ];

    for (unsigned i = 1; i < polyline.points_id.size() - 1; i++) {
      p3 = points[ polyline.points_id[i+1] ];

      Vector norm = (p2.p-p1.p)/CGAL::sqrt((p2.p-p1.p).squared_length()) + (p3.p-p2.p)/CGAL::sqrt((p3.p-p2.p).squared_length());

      seeds.emplace_back(p2, Plane(p2.p, norm));
      p1 = p2;
      p2 = p3;
    }
  }
}
 
void Network::gen_topology(double dfs)
{//fill node info and remove small branches
  //first make all possible nodes
  for (auto pl = polylines.begin(); pl != polylines.end(); ++pl) {
    nodes.push_back(Node());
    auto iter = --nodes.end();
    nodes.push_back(Node(iter, pl, pl->points_id.back()));
    *iter = Node(--nodes.end(), pl, pl->points_id.front());
    nodes.back().check(iter, pl);
    iter->check(--nodes.end(), pl);
    //nodes.push_back(Node(pl, pl->points_id.front()));
    //nodes.push_back(Node(pl, pl->points_id.back()));
  }
  //assume network is a tree
  
  //now merge same nodes
  for (auto c = nodes.begin(); c != nodes.end(); ++c) {
    Point & cp = get_Point(c->points_id[0]);
    for (auto f = std::next(c); f != nodes.end(); ++f) {
      if (get_Point(f->points_id[0]) == cp) { //merge f into c
        c->polylines_it.push_back( f->polylines_it[0] );
        c->points_id.push_back( f->points_id[0] );
        c->neighb_nodes_it.push_back( f->neighb_nodes_it[0] );
        //f->neighb_nodes_it[0]->neighb_nodes_it[0] = c;
        //that's wrong line!
        f->neighb_nodes_it[0]->change_node_it(f->polylines_it[0], c);
        auto copy = f--;
        nodes.erase(copy);
      }
    }
  }
 std::cout << "Merging complete" << std::endl;
  //search for small branches
  //delete small branch if it has at least one its own unique node
  //so search through nodes with only one polyline
  for (auto c = nodes.begin(); c != nodes.end();) {
    assert(c->points_id.size() > 0);
    assert(c->polylines_it[0]->len_full(points) > 0);
    if (c->points_id.size() > 1 || c->polylines_it[0]->len_full(points) > dfs) {
      c++;
      continue;
    }
    std::cout << "RR" << std::endl;
    auto dpol_it = c->polylines_it[0];
    //now find another node (another end of this polyline) and modify it
    auto another_node = c->getNode(dpol_it);
    assert(another_node == c->neighb_nodes_it[0]);
    another_node->remove_polyline(dpol_it);
    if (another_node->points_id.size() == 0) {
      std::cout << "removing completely one part of network, it's currently unsupported" << std::endl;
    }
    assert(another_node->points_id.size() > 0);
    //if this node now connect only 2 polylines then merge them into one
   std::cout << "EEEE"<< std::endl;



    //bad way to remove points
    Point_n_Rad pointf = points[ another_node->points_id[0] ];
    for (unsigned i = 0; i < dpol_it->points_id.size(); i++) {
      points[ dpol_it->points_id[i] ] = pointf;
    }
    //endof bad way
    

    if (another_node->points_id.size() == 2) {
      //merging two polylines...
      //add tail to the head
      //head will stay in the list
      std::list<Node>::iterator tail_node, head_node;
      std::list<Polyline>::iterator tail_poly, head_poly;
      int tail_pointId, head_pointId;
      another_node->getPolyInfo(0, tail_node, tail_poly, tail_pointId);
      tail_node->check(another_node, tail_poly);
      another_node->getPolyInfo(1, head_node, head_poly, head_pointId);
      head_node->check(another_node, head_poly);
      //head is a poly which last point is the node
      //tail is a poly which first point is the node
      //!!!!check this lapsha!
      if (head_poly->points_id.back() == head_pointId) {
        //do not change head
        if (tail_poly->points_id.front() == tail_pointId) {
          ;//ok, tail is correct, too
        } else {
          //reverse tail
          std::reverse(std::begin(tail_poly->points_id), std::end(tail_poly->points_id));
          //notify another end (tail_node)
     //     tail_node->change_pointId(tail_poly, tail_poly->points_id.back());
        }
      } else {
        if (tail_poly->points_id.back() == tail_pointId) {
          std::swap(tail_node, head_node);
          std::swap(tail_poly, head_poly);
          std::swap(tail_pointId, head_pointId);
        } else {
          //reverse head
          std::reverse(std::begin(head_poly->points_id), std::end(head_poly->points_id));
          //notify another end (head_node)
       //   head_node->change_pointId(head_poly, head_poly->points_id.front());
        }
      }
      //assume we have the same point twice but with two different radiuses
      std::cout << "VVVV"<< std::endl;
      int hip = head_poly->points_id.back();
      int tip = tail_poly->points_id.front();
      assert(tail_pointId == tip);
      assert(head_pointId == hip); 
      assert(get_Point(hip) == get_Point(tip));
      double r_hip = points[hip].r;
      double r_tip = points[tip].r;
      head_poly->points_id.pop_back();
      points[tip].r = (r_hip + r_tip) / 2;

      //append points
      head_poly->points_id.insert(head_poly->points_id.end(),
                        tail_poly->points_id.begin(), tail_poly->points_id.end());
      
      //notify both nodes about merging
      head_node->change_node_it(head_poly, tail_node);
      tail_node->change_poly
                 (tail_poly, head_poly, head_node, head_poly->points_id.back());
      std::cout << "DD" << std::endl;
      
      polylines.erase(tail_poly);
      //we don't need the node
      std::cout << "VAA" << std::endl;
      nodes.erase(another_node);
    }
 
    std::cout << "WWWWW" << std::endl;
    auto copy = c++;

    nodes.erase(copy);
    std::cout << "BB" << std::endl;
    polylines.erase(dpol_it);
    std::cout << "FV" <<std::endl;
  }

}

Network::Network(const char* ifile)
{ //1 if ok

  //Read the file
  vtkSmartPointer<vtkXMLPolyDataReader> reader =
    vtkSmartPointer<vtkXMLPolyDataReader>::New();
  reader->SetFileName(ifile);
  reader->Update();
  vtkSmartPointer<vtkPolyData> polyData = reader->GetOutput();

  //render(polyData);

  //now transform into cgal representation
  vtkSmartPointer<vtkCellArray> vtk_lines = polyData->GetLines();
  vtkSmartPointer<vtkPoints> vtk_points = polyData->GetPoints();
  vtkSmartPointer<vtkDoubleArray> vtk_radius =
    vtkDoubleArray::SafeDownCast(polyData->GetPointData()->GetArray("Radius"));

  if (vtk_radius == nullptr || vtk_points == nullptr) {
    std::cout << "SHIT" << std::endl;
    throw 0;
  }
  if (vtk_lines == nullptr) {
    std::cout << "no lines" << std::endl;
    throw 0;
  }

//fill network
  int points_num = vtk_points->GetNumberOfPoints();
  int lines_num = vtk_lines->GetNumberOfCells();
   
  points.reserve(points_num);

  for (int i = 0; i < points_num; i++) {
    double x[3];
    double r;
    vtk_radius->GetTuple(i,&r);
    vtk_points->GetPoint(i, x);
   // std::cout << x[0] << " " << x[1] << " " << x[2] << " " << r << std::endl;
    points.push_back(Point_n_Rad(x, r));
  }

  vtkIdType *ptIds = vtk_lines->GetPointer();
  for (int i = 0; i < lines_num; i++) {
    vtkIdType nverts = *ptIds++;
    polylines.emplace_back();
    Polyline & cur = polylines.back();
//    cur.is_valid = 1;
    cur.points_id.reserve(nverts);
    for (int j = 0; j < nverts; j++) {
      vtkIdType Id = *ptIds++;
      cur.points_id.push_back(Id);
      //std::cout << Id << " ";
    }
//    std::cout << std::endl;
  }
}


Network::Network(const char* ifile, double cx, double cy, double cz)
{ //1 if ok

  //Read the file
  vtkSmartPointer<vtkXMLPolyDataReader> reader =
    vtkSmartPointer<vtkXMLPolyDataReader>::New();
  reader->SetFileName(ifile);
  reader->Update();
  vtkSmartPointer<vtkPolyData> polyData = reader->GetOutput();

  //render(polyData);

  //now transform into cgal representation
  vtkSmartPointer<vtkCellArray> vtk_lines = polyData->GetLines();
  vtkSmartPointer<vtkPoints> vtk_points = polyData->GetPoints();
  vtkSmartPointer<vtkDoubleArray> vtk_radius =
    vtkDoubleArray::SafeDownCast(polyData->GetPointData()->GetArray("Radius"));

  if (vtk_radius == nullptr || vtk_points == nullptr) {
    std::cout << "SHIT" << std::endl;
    throw 0;
  }
  if (vtk_lines == nullptr) {
    std::cout << "no lines" << std::endl;
    throw 0;
  }

//fill network
  int points_num = vtk_points->GetNumberOfPoints();
  int lines_num = vtk_lines->GetNumberOfCells();
   
  points.reserve(points_num);

  for (int i = 0; i < points_num; i++) {
    double x[3];
    double r;
    vtk_radius->GetTuple(i,&r);
    vtk_points->GetPoint(i, x);
    x[0] -= cx;
    x[1] -= cy;
    x[2] -= cz;
   // std::cout << x[0] << " " << x[1] << " " << x[2] << " " << r << std::endl;
    points.push_back(Point_n_Rad(x, r));
  }

  vtkIdType *ptIds = vtk_lines->GetPointer();
  for (int i = 0; i < lines_num; i++) {
    vtkIdType nverts = *ptIds++;
    polylines.emplace_back();
    Polyline & cur = polylines.back();
//    cur.is_valid = 1;
    cur.points_id.reserve(nverts);
    for (int j = 0; j < nverts; j++) {
      vtkIdType Id = *ptIds++;
      cur.points_id.push_back(Id);
      //std::cout << Id << " ";
    }
//    std::cout << std::endl;
  }
}

void Network::find_two_nearest_points(const Point& p, Point_n_Rad &a, Point_n_Rad &b) const
{//a and b are supposed to be on the same polyline and near to each other
  //find a as nearest point
  //b will be a's neighbor
  auto best_pol = polylines.begin();
  unsigned best_index = 0;//in best_pol list of points_id vector
  FT min_dist =
                  CGAL::squared_distance(p, points[best_pol->origin_points_id[0]].p);
  for (auto cur_pol = polylines.begin(); cur_pol != polylines.end(); ++cur_pol) {
    int cur_best_index = 0;
    FT cur_min_dist =
              CGAL::squared_distance(p, points[cur_pol->origin_points_id[0]].p);

    for (unsigned cur_index = 1; cur_index < cur_pol->origin_points_id.size(); cur_index++)
    {
      FT tmp =
           CGAL::squared_distance(p, points[cur_pol->origin_points_id[cur_index]].p);
      if (tmp < cur_min_dist) {
        cur_min_dist = tmp;
        cur_best_index = cur_index;
      }
    }
    if (cur_min_dist < min_dist) {
      min_dist = cur_min_dist;
      best_pol = cur_pol;
      best_index = cur_best_index;
    }
  }
  
  a = points[best_pol->origin_points_id[best_index]];
  if (best_index == 0) {
    //select b as best_index+1
    assert(best_pol->origin_points_id.size() > 1);
    b = points[best_pol->origin_points_id[1]];
  } else if (best_index == best_pol->origin_points_id.size()-1) {
    //select b as best_index-1
    assert(best_pol->origin_points_id.size() > 1);
    b = points[best_pol->origin_points_id[best_index-1]];
  } else {
    //select nearest from best_index+-1
    int left = best_index-1;
    int right = best_index+1;
    FT dist_left =
         CGAL::squared_distance(p, points[best_pol->origin_points_id[left]].p);
    FT dist_right =
         CGAL::squared_distance(p, points[best_pol->origin_points_id[right]].p);
    if (dist_left < dist_right)
      b = points[best_pol->origin_points_id[left]];
    else
      b = points[best_pol->origin_points_id[right]];
  }
}


#include <CGAL/Random.h>

class iTubeRestorer
{
public:
  CGAL::Random random_num;
  Network & net;
  Mesh_domain & domain;
  int vessel_index;
  int upper_bound; 
 // bool is_only_one_point;//insert just one point instead of full
  typedef Mesh_domain::Index Index;
  iTubeRestorer(Network &net, Mesh_domain & domain, const int &v_i, int upper_bound)
  : net(net), domain(domain), vessel_index(v_i), upper_bound(upper_bound)
  {}
  
  void find_points(const Weighted_point& p, std::vector<std::pair<Weighted_point, Index>>& res)// const
  {//last point is for center
    //if there is no center then set index to Index()
    //first check if there is a node close to our point p so we dont need additional points at all near bifurcation 
//std::cout << p << std::endl;
    typename Mesh_domain::Construct_intersection construct_intersection =
           domain.construct_intersection_object();

    typename Mesh_domain::Is_in_domain is_in_dom =
        domain.is_in_domain_object();

    //Point_n_Rad node;
    OPoint pp = p.point();
    if (upper_bound == 0) {// || !is_in_dom(pp) || *is_in_dom(pp) != vessel_index) {
      res.emplace_back(Weighted_point(), Index());
      return;
    }
    double node_rad;
    double squared_mindist = net.find_nearest_node(/*K_to_EK()*/(pp), node_rad);
    if (squared_mindist < 4*node_rad*node_rad) { //find optimal constant...
      res.emplace_back(Weighted_point(), Index());
      return;
    }
    //random fail
    if (random_num.get_int(0,upper_bound) > 0) {
      res.emplace_back(Weighted_point(), Index());
      return;
    }


    //ok, now we should locate plane passing through p and cutting pipe nicely
    //we can use info about nearest node... but how
    Point_n_Rad a, b;
    net.find_two_nearest_points(K_to_EK()(pp), a, b); 
    //assume they are ok
    //you can implement additional checks like two segments p-a and p-b are inside vessel
    //if it's not then just drop it
    assert(a.p != b.p);
    Plane plane(K_to_EK()(pp), a.p - b.p);

    Vector v1 = plane.base1();
    v1/=CGAL::sqrt(v1.squared_length());
    Vector v2 = plane.base2();
    v2/=CGAL::sqrt(v2.squared_length());
 
    //find largest random segment from p to the vessel boundary

    CGAL::Random_points_on_circle_2<EK::Point_2> points_on_circle_2((a.r+b.r)/2);
//  typename Tr::Geom_traits::Construct_weighted_point_3 p2wp =
  //  tr.geom_traits().construct_weighted_point_3_object();

    std::vector<Vector> directions;
    for (int i = 0; i < 100; i++) {
      // shoot random directions
      FT x = points_on_circle_2->x();
      FT y = points_on_circle_2->y();
//  std::cout << "x: " << x << " y: " << y << " rad: " << (a.r+b.r)/2 << std::endl;
      points_on_circle_2++;
      //select points inside vessel
      Vector dir = x*v1+ y*v2;
      if (is_in_dom(pp + EK_to_K()(dir)) && *is_in_dom(pp + EK_to_K()(dir)) == vessel_index)
      {
//      if (is_in_dom(pp + dir)) {
  //      std::cout << "DOM: " << *is_in_dom(pp+dir) << std::endl;
      //  std::cout << "FFFFFF" << std::endl;
        typename Mesh_domain::Intersection intersect =
          construct_intersection(OSegment(pp, pp + EK_to_K()(dir)));
        if (CGAL::cpp11::get<2>(intersect) == 0)
          directions.push_back(dir);
      }
    }
    if (directions.size() == 0) {
      res.emplace_back(Weighted_point(), Index());
      return;
    }
    assert(directions.size() > 0);
    //std::vector<double> distances;
    FT sqmax_dist = 0;
    OPoint max_point(CGAL::ORIGIN);
    for (auto &dir: directions) {
      double coef;
      for (coef = 1; coef < 5; coef+=0.1) {
        typename Mesh_domain::Intersection intersect =
                              construct_intersection(OSegment(pp, pp + coef * EK_to_K()(dir)));
        if (CGAL::cpp11::get<2>(intersect) != 0) {
          double tmp = CGAL::to_double(CGAL::squared_distance(CGAL::cpp11::get<0>(intersect), pp));
          if (tmp > sqmax_dist) {
            sqmax_dist = tmp;
            max_point = CGAL::cpp11::get<0>(intersect);
          }
          break;
        }
      }
      if (coef >= 5) {
        res.emplace_back(Weighted_point(), Index());
        return;
      }
      assert(coef < 5);
    }
  
    if (CGAL::sqrt(sqmax_dist) / (a.r+b.r) > 1.5)
      std::cout << "radii: " << (a.r+b.r)/2 << " " << sqrt(sqmax_dist)/2 << std::endl;
      
    //Point center = CGAL::midpoint(max_point, pp);//(max_point + pp)/2;
    OPoint center = OPoint((max_point.x() + pp.x()) / 2,
                         (max_point.y() + pp.y()) / 2,
                         (max_point.z() + pp.z()) / 2); 

    //lets find another 4 points
    Vector normal = plane.orthogonal_vector();
    normal/=CGAL::sqrt(normal.squared_length());
    Vector ww = K_to_EK()(pp - center);
    Vector pcp = CGAL::cross_product(ww, normal);
    for (int i = 1; i < 6; i++) {
      typename Mesh_domain::Intersection intersect;
      double coef;
      Vector cur_vec = cos(M_PI/3*i)*ww + sin(M_PI/3*i)*pcp;
      for (coef = 0.1; coef < 8; coef+=0.01) {
        intersect = construct_intersection(OSegment(center, center+coef*EK_to_K()(cur_vec)));
        if (CGAL::cpp11::get<2>(intersect) != 0)
          break;
      }
      if (coef >= 8) {
        continue;
      }
      assert (coef < 8);
      res.emplace_back(Weighted_point(CGAL::cpp11::get<0>(intersect), 0),
                            CGAL::cpp11::get<1>(intersect));
    }
    //dont forget about center
    res.emplace_back(Weighted_point(center, 0), vessel_index);
    return;
  }
};

class Vessel
{
public:
  Network * netp;
  int limit_points;
  double bunch_limit;

  //lets manage set of points inside vessel, they'll be inserted in mesh as points inside vessel domain
  //dont forget to check if all of them are inside !
  //points from vessel surface are points on the border between vessel domain and smth else
  std::vector<Point> p_inside_vessel;
  std::vector<Face_index> faces_on_stubs;
//  int saved_index;//a property from intersected facet linked with bunches[saved_index]
//  Point saved_point; //intersection point
 
  std::vector<std::pair<Point,int>> saved_points;
  void save_point(const Point & p, const int & i)
  {
    for (auto &pb:saved_points) {
      if (pb.second == -2) {
        pb.second = i;
        pb.first = p;
        return;
      }
    }
    saved_points.emplace_back(p, i);
  }
  void get_saved_index_and_point(const OPoint &p, int &i, Point & true_p)
  {
    for (auto &pb:saved_points) {
      if (p == EK_to_K()(pb.first)) {
        assert(pb.second > -2);//i think it should never be called twice for the same point
        i = pb.second;
        true_p = pb.first;
        pb.second = -2;
        return;
      }
    }
    i = -1;
    return;
  //  assert(0);
  }
  //use Super domain here!!!
  Mesh_domain* orig_domain;
  void set_domain(Mesh_domain & domain)
  {
    orig_domain = &domain;
  }

  typedef Mesh_domain::Index Index;
 
  //last segment is for central line!
  typedef std::pair<std::vector<Segment>, std::vector<Point>> Bunch;//segments plus limit of point additions
  std::vector<Bunch> bunches;
//attentioN!!!!
//maybe we should store special surface_mesh for each connected component of vessel structure!
  AABB_oracle oracle;

//  CGAL::Side_of_triangle_mesh<Surface_mesh, K> inside;

/*Attention!
 *another way is to use just vector of triangles
 *to represent surface
 *we can make class derived from CGAL::Triangle_3
 *with additional field for bunch index!
 */ 
  Surface_mesh vm;
  Surface_mesh::Property_map<Face_index,int> bunches_indices;

  typedef int Subdomain_index;
  typedef boost::optional<Subdomain_index> Subdomain;
  typedef std::pair<Subdomain_index, Subdomain_index> Surface_patch_index;
  typedef boost::optional<Surface_patch_index> Surface_patch;
  /// Type of indexes to characterize the lowest dimensional face of the input
  /// complex on which a vertex lie
//  typedef boost::variant<Subdomain_index, Surface_patch_index> Index;
  typedef CGAL::cpp11::tuple<OPoint,Index,int> Intersection;
  /* The integer \ccc{dimension} is set to the dimension of the lowest
   * dimensional face in the input complex containing the returned point, and
   * \ccc{index} is set to the index to be stored at a mesh vertex lying
   * on this face.
   */

  int vessel_index;
  Vessel(int limit_points, double bunch_limit, int vessel_index)
  : limit_points(limit_points), bunch_limit(bunch_limit), vessel_index(vessel_index)
  {}
 
template <class Placement, class Cost>
void deal_with_optimizer_global(const std::string & dump_file);


void fair_global(const std::string & dump_file);


template <class Placement, class Cost>
void deal_with_optimizer_local(const std::string & dump_file);

  Subdomain get_subdomain(const OPoint & p) const
  {//return Subdomain(vessel_index) if inside
   //Subdomain() if outside
   //does it work? 
//std::cout << "subdom? " << std::endl;
//    CGAL::Bounded_side res = inside(p);
//    if (res == CGAL::ON_BOUNDED_SIDE) {
      //std::cout << "Oracle must be true: " << oracle.is_in_volume(oracle, p) << std::endl;//must be true!
    if (oracle.is_in_volume(oracle, K_to_EK()(p))) {
      return Subdomain(vessel_index);
    }
    //if (res == CGAL::ON_BOUNDARY) { ++nb_boundary; }
    else { return Subdomain(); }

//we can also use method from AABB_oracle
//    bool is_in_volume(const Surface_3& surface, const P& p)
//    oracle.is_in_volume(oracle, p)

  }

  bool can_i_add_points(Bunch & bunch, const Point & point)
  {
    std::vector<Point> & spoints = bunch.second;
    const Segment & central_s = bunch.first.back();
    const Point proj = Line(central_s).projection(point);
    for (auto &p : spoints) {
      if (CGAL::squared_distance(p, proj) < bunch_limit)
        return 0;
    }
    spoints.push_back(proj);
    return 1;
  }

  template<typename Object>
  bool get_intersection(Intersection& is, const Object& s)
  {
    //we must find not only point but also id of facet!
    //because facet is linked with its bunch!
    
    auto smt = oracle.tree()->any_intersection(K_to_EK()(s));
    if (smt) {
      // gets intersection object
      const Point* p = boost::get<Point>(&(smt->first));
      const Face_index fi = boost::get<Face_index>(smt->second);
      assert(p);
     // if (p)
      
//      std::cout << "intersection object is a point " << *p << " " << fi << std::endl;
      //somehow get value for saved_index from fi
      int saved_index = bunches_indices[fi];
    assert(saved_index > -2);
      const OPoint conv = EK_to_K()(*p);
      std::get<0>(is) = conv;// saved_point = *p;
      save_point(*p, saved_index);
      typename Mesh_domain::Is_in_domain is_in_dom =
        orig_domain->is_in_domain_object();
      auto os = is_in_dom(conv); 
      //assert(os); think about IT!! TODO!
      Subdomain_index a_d = (os ? *os : 0);
       
      Surface_patch_index index_v =
          (vessel_index < a_d ? Surface_patch_index(vessel_index, a_d) :
                                Surface_patch_index(a_d, vessel_index));

      std::get<1>(is) = Index(index_v);//todo!!! find correct Index(done?)
      std::get<2>(is) = 2;
      return true; //todo(done?)
    } else {
      //check if object is completely inside vessel
      if (oracle.is_in_volume(oracle, K_to_EK()(s.point(0)))) {
        is = Intersection();
        return true;
      } else { 
        return false;
      }
    }
  }
  void find_points(const Weighted_point& p, std::vector<std::pair<Weighted_point, Index>>& res) //const
  {
    //safe way
    //res.emplace_back(Weighted_point(), Index());
    //return;
   // std::cout << "add points" << std::endl;
    //true way
    double rad;
    const double squared_mindist = netp->find_nearest_node(p.point(), rad);
    if (squared_mindist < 4*rad) { //find optimal constant...
      res.emplace_back(Weighted_point(), Index());
      return;
    }
 

    //find index of the bunch
    //
    int bindex;
    Point true_p;
    get_saved_index_and_point(p.point(), bindex, true_p);
/*
    if (p.point() == saved_point) {
      bindex = saved_index;
    } else {
      //TODONOW
     // auto smt = oracle.tree()->any_intersected_primitive(p.point());
      
    }
*/
  //  std::cout << "Alive" << std::endl;

    if (limit_points == 0 || bindex == -1 || !can_i_add_points(bunches[bindex], true_p)) {
      //do not any other points!
      //i.e. it can be facet of bifurcation zone
      res.emplace_back(Weighted_point(), Index());
      return; 
    }
    std::cout << "LIM:" << limit_points << std::endl;
    limit_points--;
    //then find all intersections of segments in bunch with plane through p and
    //orthogonal to the last segment in bunch
    const std::vector<Segment> & bunch = bunches[bindex].first;
    const Segment & central = bunch.back();
    Plane plane(true_p, central.to_vector());
    assert(bunch.size() > 0);
    for (unsigned i = 0; i < bunch.size() - 1; i++) {
      CGAL::cpp11::result_of<EK::Intersect_3(Segment, Plane)>::type
        result = intersection(bunch[i], plane);
      if (result) {
        if (const Point* np = boost::get<Point>(&*result)) {
          //add this point
          //todo
          //find index! we need info about domain
          typename Mesh_domain::Is_in_domain is_in_dom =
            orig_domain->is_in_domain_object();
          const OPoint convv = EK_to_K()(*np);
          auto os = is_in_dom(convv); 
          assert(os);
          Subdomain_index a_d = *os;
          Surface_patch_index index_v =
              (vessel_index < a_d ? Surface_patch_index(vessel_index, a_d) :
                                    Surface_patch_index(a_d, vessel_index));
          res.emplace_back(Weighted_point(convv, 0), Index(index_v));
        } else {
          assert(0);
        }
      } else {
        std::cout << "!" << std::endl;
      }
    }
    //now check intersection with central
    CGAL::cpp11::result_of<EK::Intersect_3(Segment, Plane)>::type
      result = intersection(central, plane);
    if (result) {
      if (const Point* np = boost::get<Point>(&*result)) {
        //add this point
        //todo
        res.emplace_back(Weighted_point(EK_to_K()(*np), 0), Index(vessel_index));
      } else {
        assert(0);
      }
    } else {
      std::cout  << "!" << std::endl;
      res.emplace_back(Weighted_point(), Index());
    }
  }//end of func


struct Pbase
{
  Plane plane;
  std::vector<Point> points;
  std::vector<Vertex_index> indices;
  Point_n_Rad center;
  Pbase(const std::vector<Point> & points, const std::vector<Vertex_index> & indices,
        const Point_n_Rad & center)
  : points(points), indices(indices), center(center)
  {}
  Pbase(const Plane & plane, const Point_n_Rad & center)
  : plane(plane), center(center)
  {}
  Pbase()
  {}
  void reset()
  {
    points.clear();
    indices.clear();
  }


  void new_mode_propagate_from(const Pbase & prev, const FT & coef)
  {
    const std::vector<Point> & p = prev.points;
    //const double & r = center.r;
    Vector e = center.p - prev.center.p;
    if (center.r > prev.center.r) e = -e;
    e/=CGAL::sqrt(e.squared_length()); 
    for (unsigned i = 0; i < p.size(); i++) {
      const Point & pp = p[i];
      Vector oe = pp - prev.center.p - CGAL::scalar_product(pp - prev.center.p, e) * e;
      oe/=CGAL::sqrt(oe.squared_length());
      const Line x(pp, CGAL::sqrt(1-coef)*e - CGAL::sqrt(coef)*oe);
      //find intersection of line and plane
      CGAL::cpp11::result_of<EK::Intersect_3(Line, Plane)>::type
        result = intersection(x, plane);
      assert(result);
      const Point new_p = *(boost::get<Point>(&*result));
      if (CGAL::scalar_product(new_p-pp, center.p - prev.center.p) < 0)//means only forward by e
      {
        std::cout << "possible intersection!" << std::endl;
        assert(0);
      }
      points.push_back(new_p);
    }
    std::cout << "checking coplanarity... " << std::endl; 
    //check coplanar property
    unsigned ps = p.size();
    for (unsigned i = 0; i < ps; i++) {
    //  std::cout << (CGAL::coplanar(p[i], p[(i+1)%ps], points[i], points[(i+1)%ps])) <<*/ (CGAL::orientation(p[i],p[(i+1)%ps],points[i],points[(i+1)%ps])==CGAL::COPLANAR)  << std::endl;
;//       std::cout << (CGAL::volume(p[i], p[(i+1)%ps], points[i], points[(i+1)%ps]) == 0) << std::endl;
      const Point proj = Plane(p[i], p[(i+1)%ps], points[i]).projection(points[(i+1)%ps]);
      std::cout << "DIST: " << CGAL::sqrt(CGAL::squared_distance(proj, points[(i+1)%ps]))  << " Are same? " << (proj == points[(i+1)%ps]) << std::endl;
    }
    std::cout << "OK" << std::endl;
  }



  void new_mode_propagate_from(const Pbase & prev)
  {//assume this is for the last plane
   //so we have to propagate points parallel to central line
    const std::vector<Point> & p = prev.points;
    //const double & r = center.r;
    Vector e = center.p - prev.center.p;
    //e/=sqrt(e.squared_length()); 
    //const Vector norm = plane.orthogonal_vector();
    for (unsigned i = 0; i < p.size(); i++) {
      const Line x(p[i], e);
      //find intersection of line and plane
      CGAL::cpp11::result_of<EK::Intersect_3(Line, Plane)>::type
        result = intersection(x, plane);
      assert(result);
      const Point new_p = *(boost::get<Point>(&*result));
      if (CGAL::scalar_product(new_p-p[i], e) < 0)//means only forward by e
      {
        std::cout << "possible intersection!" << std::endl;
        assert(0);
      }
      points.push_back(new_p);
    }
    
    //check coplanar property
    unsigned ps = p.size();
    for (unsigned i = 0; i < ps; i++) {
 //     std::cout << (CGAL::coplanar(p[i], p[(i+1)%ps], points[i], points[(i+1)%ps])) << std::endl;
      const Point proj = Plane(p[i], p[(i+1)%ps], points[i]).projection(points[(i+1)%ps]);
      std::cout << "DIST: " << CGAL::sqrt(CGAL::squared_distance(proj, points[(i+1)%ps])) << std::endl;
 

    }
    std::cout << "OK" << std::endl;
  }


  void propagate_from(const Pbase & prev)
  {
    const std::vector<Point> & p = prev.points;
    const double & r = center.r;
    Vector e = center.p - prev.center.p;
    e/=CGAL::sqrt(e.squared_length()); 
    const Vector norm = plane.orthogonal_vector();
    for (unsigned i = 0; i < p.size(); i++) {
      const Vector x = p[i] - CGAL::ORIGIN;
      const Vector cent_v = center.p - CGAL::ORIGIN;
      const Vector k = x - cent_v -
        CGAL::scalar_product(x-cent_v,norm)/CGAL::scalar_product(e,norm) * e;

      const Point new_p = CGAL::ORIGIN + r/CGAL::sqrt(k.squared_length()) * k + cent_v;
/*this is bad attempt too
      const Vector p_min_c = p[i] - center.p;
      Vector long_boi = p_min_c - CGAL::scalar_product(p_min_c, e) * e;
      long_boi/=sqrt(long_boi.squared_length());
      Vector x_min_p = r*long_boi - p_min_c;
      const Point new_p = p[i] - CGAL::scalar_product(p_min_c, norm) / CGAL::scalar_product(x_min_p, norm) * x_min_p;
*/      
      if (CGAL::scalar_product(new_p-p[i], e) < 0)//means only forward by e
      {
        std::cout << "possible intersection!" << std::endl;
        assert(0);
      }
      points.push_back(new_p);
    }
    
    //check coplanar property
    unsigned ps = p.size();
    for (unsigned i = 0; i < ps; i++) {
      std::cout << (CGAL::coplanar(p[i], p[(i+1)%ps], points[i], points[(i+1)%ps])) << std::endl;
      const Point proj = Plane(p[i], p[(i+1)%ps], points[i]).projection(points[(i+1)%ps]);
      std::cout << "DIST: " << CGAL::sqrt(CGAL::squared_distance(proj, points[(i+1)%ps])) << std::endl;
 

    }
    std::cout << "OK" << std::endl;
  }
};


  void insert_faces_in_surface_spec(const Pbase & a_base, Pbase & b_base, Surface_mesh & sm, Surface_mesh::Property_map<Face_index,int> & propmap)
  {//and dont forget about bunches!
   //assume we had to insert vertices from b_base
   //a_base vertices were inserted on the previous step
   
    //1.insert b_base vertices
    for (auto &point: b_base.points) {
      const Vertex_index vi = sm.add_vertex(point);
      assert(vi != Surface_mesh::null_vertex());
      b_base.indices.push_back(vi);
    }
    //2.save bunches
/*yet we dont know what to do with it please check it!!!!!
 */
    bunches.emplace_back();
    std::vector<Segment> & bunch = bunches.back().first;
    bunches.back().second.push_back(a_base.center.p);
    bunches.back().second.push_back(b_base.center.p);
    const int bunch_index = bunches.size() - 1;
    for (unsigned i = 0; i < a_base.points.size(); i++) {
      bunch.emplace_back(a_base.points[i], b_base.points[i]);
    }
    //dont forget about central segment
    bunch.emplace_back(a_base.center.p, b_base.center.p);
/**/
    //3.insert facets in sm and set property
    const int n = a_base.indices.size();
    for (int i = 0; i < n; i++) {
      //we add quad 
      //if something wrong with indices try to add manually two triangles
     // std::cout << "OK" << std::endl;

/*this creates single quad
      const Face_index fi = sm.add_face(a_base.indices[i],
                                        a_base.indices[(i+1)%n],
                                        b_base.indices[(i+1)%n],
                                        b_base.indices[i]);
      assert(fi != Surface_mesh::null_face());
      bunches_indices[fi] = bunch_index;
*/
/*this creates two triangles*/
      const Face_index f1 = sm.add_face(a_base.indices[i],
                                        a_base.indices[(i+1)%n],
                                        b_base.indices[i]);
      assert(f1 != Surface_mesh::null_face());
      propmap[f1] = bunch_index;
      const Face_index f2 = sm.add_face(a_base.indices[(i+1)%n],
                                        b_base.indices[(i+1)%n],
                                        b_base.indices[i]);
      assert(f2 != Surface_mesh::null_face());
      propmap[f2] = bunch_index;
    }
  }


  void insert_faces_in_surface(const Pbase & a_base, Pbase & b_base)
  {//and dont forget about bunches!
   //assume we had to insert vertices from b_base
   //a_base vertices were inserted on the previous step
   
    //1.insert b_base vertices
    for (auto &point: b_base.points) {
      const Vertex_index vi = vm.add_vertex(point);
      assert(vi != Surface_mesh::null_vertex());
      b_base.indices.push_back(vi);
    }
    //2.save bunches
    bunches.emplace_back();
    std::vector<Segment> & bunch = bunches.back().first;
    bunches.back().second.push_back(a_base.center.p);
    bunches.back().second.push_back(b_base.center.p);
    const int bunch_index = bunches.size() - 1;
    for (unsigned i = 0; i < a_base.points.size(); i++) {
      bunch.emplace_back(a_base.points[i], b_base.points[i]);
    }
    //dont forget about central segment
    bunch.emplace_back(a_base.center.p, b_base.center.p);
    //3.insert facets in vm and set property
    const int n = a_base.indices.size();
    for (int i = 0; i < n; i++) {
      //we add quad 
      //if something wrong with indices try to add manually two triangles
     // std::cout << "OK" << std::endl;

/*this creates single quad
      const Face_index fi = vm.add_face(a_base.indices[i],
                                        a_base.indices[(i+1)%n],
                                        b_base.indices[(i+1)%n],
                                        b_base.indices[i]);
      assert(fi != Surface_mesh::null_face());
      bunches_indices[fi] = bunch_index;
*/
/*this creates two triangles*/
      const Face_index f1 = vm.add_face(a_base.indices[i],
                                        a_base.indices[(i+1)%n],
                                        b_base.indices[i]);
      assert(f1 != Surface_mesh::null_face());
      bunches_indices[f1] = bunch_index;
      const Face_index f2 = vm.add_face(a_base.indices[(i+1)%n],
                                        b_base.indices[(i+1)%n],
                                        b_base.indices[i]);
      assert(f2 != Surface_mesh::null_face());
      bunches_indices[f2] = bunch_index;
    }
  }
  void process_convex_hull(const Node & node);
  void construct_vessel(Network & net, bool is_new_mode);
  void new_construct_vessel(Network & net);
  void gen_initial_points(Network & net, C3t3& c3t3);
  void add_stub(const Point & center, const std::vector<Vertex_index> & indices);

  void add_stub_spec(const Point & center, const std::vector<Vertex_index> & indices, Surface_mesh & sm);
  void add_fake_stub_spec_no_center(const Point & center, const std::vector<Vertex_index> & indices, Surface_mesh & sm);


 
};

/*saved var with center
void Vessel::add_stub_spec(const Point & center, const std::vector<Vertex_index> & indices, Surface_mesh & sm)
{
old var
  Face_index fi = sm.add_face(indices);
  if (fi == Surface_mesh::null_face()) {
    auto x = indices;
    std::reverse(x.begin(), x.end());
    fi = sm.add_face(x);
  }
  assert(fi != Surface_mesh::null_face());
  //bunches_indices[fi] = -1;
//end old var

  //new var inserts triangles plus center!
  const int n = indices.size();
  const Vertex_index center_index = sm.add_vertex(center);
  assert(center_index != Surface_mesh::null_vertex());
  bool should_reverse = 0;
  for (int i = 0; i < n; i++) {
    const Face_index fi = sm.add_face(indices[i], indices[(i+1)%n], center_index);
    if (fi == Surface_mesh::null_face()) {
      assert(i == 0);
      should_reverse = 1;
      break;
    }
//    bunches_indices[fi] = -1;
  }
  
  if (should_reverse) {

  auto cp = indices;
  std::reverse(cp.begin(), cp.end());
  for (int i = 0; i < n; i++) {
    const Face_index fi = sm.add_face(cp[i], cp[(i+1)%n], center_index);
    assert(fi != Surface_mesh::null_face());
  //  bunches_indices[fi] = -1;
  }
//end reversed
  }
}
*/

void Vessel::add_fake_stub_spec_no_center(const Point & center, const std::vector<Vertex_index> & indices, Surface_mesh & sm)
{
/*old var
  Face_index fi = sm.add_face(indices);
  if (fi == Surface_mesh::null_face()) {
    auto x = indices;
    std::reverse(x.begin(), x.end());
    fi = sm.add_face(x);
  }
  assert(fi != Surface_mesh::null_face());
  //bunches_indices[fi] = -1;
//end old var
*/
  //new var inserts triangles plus center!
  const int n = indices.size();
//  const Vertex_index center_index = sm.add_vertex(center);
//  assert(center_index != Surface_mesh::null_vertex());
  bool should_reverse = 0;
  for (int i = 2; i < n; i++) {
    const Face_index fi = sm.add_face(indices[i], indices[i-1], indices[0]);
    if (fi == Surface_mesh::null_face()) {
      assert(i == 2);
      should_reverse = 1;
      break;
    }
//    bunches_indices[fi] = -1;
  }
  
  if (should_reverse) {

  auto cp = indices;
  std::reverse(cp.begin(), cp.end());
  for (int i = 2; i < n; i++) {
    const Face_index fi = sm.add_face(cp[i], cp[i-1], cp[0]);
    assert(fi != Surface_mesh::null_face());
  //  bunches_indices[fi] = -1;
  }
//end reversed
  }
}

void Vessel::add_stub_spec(const Point & center, const std::vector<Vertex_index> & indices, Surface_mesh & sm)
{
/*old var
  Face_index fi = sm.add_face(indices);
  if (fi == Surface_mesh::null_face()) {
    auto x = indices;
    std::reverse(x.begin(), x.end());
    fi = sm.add_face(x);
  }
  assert(fi != Surface_mesh::null_face());
  //bunches_indices[fi] = -1;
//end old var
*/
  //new var inserts triangles plus center!
  const int n = indices.size();
  const Vertex_index center_index = sm.add_vertex(center);
  assert(center_index != Surface_mesh::null_vertex());
  bool should_reverse = 0;
  for (int i = 0; i < n; i++) {
    const Face_index fi = sm.add_face(indices[i], indices[(i+1)%n], center_index);
    if (fi == Surface_mesh::null_face()) {
      assert(i == 0);
      should_reverse = 1;
      break;
    }
    faces_on_stubs.push_back(fi);
//    bunches_indices[fi] = -1;
  }
  
  if (should_reverse) {

  auto cp = indices;
  std::reverse(cp.begin(), cp.end());
  for (int i = 0; i < n; i++) {
    const Face_index fi = sm.add_face(cp[i], cp[(i+1)%n], center_index);
    assert(fi != Surface_mesh::null_face());
    faces_on_stubs.push_back(fi);
  //  bunches_indices[fi] = -1;
  }
//end reversed
  }
}


void Vessel::add_stub(const Point & center, const std::vector<Vertex_index> & indices)
{
/*old var
  Face_index fi = vm.add_face(indices);
  if (fi == Surface_mesh::null_face()) {
    auto x = indices;
    std::reverse(x.begin(), x.end());
    fi = vm.add_face(x);
  }
  assert(fi != Surface_mesh::null_face());
  bunches_indices[fi] = -1;
end old var*/

  //new var inserts triangles plus center!
  const int n = indices.size();
  const Vertex_index center_index = vm.add_vertex(center);
  assert(center_index != Surface_mesh::null_vertex());
  bool should_reverse = 0;
  for (int i = 0; i < n; i++) {
    const Face_index fi = vm.add_face(indices[i], indices[(i+1)%n], center_index);
    if (fi == Surface_mesh::null_face()) {
      assert(i == 0);
      should_reverse = 1;
      break;
    }
    bunches_indices[fi] = -1;
  }
  
  if (should_reverse) {

  auto cp = indices;
  std::reverse(cp.begin(), cp.end());
  for (int i = 0; i < n; i++) {
    const Face_index fi = vm.add_face(cp[i], cp[(i+1)%n], center_index);
    assert(fi != Surface_mesh::null_face());
    bunches_indices[fi] = -1;
  }
//end reversed
  }
}

void Vessel::gen_initial_points(Network & net, C3t3 & c3t3)
{//simply inserts vm points
 //todo: insert all points!!! (plus inside and stub centers)
  typedef typename C3t3::Triangulation       Tr;
  typedef typename Tr::Weighted_point        Weighted_point;
//  typedef typename Tr::Bare_point            Bare_point;
//  typedef typename Tr::Segment               Segment_3;
//  typedef typename Tr::Geom_traits::Vector_3 Vector_3;
  typedef typename Tr::Vertex_handle         Vertex_handle;
  
  Tr& tr = c3t3.triangulation();
  typename Tr::Geom_traits::Construct_weighted_point_3 p2wp =
    tr.geom_traits().construct_weighted_point_3_object();

  typename Mesh_domain::Is_in_domain is_in_dom =
      orig_domain->is_in_domain_object();

  for (const Vertex_index& vi: vm.vertices()) {
      const Point & point = vm.points()[vi];
      const Weighted_point pi = p2wp(EK_to_K()(point));
      auto os = is_in_dom(EK_to_K()(point));
      Subdomain_index a_d = (os ? *os : 0);
      Surface_patch_index index_v =
          (vessel_index < a_d ? Surface_patch_index(vessel_index, a_d) :
                                Surface_patch_index(a_d, vessel_index));

      Vertex_handle v = tr.insert(pi);
      // `v` could be null if `pi` is hidden by other vertices of `tr`.
      CGAL_assertion(v != Vertex_handle());
      c3t3.set_dimension(v, 2); // by construction, points are on surface
      c3t3.set_index(v, Index(index_v));
  }
  std::cout << "OKE" << std::endl; 
  //now insert non-mono nodes and inner poly vertices as inner point
  for (auto &n: net.nodes) {
    const int sz = n.points_id.size();
    if (sz < 3) continue;
    const Point node_to_insert = net.get_Point(n.points_id[0]);

    //now we'll find middle point
/*we dont need this anymore
    FT x = 0, y = 0, z = 0;
    for (int i = 0; i < sz; i++) {
      const Polyline & poly = *n.polylines_it[i];
      if (n.is_first[i]) {
        x += poly.point_start.p.x();
        y += poly.point_start.p.y();
        z += poly.point_start.p.z();
      } else {
        x += poly.point_end.p.x();
        y += poly.point_end.p.y();
        z += poly.point_end.p.z();
      }
    }
    const Point node_to_insert(x/sz, y/sz, z/sz);
*/
    Vertex_handle v = tr.insert(p2wp(EK_to_K()(node_to_insert)));
    CGAL_assertion(v != Vertex_handle());
    c3t3.set_dimension(v, 3); // these points are inside vessel domain
    c3t3.set_index(v, Index(vessel_index));
  }
  std::cout << "DOKI" << std::endl;
  for (auto &poly: net.polylines) {
    for (auto point_i = std::next(poly.points_id.begin()); point_i != std::prev(poly.points_id.end()); ++point_i) {
      Vertex_handle v = tr.insert(p2wp(EK_to_K()(net.get_Point(*point_i))));
      CGAL_assertion(v != Vertex_handle());
      c3t3.set_dimension(v, 3); // these points are inside vessel domain
      c3t3.set_index(v, Index(vessel_index));
    }
    //dont forget about centers of convex hull facets
/*we dont need this anymore
    if (!poly.is_start_stub) {
      Vertex_handle v = tr.insert(p2wp(EK_to_K()(poly.point_start.p)));
      CGAL_assertion(v != Vertex_handle());
      c3t3.set_dimension(v, 3); // these points are inside vessel domain
      c3t3.set_index(v, Index(vessel_index));
    }
    if (!poly.is_end_stub) {
      Vertex_handle v = tr.insert(p2wp(EK_to_K()(poly.point_end.p)));
      CGAL_assertion(v != Vertex_handle());
      c3t3.set_dimension(v, 3); // these points are inside vessel domain
      c3t3.set_index(v, Index(vessel_index));
    }
*/
  }

  std::cout << "  " << tr.number_of_vertices() << " initial points." << std::endl;
  if ( c3t3.triangulation().dimension() != 3 ) {
    std::cout << "  not enough points: triangulation.dimension() == "
              << c3t3.triangulation().dimension() << std::endl;
    assert(0);
  }
}


void gen_base_points
(const Point_n_Rad& p, const Plane & plane, const int & n, std::vector<Point> &res)
{
  Vector v1 = plane.base1();
  v1/=CGAL::sqrt(v1.squared_length());
  Vector v2 = plane.base2();
  v2/=CGAL::sqrt(v2.squared_length());
//  assert(std::abs(v1.squared_length() - 1) < 1e-6);
//  assert(std::abs(v2.squared_length() - 1) < 1e-6);
  double alpha = 2*M_PI/n;
  double a = 0;
  for (int i = 0; i < n; i++) {
    res.push_back(p.p + p.r*(cos(a)*v1 + sin(a)*v2));
    a += alpha;
  }
}


struct Point_for_ch
: public OPoint
{
public:
 Vertex_index vi;//it represent element in vm
 int tube_id;//according to the node info
 Point_for_ch(const OPoint & p, const Vertex_index & vi, const int & tube_id)
 : OPoint(p), vi(vi), tube_id(tube_id)
 {}
 Point_for_ch()
 : OPoint(), vi(Surface_mesh::null_vertex()), tube_id(-1)
 {}
 Point_for_ch(const OPoint &p)//dirty hack
 : OPoint(p), vi(Surface_mesh::null_vertex()), tube_id(-1)
 {}
 Point_for_ch & operator=(const Point_for_ch & other)
 {
   if (this != &other) {
     OPoint::operator=(other);
     vi = other.vi;
     tube_id = other.tube_id;
   }
   return *this;
 }
};

#include "New_CH_traits.h"


void Vessel::process_convex_hull(const Node & node)
{
  std::vector<Point_for_ch> ch_points;
  //fill ch_points
  for (unsigned i = 0; i < node.polylines_it.size(); i++) {
    auto & poly = *(node.polylines_it[i]);
    bool is_start = node.is_first[i];//(poly.points_id[0] == node.points_id[i] ? 1: 0);
    const std::vector<Point> & pp = (is_start ?
                                     poly.points_plane_start :
                                     poly.points_plane_end);
    const std::vector<Vertex_index> & vis = (is_start ?
                                             poly.points_start_indices :
                                             poly.points_end_indices);
    for (unsigned j = 0; j < pp.size(); j++) {
      assert(vm.is_border(vis[j]));
      Surface_mesh::Halfedge_index halfedge = vm.halfedge(vis[(j+1)%pp.size()], vis[j]);
      assert(halfedge != Surface_mesh::Halfedge_index());
      assert(vm.is_border(halfedge) || vm.is_border(vm.opposite(halfedge)));
      std::cout << pp[j] << " F " << vis[j]  << " R " << i << std::endl;
      ch_points.emplace_back(EK_to_K()(pp[j]), vis[j], i);
    }
  }
  const unsigned inserted_num = ch_points.size();
  typedef CGAL::Surface_mesh<Point_for_ch> SM4ch;
  SM4ch sm4ch;
  CGAL::convex_hull_3(ch_points.begin(), ch_points.end(), sm4ch, CGALOL::CH_traits_SM4ch<K>());
  //check if all facets from bases present
  //std::cout << "Inserted: " << inserted_num << " ON: " << sm4ch.number_of_vertices() << std::endl;
  assert(inserted_num == sm4ch.number_of_vertices());

  //now for all facets that are not in tubes bases
  //hint: these facets should not contain all 3 points of base
  /*wtf is this for
   face_iterator fb, fe;
   for(boost::tie(fb, fe) = faces(sm); fb != fe; ++fb) {

   }*/
  typedef SM4ch::Vertex_index vi_SM4ch;
/*useless
  for (auto &ff : sm4ch.faces()) {
//    std::cout << ff << std::endl;
    std::vector<Point_for_ch> f_p;
    BOOST_FOREACH(vi_SM4ch vism,vertices_around_face(sm4ch.halfedge(ff), sm4ch)) {
      //std::cout << sm.point(vd) << std::endl;
      f_p.push_back(sm4ch.point(vism));
    }
    assert(f_p.size() == 3);//assume it's a simplicial complex
  assert(f_p[0].tube_id >= 0 &&
         f_p[1].tube_id >= 0 &&
         f_p[2].tube_id >= 0);

    if (f_p[0].tube_id == f_p[1].tube_id && f_p[0].tube_id == f_p[2].tube_id) {
      std::cout << "skipping face from tube#"<< f_p[0].tube_id << std::endl;
      continue;
    } else {
      std::cout << "inserting..." << std::endl;
      assert(f_p[0].vi != Surface_mesh::null_vertex() &&
             f_p[1].vi != Surface_mesh::null_vertex() &&
             f_p[2].vi != Surface_mesh::null_vertex());
      assert(vm.is_valid(f_p[0].vi));
      assert(vm.is_valid(f_p[1].vi));
      assert(vm.is_valid(f_p[2].vi));
     std::cout << f_p[0] << " F " << f_p[0].vi  << " R " << f_p[0].tube_id << std::endl;
      std::cout << f_p[1] << " F " << f_p[1].vi  << " R " << f_p[1].tube_id << std::endl;
       std::cout << f_p[2] << " F " << f_p[2].vi  << " R " << f_p[2].tube_id << std::endl;
 
      Face_index fi = vm.add_face(f_p[0].vi, f_p[1].vi, f_p[2].vi);
      //assert(fi != Surface_mesh::null_face());
      //bunches_indices[fi] = -1;
      if (fi == Surface_mesh::null_face())
        fi = vm.add_face(f_p[2].vi, f_p[1].vi, f_p[0].vi);
      if (fi == Surface_mesh::null_face())
        fi = vm.add_face(f_p[1].vi, f_p[0].vi, f_p[2].vi);
      if (fi == Surface_mesh::null_face())
        fi = vm.add_face(f_p[0].vi, f_p[2].vi, f_p[1].vi);
      if (fi == Surface_mesh::null_face())
        fi = vm.add_face(f_p[1].vi, f_p[2].vi, f_p[0].vi);
      if (fi == Surface_mesh::null_face())
        fi = vm.add_face(f_p[2].vi, f_p[0].vi, f_p[1].vi);
   
      if (fi == Surface_mesh::null_face())
        std::cout << "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE" <<std::endl;
    }
  }*/

  //we have halfedge data structure
  //surface mesh doesn't hide it properly
  std::vector<std::vector<Point_for_ch>> three_different_tubes;
  for (auto &ff : sm4ch.faces()) {
    std::vector<Point_for_ch> f_p;
    BOOST_FOREACH(vi_SM4ch vism,vertices_around_face(sm4ch.halfedge(ff), sm4ch)) {
      f_p.push_back(sm4ch.point(vism));
    }
    assert(f_p.size() == 3);//assume it's a simplicial complex
    const int a = f_p[0].tube_id;
    const int b = f_p[1].tube_id;
    const int c = f_p[2].tube_id;
    if (a == b && b == c) {
      std::cout << "skipping face from tube#"<< a << std::endl;
      continue;
    } else if (a == b || a == c || b == c ) {
      //insert now!
      Face_index fi = vm.add_face(f_p[0].vi, f_p[1].vi, f_p[2].vi);
      if (fi == Surface_mesh::null_face()) //reverse order
        fi = vm.add_face(f_p[1].vi, f_p[0].vi, f_p[2].vi);
/* useless checks
      if (fi == Surface_mesh::null_face()) {
        std::cout << "SHIT: " << std::endl;
        std::cout << f_p[0] << " F " << f_p[0].vi  << " R " << f_p[0].tube_id << std::endl;
        std::cout << f_p[1] << " F " << f_p[1].vi  << " R " << f_p[1].tube_id << std::endl;
        std::cout << f_p[2] << " F " << f_p[2].vi  << " R " << f_p[2].tube_id << std::endl;
     if ( a == b) {
      Surface_mesh::Halfedge_index halfedge = vm.halfedge(f_p[0].vi, f_p[1].vi);
      assert(halfedge != Surface_mesh::Halfedge_index());
      assert(vm.is_border(halfedge) || vm.is_border(vm.opposite(halfedge)));

      Surface_mesh::Halfedge_index halfedge2 = vm.halfedge(f_p[1].vi, f_p[2].vi);
      if (halfedge2 != Surface_mesh::Halfedge_index())
        assert(vm.is_border(halfedge2) || vm.is_border(vm.opposite(halfedge2)));

      Surface_mesh::Halfedge_index halfedge3 = vm.halfedge(f_p[2].vi, f_p[0].vi);
      if (halfedge3 != Surface_mesh::Halfedge_index())
        assert(vm.is_border(halfedge3) || vm.is_border(vm.opposite(halfedge3)));
     }
     if ( a == c) {
      Surface_mesh::Halfedge_index halfedge = vm.halfedge(f_p[0].vi, f_p[2].vi);
      assert(halfedge != Surface_mesh::Halfedge_index());
      assert(vm.is_border(halfedge) || vm.is_border(vm.opposite(halfedge)));

      Surface_mesh::Halfedge_index halfedge2 = vm.halfedge(f_p[1].vi, f_p[2].vi);
      if (halfedge2 != Surface_mesh::Halfedge_index())
        assert(vm.is_border(halfedge2) || vm.is_border(vm.opposite(halfedge2)));

      Surface_mesh::Halfedge_index halfedge3 = vm.halfedge(f_p[1].vi, f_p[0].vi);
      if (halfedge3 != Surface_mesh::Halfedge_index())
        assert(vm.is_border(halfedge3) || vm.is_border(vm.opposite(halfedge3)));
     }
     if ( c == b) {
      Surface_mesh::Halfedge_index halfedge = vm.halfedge(f_p[2].vi, f_p[1].vi);
      assert(halfedge != Surface_mesh::Halfedge_index());
      assert(vm.is_border(halfedge) || vm.is_border(vm.opposite(halfedge)));

      Surface_mesh::Halfedge_index halfedge2 = vm.halfedge(f_p[0].vi, f_p[2].vi);
      if (halfedge2 != Surface_mesh::Halfedge_index())
        assert(vm.is_border(halfedge2) || vm.is_border(vm.opposite(halfedge2)));

      Surface_mesh::Halfedge_index halfedge3 = vm.halfedge(f_p[1].vi, f_p[0].vi);
      if (halfedge3 != Surface_mesh::Halfedge_index())
        assert(vm.is_border(halfedge3) || vm.is_border(vm.opposite(halfedge3)));
     }

      }*/
      assert(fi != Surface_mesh::null_face());
      bunches_indices[fi] = -1;
    } else if (a != b && a != c && b != c) {
      three_different_tubes.push_back(f_p);
    } else {
      assert(0);
    }
  }
  for (auto &f_p: three_different_tubes) {
    Face_index fi = vm.add_face(f_p[0].vi, f_p[1].vi, f_p[2].vi);
    if (fi == Surface_mesh::null_face()) //reverse order
      fi = vm.add_face(f_p[1].vi, f_p[0].vi, f_p[2].vi);
    assert(fi != Surface_mesh::null_face());
    bunches_indices[fi] = -1;
  }
}



Plane find_ellipse_plane(const Point_n_Rad &prev_center,
                         const Point_n_Rad &new_center,
                         const Point_n_Rad &next_center,
                         FT &coef)
{//consider two cones built around prev_center, new_center and new_center, next_center spheres resp.
 //these two cones intersection is made of two ellipses
 //this function returns plane of the one ellipse suited for purpose of pipe construction
 //and set coef for next calculations
  
  const FT sqdist1 = CGAL::squared_distance(new_center.p, prev_center.p);
  Vector e1 = (new_center.p - prev_center.p)/CGAL::sqrt(sqdist1);
 
  const FT sqdist2 = CGAL::squared_distance(next_center.p, new_center.p);
  Vector e2 = (next_center.p - new_center.p)/CGAL::sqrt(sqdist2);
  
  Vector oe1 = e2-e1 - CGAL::scalar_product(e2-e1,e1)*e1;
  oe1/=CGAL::sqrt(oe1.squared_length());
  Vector oe2 = e2-e1 - CGAL::scalar_product(e2-e1,e2)*e2;
  oe2/=CGAL::sqrt(oe2.squared_length());
  
  const double & R1 = prev_center.r;
  const double & R2 = new_center.r;
  const double & R3 = next_center.r;
  
  const FT coef1 = (R2-R1)*(R2-R1) / sqdist1;
  const FT coef2 = (R3-R2)*(R3-R2) / sqdist2;

  if (R2 > R1) e1 = -e1;
  if (R3 > R2) e2 = -e2;

  const Vector v1 = CGAL::sqrt(coef1)*e1 + CGAL::sqrt(1-coef1)*oe1;
  const Vector v2 = CGAL::sqrt(coef2)*e2 + CGAL::sqrt(1-coef2)*oe2;
  const Point I = new_center.p + R2 / (1 + CGAL::scalar_product(v1, v2)) * (v1+v2);
  
  const Vector v1r = CGAL::sqrt(coef1)*e1 - CGAL::sqrt(1-coef1)*oe1;
  const Vector v2r = CGAL::sqrt(coef2)*e2 - CGAL::sqrt(1-coef2)*oe2;
  const Point II = new_center.p + R2 / (1 + CGAL::scalar_product(v1r, v2r)) * (v1r+v2r);
  
  //dont forget about coef
  coef = coef1;
  return Plane(I, II, I + CGAL::orthogonal_vector(prev_center.p, new_center.p, next_center.p));
}


//for linear programming
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/MP_Float.h>
typedef CGAL::MP_Float ET;
//end for linear programming

//the scariest function here
//
//
//
//
void Vessel::construct_vessel(Network & net, bool is_new_mode)
{
  netp = &net;
  //im tired of this stuff with ends
  for (auto &n: net.nodes) {
    assert(n.points_id.size() == n.polylines_it.size());
    for (unsigned i = 0; i < n.points_id.size(); i++) {
      Polyline &poly = *n.polylines_it[i];
      if (n.points_id[i] == poly.points_id[0])
        n.is_first.push_back(1);
      else if (n.points_id[i] == poly.points_id.back())
        n.is_first.push_back(0);
      else
        assert(0);
    }
  }

  //we dont need it anymore
//  if (is_new_mode) {
  //  net.prepare_for_new_mode();
 // }

  //first create planes of convex hulls near bifurcation zones
  //or just single plane for mono nodes
  for (auto &n: net.nodes) {
    if (n.points_id.size() == 1) {
      //mono node
      //i.e. create just plane orthogonal to the segment
      Polyline & poly = *(n.polylines_it[0]);
      if (n.is_first[0]) {
        poly.point_start = net.points[poly.points_id.front()];
        poly.plane_start = Plane(poly.point_start.p,
                               net.get_Point(poly.points_id[1]) - poly.point_start.p);
        poly.plane_start_ready = 1;
        poly.is_start_stub = 1;
      } else {
        poly.point_end = net.points[poly.points_id.back()];
        const int prelast_i = poly.points_id[poly.points_id.size()-2];
        poly.plane_end = Plane(poly.point_end.p,
                            poly.point_end.p - net.get_Point(prelast_i));
        poly.plane_end_ready = 1;
        poly.is_end_stub = 1;
      }
     //end mono node case
    } else if (n.points_id.size() > 2) {
std::cout << "SOLVING OPTIM PROBLEM " << std::endl;
      //solve optimization problem
      //for any pair of unitary normals n1 and n2:
      //find d1,d2 >= 0, so
      //plane 1 is through Node+d1*n1 and orthogonal to n1
      //similar to plane 2
      //minimize sum of d_i

      // program and solution types
      typedef CGAL::Quadratic_program<double> Program;
      typedef CGAL::Quadratic_program_solution<ET> Solution;
      Program lp (CGAL::SMALLER);//, true, 0, false, 0); 
      const int n_size = n.points_id.size();
      //n_size unknowns
     
      //we should just find two normals and two radii for each block of matrix
      std::vector<Vector> normals;
      std::vector<double> radii;
      std::vector<double> dist;//for debug;
      for (int i = 0; i < n_size; i++) {
        Vector v;
        double r;
        const Polyline & poly = *(n.polylines_it[i]);
        if (n.is_first[i]) {
          r = net.points[poly.points_id[1]].r;//(net.points[poly.points_id[0]].r + net.points[poly.points_id[1]].r)/2;
          v = net.get_Point(poly.points_id[1]) - net.get_Point(poly.points_id[0]);
        } else {
          int node_index = poly.points_id.size() - 1;
         // r = (net.points[poly.points_id[node_index]].r +
           //    net.points[poly.points_id[node_index-1]].r)/2;
           r = net.points[poly.points_id[node_index-1]].r;
           v = net.get_Point(poly.points_id[node_index-1]) - 
              net.get_Point(poly.points_id[node_index]);
        }
        dist.push_back(CGAL::to_double(CGAL::sqrt(v.squared_length())));
        v /= CGAL::sqrt(v.squared_length());
        normals.push_back(v);
        radii.push_back(r);
      }

      //minimize sum of d_i so
      for (int i = 0; i < n_size; i++)
        lp.set_c(i, 1.0/dist[i]);
 

      int rr = 0;
      for (int i = 0; i < n_size; i++) {
        const Vector &vi = normals[i];
        const double &ri = radii[i];
  //      if (n.polylines_it[i]->points_id.size() == 3)
    //      lp.set_u(i, true, dist[i]/2);//for upper bound
      //  else
        lp.set_u(i, true, dist[i]);//for upper bound
        for (int j = i+1; j < n_size; j++) {
          const Vector &vj = normals[j];
          const double &rj = radii[j];
          const double ccos = CGAL::to_double(CGAL::scalar_product(vi, vj));
          const double ssin = CGAL::to_double(CGAL::sqrt(1 - ccos*ccos));// > 0 ? sqrt(1 - ccos*ccos) : 0);
    //lp.set_a(X, 0,  1); lp.set_a(Y, 0, 1); lp.set_b(0, 7);  //  x + y  <= 7
    //lp.set_a(X, 1, -1); lp.set_a(Y, 1, 2); lp.set_b(1, 4);  // -x + 2y <= 4
          lp.set_a(i, rr, ccos); lp.set_a(j, rr,   -1/*+0.1*/); lp.set_b(rr++, -ssin*(ri));
          lp.set_a(i, rr,   -1/*+0.1*/); lp.set_a(j, rr, ccos); lp.set_b(rr++, -ssin*(rj));
        }
      }
      
     // Solution s = CGAL::solve_nonnegative_linear_program(lp, ET());
     // assert (s.solves_nonnegative_linear_program(lp));
     
  Solution s = CGAL::solve_linear_program(lp, ET());
  assert (s.solves_linear_program(lp));
      //now get solution somehow
      //and set info in polylines!!!
      int i = 0;
      Point node_p = net.get_Point(n.points_id[0]);
      for (auto res_d = s.variable_values_begin(); res_d != s.variable_values_end(); ++res_d) {
        Polyline & poly = *(n.polylines_it[i]);
        if (n.is_first[i]) {
          poly.point_start.p = node_p + CGAL::to_double(*res_d) * normals[i];
          poly.point_start.r = radii[i];
 
          std::cout << CGAL::to_double(*res_d) << " "<< dist[i] << std::endl; 
          assert(CGAL::to_double(*res_d) < dist[i]);

          poly.plane_start = Plane(poly.point_start.p, normals[i]);
          poly.is_start_stub = 0;
          poly.plane_start_ready = 1;
        } else {
          poly.point_end.p = node_p + CGAL::to_double(*res_d) * normals[i];
          poly.point_end.r = radii[i];
       
          std::cout << CGAL::to_double(*res_d) << " "<< dist[i] << std::endl; 
          assert(CGAL::to_double(*res_d) < dist[i]);
          
          poly.plane_end = Plane(poly.point_end.p, -normals[i]);
          poly.is_end_stub = 0;
          poly.plane_end_ready = 1;
        }
        i++;
      }
      
      //end of optimization
    } else {
      //size() == 2
      //must be error
      assert(0);
    }
  }

  //add property to facets
  bool created;
  boost::tie(bunches_indices, created) =
         vm.add_property_map<Face_index, int>("f:bunch", -2);
  //-2 - default uninitialized value
  //-1 - no bunch
  //i >= 0 - index of bunch
  assert(created);

  //then propagate edges of pipes
  //currently two modes
  if (is_new_mode) {
//new mode
  for (auto &p : net.polylines) {
    std::cout << "new polyline" << std::endl;
    assert(p.plane_start_ready && p.plane_end_ready);
    
    //1. generate points on plane_start
    gen_base_points(p.point_start, p.plane_start, 6, p.points_plane_start);
    //insert in surface_mesh
    for (auto &point: p.points_plane_start) {
      Vertex_index vi = vm.add_vertex(point);
      assert(vi != Surface_mesh::null_vertex());
      p.points_start_indices.push_back(vi);
    }
    Pbase a(p.points_plane_start, p.points_start_indices, p.point_start);
    Pbase b;
    Pbase *start_base_i = &a;
    Pbase *end_base_i = &b;
    //2. move points forward
    for (unsigned i = 1; ; i++) {
      //i is for next point in polyline!
      if (i == p.points_id.size() - 1) {
        //3.stop at plane_end
        std::cout << "finishing this polyline" << std::endl;
        end_base_i->plane = p.plane_end;
        end_base_i->center = p.point_end;
        end_base_i->new_mode_propagate_from(*start_base_i);
        insert_faces_in_surface(*start_base_i, *end_base_i);
        p.points_end_indices = end_base_i->indices;
        p.points_plane_end = end_base_i->points;
        break;
      } else {
        //keep going
        std::cout << "making segment" << std::endl;
        const Point_n_Rad & new_center = net.points[p.points_id[i]];
        const Point_n_Rad & next_center = (i != p.points_id.size() - 2
                                           ? net.points[p.points_id[i+1]]
                                           : p.point_end);
        const Point_n_Rad & prev_center = start_base_i->center;
        //now find ellipse plane
        FT coef = -1;
        end_base_i->plane = find_ellipse_plane(prev_center, new_center, next_center, coef);
        end_base_i->center = new_center;
        std::cout << "ellipse plane found" << std::endl;
        end_base_i->new_mode_propagate_from(*start_base_i, coef);
        std::cout << "insertinh faces in surface" << std::endl;
        insert_faces_in_surface(*start_base_i, *end_base_i);
      }
      start_base_i->reset();
      std::swap(start_base_i, end_base_i);
    }
  }


//end new mode
  } else {
//old mode

  for (auto &p : net.polylines) {
    std::cout << "new polyline" << std::endl;
    assert(p.plane_start_ready && p.plane_end_ready);
    
    //1. generate points on plane_start
    gen_base_points(p.point_start, p.plane_start, 6, p.points_plane_start);
    //insert in surface_mesh
    for (auto &point: p.points_plane_start) {
      Vertex_index vi = vm.add_vertex(point);
      assert(vi != Surface_mesh::null_vertex());
      p.points_start_indices.push_back(vi);
    }
    Pbase a(p.points_plane_start, p.points_start_indices, p.point_start);
    Pbase b;
    Pbase *start_base_i = &a;
    Pbase *end_base_i = &b;
    //2. move points forward
    for (unsigned i = 1; ; i++) {
      //i is for next point in polyline!
      if (i == p.points_id.size() - 1) {
        //3.stop at plane_end
        std::cout << "finishing this polyline" << std::endl;
        end_base_i->plane = p.plane_end;
        end_base_i->center = p.point_end;
        end_base_i->propagate_from(*start_base_i);
        insert_faces_in_surface(*start_base_i, *end_base_i);
        p.points_end_indices = end_base_i->indices;
        p.points_plane_end = end_base_i->points;
        break;
      } else {
        //keep going
        std::cout << "making segment" << std::endl;
        const Point_n_Rad & new_center = net.points[p.points_id[i]];
        const Point_n_Rad & next_center = (i != p.points_id.size() - 2
                                           ? net.points[p.points_id[i+1]]
                                           : p.point_end);
        const Point_n_Rad & prev_center = start_base_i->center;
        const Vector norm = (new_center.p-prev_center.p)/CGAL::sqrt((new_center.p-prev_center.p).squared_length())
                          + (next_center.p-new_center.p)/CGAL::sqrt((next_center.p-new_center.p).squared_length());

        end_base_i->plane = Plane(new_center.p, norm);
        end_base_i->center = new_center;
        end_base_i->propagate_from(*start_base_i);
        insert_faces_in_surface(*start_base_i, *end_base_i);
      }
      start_base_i->reset();
      std::swap(start_base_i, end_base_i);
    }
  }
//end of old mode
  }
  //end propagating edges


  assert(vm.is_valid());
  std::ofstream dump("dump_surface_mesh.off");
  dump << vm;
  dump.close();

  //now find convex hulls
  //or just build stubs
  //
/*
  for (auto &n: net.nodes) {
      std::cout << "Making stub" << std::endl;
      //currently build only stub
      for (unsigned i = 0; i < n.points_id.size(); i++) {

      //start test
      std::cout << "stub for poly # " << i << std::endl;
      Polyline &poly = *n.polylines_it[i];
      if (n.is_first[i]) {
        for (auto &p: poly.points_plane_start)
          std::cout << p << std::endl;
        std::cout << "C: " << poly.point_start.p << std::endl;
        std::cout << "complanar tests : " <<
          CGAL::coplanar(poly.points_plane_start[0],
                         poly.points_plane_start[1],
                         poly.points_plane_start[2],
                         poly.points_plane_start[3]
                         )
         << 
           CGAL::coplanar(poly.points_plane_start[2],
                         poly.points_plane_start[3],
                         poly.points_plane_start[4],
                         poly.points_plane_start[5]
                         )
         <<
            CGAL::coplanar(poly.points_plane_start[4],
                         poly.points_plane_start[5],
                         poly.points_plane_start[0],
                         poly.points_plane_start[1]
                         )
 
         << std::endl;
        assert(poly.points_start_indices.size() == 6);
        const Face_index fi = vm.add_face(poly.points_start_indices);
        assert(fi != Surface_mesh::null_face());
        bunches_indices[fi] = -1;
      } else {
        const Face_index fi = vm.add_face(poly.points_end_indices);
        assert(fi != Surface_mesh::null_face());
        bunches_indices[fi] = -1;
      }
      //end test
      }
  }
*/
//true 

  for (auto &n: net.nodes) {
    if (n.points_id.size() == 1) {
      std::cout << "Making stub" << std::endl;
      //currently build only stub
      Polyline &poly = *n.polylines_it[0];
      if (n.is_first[0]) {
        assert(poly.is_start_stub);
        add_stub(poly.point_start.p, poly.points_start_indices);
      } else {
        assert(poly.is_end_stub);
        add_stub(poly.point_end.p, poly.points_end_indices);
      }
    //end mono node case
    } else {
      std::cout << "making ch" << std::endl;
      //find convex hull
      process_convex_hull(n);
    }//end convex hull case
  }

  //thats all
  //now check correctness
  assert(vm.is_valid());
  std::ofstream dump2("dump_surface_mesh_after.off");
  dump2 << vm;
  dump2.close();


//this works  for simplicial complexes
#ifdef NOT_PREC_ARITHM
  bool intersecting = PMP::does_self_intersect(vm,
      PMP::parameters::vertex_point_map(get(CGAL::vertex_point, vm)));
  std::cout
    << (intersecting ? "There are self-intersections." : "There is no self-intersection.")
    << std::endl;
  assert(!intersecting);
#endif
//if you use AABB_oracle then it's time to init it
  oracle = AABB_oracle(vm);
  std::cout << "Oracle complete" << std::endl;
  //inside =  CGAL::Side_of_triangle_mesh<Surface_mesh, K>(vm);

}
/**
 *
 *
 * end of the scariest function*/









/*
 *
 *

//new_construct_vessel --freezed alpha
//
//
void Vessel::new_construct_vessel(Network & net)
{
	std::cout << "NEW" << std::endl;
  netp = &net;
  //main idea:
  //iterate over all polylines, create individual vessel for each of them
  //and merge it!



  //add property to facets
  bool created;
  boost::tie(bunches_indices, created) =
         vm.add_property_map<Face_index, int>("f:bunch", -2);
  //-2 - default uninitialized value
  //-1 - no bunch
  //i >= 0 - index of bunch
  assert(created);


  for (auto &poly : net.polylines) {
    std::cout << "new polyline" << std::endl;
 //build plane in the beginning 
    poly.point_start = net.points[poly.points_id.front()];
    poly.plane_start = Plane(poly.point_start.p,
                            net.get_Point(poly.points_id[1]) - poly.point_start.p);
    poly.plane_start_ready = 1;
    poly.is_start_stub = 1;
     
 //build plane in the end
    poly.point_end = net.points[poly.points_id.back()];
    const int prelast_i = poly.points_id[poly.points_id.size()-2];
    poly.plane_end = Plane(poly.point_end.p,
                        poly.point_end.p - net.get_Point(prelast_i));
    poly.plane_end_ready = 1;
    poly.is_end_stub = 1;
//done
    assert(poly.plane_start_ready && poly.plane_end_ready);
    
    //1. generate points on plane_start
    gen_base_points(poly.point_start, poly.plane_start, 6, poly.points_plane_start);
    //insert in surface_mesh
    for (auto &point: poly.points_plane_start) {
      Vertex_index vi = vm.add_vertex(point);
      assert(vi != Surface_mesh::null_vertex());
      poly.points_start_indices.push_back(vi);
    }
    Pbase a(poly.points_plane_start, poly.points_start_indices, poly.point_start);
    Pbase b;
    Pbase *start_base_i = &a;
    Pbase *end_base_i = &b;
    //2. move points forward
    for (unsigned i = 1; ; i++) {
      //i is for next point in polyline!
      if (i == poly.points_id.size() - 1) {
        //3.stop at plane_end
        std::cout << "finishing this polyline" << std::endl;
        end_base_i->plane = poly.plane_end;
        end_base_i->center = poly.point_end;
        end_base_i->new_mode_propagate_from(*start_base_i);
        insert_faces_in_surface(*start_base_i, *end_base_i);
        poly.points_end_indices = end_base_i->indices;
        poly.points_plane_end = end_base_i->points;
        break;
      } else {
        //keep going
        std::cout << "making segment" << std::endl;
        const Point_n_Rad & new_center = net.points[poly.points_id[i]];
        const Point_n_Rad & next_center = (i != poly.points_id.size() - 2
                                           ? net.points[poly.points_id[i+1]]
                                           : poly.point_end);
        const Point_n_Rad & prev_center = start_base_i->center;
        //now find ellipse plane
        FT coef = -1;
        end_base_i->plane = find_ellipse_plane(prev_center, new_center, next_center, coef);
        end_base_i->center = new_center;
        std::cout << "ellipse plane found" << std::endl;
        end_base_i->new_mode_propagate_from(*start_base_i, coef);
        std::cout << "insertinh faces in surface" << std::endl;
        insert_faces_in_surface(*start_base_i, *end_base_i);
      }
      start_base_i->reset();
      std::swap(start_base_i, end_base_i);
    }
  }
  //end propagating edges


  assert(vm.is_valid());
  std::ofstream dump("dump_surface_mesh.off");
  dump << vm;
  dump.close();

  for (auto &poly : net.polylines) {
    add_stub(poly.point_start.p, poly.points_start_indices);
    add_stub(poly.point_end.p, poly.points_end_indices);
  }
  //thats all
  //now check correctness
  assert(vm.is_valid());
  std::ofstream dump2("dump_surface_mesh_after.off");
  dump2 << vm;
  dump2.close();
 

//dont forget to uncomment!!!1
//this works  for simplicial complexes
#ifdef NOT_PREC_ARITHM
  bool intersecting = PMP::does_self_intersect(vm,
      PMP::parameters::vertex_point_map(get(CGAL::vertex_point, vm)));
  std::cout
    << (intersecting ? "There are self-intersections." : "There is no self-intersection.")
    << std::endl;
  assert(!intersecting);
#endif

//if you use AABB_oracle then it's time to init it
  oracle = AABB_oracle(vm);
  std::cout << "Oracle complete" << std::endl;
  //inside =  CGAL::Side_of_triangle_mesh<Surface_mesh, K>(vm);

}
*/






#include <CGAL/Polygon_mesh_processing/corefinement.h>

#include <CGAL/Polygon_mesh_processing/refine.h>
#include <CGAL/Polygon_mesh_processing/fair.h>
#include <CGAL/Polygon_mesh_processing/stitch_borders.h>
#include <CGAL/Polygon_mesh_processing/remesh.h>








// Simplification function
#include <CGAL/Surface_mesh_simplification/edge_collapse.h>
// Stop-condition policy
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Count_stop_predicate.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/LindstromTurk_cost.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/LindstromTurk_placement.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Bounded_normal_change_placement.h>
namespace SMS = CGAL::Surface_mesh_simplification ;
void SM_Simply(Surface_mesh & surface_mesh)
{
  // This is a stop predicate (defines when the algorithm terminates).
  // In this example, the simplification stops when the number of undirected edges
  // left in the surface mesh drops below the specified number (1000)
  SMS::Count_stop_predicate<Surface_mesh> stop(num_halfedges(surface_mesh)/4 - 1);
     
typedef SMS::Bounded_normal_change_placement<SMS::LindstromTurk_placement<Surface_mesh> > Placement;
  // This the actual call to the simplification algorithm.
  // The surface mesh and stop conditions are mandatory arguments.
  // The index maps are needed because the vertices and edges
  // of this surface mesh lack an "id()" field.
  int k = SMS::edge_collapse( surface_mesh,
                      stop);/*,
                      CGAL::parameters::vertex_index_map(get(CGAL::vertex_external_index,surface_mesh))
                        .halfedge_index_map  (get(CGAL::halfedge_external_index  ,surface_mesh))
                        .get_cost (SMS::LindstromTurk_cost<Surface_mesh>())
                        .get_placement(Placement())
                      );*/
  std::cout << k << " edges collapsed" << std::endl;
}




#include <map>
// Midpoint placement policy
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Midpoint_placement.h>
//Placement wrapper
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Constrained_placement.h>
// Stop-condition policy
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Count_stop_predicate.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Edge_length_cost.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Edge_length_stop_predicate.h>
//
// BGL property map which indicates whether an edge is marked as non-removable
//
struct Border_is_constrained_edge_map{
  typedef boost::graph_traits<Surface_mesh>::edge_descriptor key_type;
  typedef bool value_type;
  typedef value_type reference;
  typedef boost::readable_property_map_tag category;
  
  //const Surface_mesh* sm_ptr;
  //Border_is_constrained_edge_map(const Surface_mesh& sm)
  //  : sm_ptr(&sm)
  //{}
 // friend bool get(Border_is_constrained_edge_map m, const key_type& edge) {
 //   return CGAL::is_border(edge, *m.sm_ptr);
  //}
  const std::vector<key_type> * c_edges;
  Border_is_constrained_edge_map(const std::vector<key_type> & ce)
    : c_edges(&ce)
  {
    assert(!c_edges->empty());
  }
  friend bool get(Border_is_constrained_edge_map m, const key_type& edge) {
    return (std::find(m.c_edges->begin(), m.c_edges->end(), edge) != m.c_edges->end());
  }
};




//try to use one of them or one after the other i dk 
typedef SMS::Bounded_normal_change_placement<SMS::Midpoint_placement<Surface_mesh>> Pl1;
typedef SMS::Constrained_placement<Pl1, Border_is_constrained_edge_map > Pl_Mid;

typedef SMS::Bounded_normal_change_placement<SMS::LindstromTurk_placement<Surface_mesh>> Pl2;
typedef SMS::Constrained_placement<Pl2, Border_is_constrained_edge_map > Pl_Lind;

typedef SMS::Edge_length_cost<Surface_mesh> Len_cost;
typedef SMS::LindstromTurk_cost<Surface_mesh> Lind_cost;

template <class Placement, class Cost, class Stop>
void SM_Complex(Surface_mesh & sm, const std::vector<Edge_index> * c_edges, const Stop & stop)
{
  // map used to check that constrained_edges and the points of its vertices
  // are preserved at the end of the simplification
  std::map<Edge_index,std::pair<Point, Point> > constrained_edges;
  for (const Edge_index & ei: *c_edges) {
      constrained_edges[ei]=std::make_pair( sm.point(sm.vertex(ei, 0)),
                                            sm.point(sm.vertex(ei, 1)) );
  }
  //SMS::Count_stop_predicate<Surface_mesh> stop(nff);
//  SMS::Edge_length_stop_predicate<double> stop(len_lim);
  Border_is_constrained_edge_map bem(*c_edges);
  // This the actual call to the simplification algorithm.
  // The surface mesh and stop conditions are mandatory arguments.
  // The index maps are needed because the vertices and edges
  // of this surface mesh lack an "id()" field.
//  int r = SMS::edge_collapse
 //           (sm
  //          ,stop
   //          ,CGAL::parameters::vertex_index_map(get(CGAL::vertex_external_index,sm))
    //                           .halfedge_index_map  (get(CGAL::halfedge_external_index  ,sm))
     //                          .edge_is_constrained_map(bem)
      //                         .get_placement(Placement(bem))
       //     );
  int r = SMS::edge_collapse
            (sm
            ,stop
             ,CGAL::parameters::edge_is_constrained_map(bem)
                               .get_placement(Placement(bem))
			       .get_cost(Cost())
            );


  std::cout << "\nFinished...\n" << r << " edges removed.\n";
  // now check!

  for (const Edge_index & ei: *c_edges) {
      assert(sm.is_valid(ei));
      assert( constrained_edges[ei] ==
              std::make_pair( sm.point(sm.vertex(ei, 0)),
                              sm.point(sm.vertex(ei, 1)) ));
  }
}









//visitor for corefinement
//
//with bunch transfer
class Visitor_with_bt: public CGAL::Polygon_mesh_processing::Corefinement::Default_visitor<Surface_mesh>
{
public:
  void after_face_copy(Face_index f_src, Surface_mesh & sm_src, Face_index f_tgt, Surface_mesh & sm_tgt)
  {
    //just save info about bunches
   bool is_ok;
   Surface_mesh::Property_map<Face_index,int>  pm_src, pm_tgt;
  
   boost::tie(pm_src, is_ok) = sm_src.property_map<Face_index, int>("f:bunch");
   assert(is_ok);
   boost::tie(pm_tgt, is_ok) = sm_tgt.property_map<Face_index, int>("f:bunch");
   assert(is_ok);
   
   pm_tgt[f_tgt] = pm_src[f_src];

  }
};	
//visitor done


#include <CGAL/Polygon_mesh_processing/compute_normal.h>

void Vessel::fair_global(const std::string & dump_file)
{
  Network & net = *netp;
  double bbbb;
  double fc;
  std::cout << "enter rad_coef( >= 1), fc(small plz): ";
  std::cin >> bbbb >> fc;
  std::cout << "fairing starts" << std::endl;
  //now lets walk over bifurcations and fair
  for (auto &n: net.nodes) {
    if (n.points_id.size() > 2) {
      const Point & np = net.points[n.points_id[0]].p;
      std::vector<Vertex_index> nearest_points;
      double bif_rad = 0;
      for (int &i: n.points_id) {
        if (bif_rad < net.points[i].r)
		bif_rad = net.points[i].r;
      }
      bif_rad*=bbbb;
      for (Vertex_index & vi : vm.vertices()) {
        const Point & p = vm.point(vi);
        if (CGAL::to_double(CGAL::squared_distance(np, p)) < bif_rad*bif_rad)
          nearest_points.push_back(vi);
      }
      int kek = -1;
      assert(nearest_points.size() > 0);
      for (auto &vi: nearest_points) {
        const Vector normal = CGAL::Polygon_mesh_processing::compute_vertex_normal(vi, vm);
        vm.point(vi) += kek*fc*normal;
	kek = (kek > 0 ? -1 : 1);
      }
     // bool success = CGAL::Polygon_mesh_processing::fair(vm, nearest_points,CGAL::Polygon_mesh_processing::parameters::fairing_continuity(fc));
    //  std::cout << "Fairing : " << (success ? "succeeded" : "failed") << std::endl;
    }
  }

  std::cout << "global wtf complete" << std::endl;
  assert(vm.is_valid());
  std::ofstream dump2(dump_file + ".off");
  dump2 << vm;
  dump2.close();

  bool intersecting = PMP::does_self_intersect(vm,
      PMP::parameters::vertex_point_map(get(CGAL::vertex_point, vm)));
  std::cout
    << (intersecting ? "There are self-intersections." : "There is no self-intersection.")
    << std::endl;



}



template <class Placement, class Cost>
void Vessel::deal_with_optimizer_global(const std::string & dump_file)
{
 Network & net = *netp;
 //SM_Complex global
 double rad_coef;
  std::cout << "enter rad_coef( >= 1): ";
  std::cin >> rad_coef;
  std::vector<Edge_index> v_e;
  double len_lim;
  std::cout << "enter global len_lim: ";
  std::cin >> len_lim;
  //SMS::Count_stop_predicate<Surface_mesh> stop(nff);
  typedef typename SMS::Edge_length_stop_predicate<double> Stop;
  Stop stop(len_lim);
 

//or maybe store biggest rad in Node?
  std::vector<Point_n_Rad> bif_nodes;
  for (auto &n: net.nodes) {
    if (n.points_id.size() > 2) {
      bif_nodes.push_back(net.points[n.points_id[0]]);
      //std::cout << np << std::endl;
      double biggest_rad = 0;
      for (int &i: n.points_id) {
        if (biggest_rad < net.points[i].r)
		biggest_rad = net.points[i].r;
      }
      bif_nodes.back().r = rad_coef*biggest_rad;
    }
  }



  for (auto & ei: vm.edges()) {
	//first a small hack: check if an edge is on stub
	//we dont want to bother em
	bool is_on_stub = 0;
	const Face_index fiN0 = vm.face(vm.halfedge(ei, 0)),
	                 fiN1 = vm.face(vm.halfedge(ei, 1));
        for (auto &fi: faces_on_stubs) {
          if (!vm.is_valid(fi)) continue;
          if (fi == fiN0 || fi == fiN1) {
		  is_on_stub = 1;
		  break;
          }
	}
	if (is_on_stub) {
		v_e.push_back(ei);
		continue;
	}
	const Point & p1 = vm.point(vm.vertex(ei, 0));
	const Point & p2 = vm.point(vm.vertex(ei, 1));
	bool is_any_inside_bif_zone = 0;
        for (auto &bif : bif_nodes) {
          if (CGAL::to_double(CGAL::squared_distance(bif.p, p1)) < bif.r*bif.r ||
	      CGAL::to_double(CGAL::squared_distance(bif.p, p2)) < bif.r*bif.r)
          {
	    is_any_inside_bif_zone = 1;
	    break;
          }
	}
	if (!is_any_inside_bif_zone)
		v_e.push_back(ei);
        
   }



  std::cout << "DEF: " << vm.number_of_edges()  << " " <<  v_e.size() << std::endl;
  
  assert(!v_e.empty());
  SM_Complex<Placement, Cost, Stop>(vm, &v_e, stop);

  assert(vm.is_valid());
  std::ofstream dump2(dump_file + ".off");
  dump2 << vm;
  dump2.close();

  bool intersecting = PMP::does_self_intersect(vm,
      PMP::parameters::vertex_point_map(get(CGAL::vertex_point, vm)));
  std::cout
    << (intersecting ? "There are self-intersections." : "There is no self-intersection.")
    << std::endl;

}


template <class Placement, class Cost>
void Vessel::deal_with_optimizer_local(const std::string & dump_file)
{
 Network & net = *netp;
 //SM_Complex local
 double rad_coef;
  std::cout << "enter rad_coef( >= 1): ";
  std::cin >> rad_coef;
double part;
  std::cout << "enter multiplier before radii for lenght limit so all edges longer are safe: ";
  std::cin >> part;
//  typedef typename SMS::Count_stop_predicate<Surface_mesh> Stop;
  typedef typename SMS::Edge_length_stop_predicate<double> Stop;
 
  for (auto &n: net.nodes) {
    if (n.points_id.size() > 2) {
      std::vector<Edge_index> c_edges;
      const Point & np = net.points[n.points_id[0]].p;
      double bif_rad = 0;
      double real_radii; // for len stop criteria
      for (int &i: n.points_id) {
        if (bif_rad < net.points[i].r)
		bif_rad = net.points[i].r;
      }
      real_radii = bif_rad;
      bif_rad*=rad_coef;
       
      for (auto & ei : vm.edges() ) {
	bool is_on_stub = 0;
	const Face_index fiN0 = vm.face(vm.halfedge(ei, 0)),
	                 fiN1 = vm.face(vm.halfedge(ei, 1));
        for (auto &fi: faces_on_stubs) {
          if (!vm.is_valid(fi)) continue;
          if (fi == fiN0 || fi == fiN1) {
		  is_on_stub = 1;
		  break;
          }
	}
	if (is_on_stub) {
		c_edges.push_back(ei);
		continue;
	}

	const Point & p1 = vm.point(vm.vertex(ei, 0));
	const Point & p2 = vm.point(vm.vertex(ei, 1));
        if (CGAL::to_double(CGAL::squared_distance(np, p1)) < bif_rad*bif_rad ||
	    CGAL::to_double(CGAL::squared_distance(np, p2)) < bif_rad*bif_rad)
	{
	  continue; 
        }
	c_edges.push_back(ei);
      }
      SM_Complex<Placement, Cost, Stop>(vm, &c_edges, Stop(part*real_radii));//vm.number_of_edges() - part*c_edges.size()));
    }
  }

  assert(vm.is_valid());
  std::ofstream dump2(dump_file + ".off");
  dump2 << vm;
  dump2.close();

  bool intersecting = PMP::does_self_intersect(vm,
      PMP::parameters::vertex_point_map(get(CGAL::vertex_point, vm)));
  std::cout
    << (intersecting ? "There are self-intersections." : "There is no self-intersection.")
    << std::endl;

}






//new_construct_vessel -- beta
//
//
void Vessel::new_construct_vessel(Network & net)
{
  netp = &net;
  //main idea:
  //iterate over all polylines, create individual vessel for each of them
  //and merge it!
  for (auto &n: net.nodes) {
    assert(n.points_id.size() == n.polylines_it.size());
    const bool is_stub = (n.points_id.size() == 1);
    for (unsigned i = 0; i < n.points_id.size(); i++) {
      Polyline &poly = *n.polylines_it[i];
      if (n.points_id[i] == poly.points_id[0]) {
        n.is_first.push_back(1);
        poly.is_start_stub = is_stub;
      } else if (n.points_id[i] == poly.points_id.back()) {
        n.is_first.push_back(0);
	poly.is_end_stub = is_stub;
      } else {
        assert(0);
      }
    }
  }


//vm is main Surface_mesh
//tmp_sm -- tmp S_m
    //add property to facets
    bool created;
    boost::tie(bunches_indices, created) =
           vm.add_property_map<Face_index, int>("f:bunch", -1); //attention!
    //-2 - default uninitialized value
    //-1 - no bunch
    //i >= 0 - index of bunch
    assert(created);
    std::vector<int> overo; //for debug

  double coeff_for_bif;
  std::cout << "ENTER coeff_for_bif (like 0.5 or smaller): ";
  std::cin >> coeff_for_bif;
bool first_time = 1;
  for (auto &poly : net.polylines) {
    Surface_mesh tmp_sm;
    Surface_mesh::Property_map<Face_index,int> tmp_bunches_indices;
    //add property to facets
    bool created;
    boost::tie(tmp_bunches_indices, created) =
           tmp_sm.add_property_map<Face_index, int>("f:bunch", -1); //attention!
    //-2 - default uninitialized value
    //-1 - no bunch
    //i >= 0 - index of bunch
    assert(created);


    std::cout << "new polyline" << std::endl;
 //build plane in the beginning
    if (poly.is_start_stub) {
      poly.point_start = net.points[poly.points_id.front()];
    } else {//or hack: move the plane a bit further to cover bifurcation zone a bit better
      const Point_n_Rad & rr = net.points[poly.points_id.front()];
    
      Vector v1 = rr.p - net.points[poly.points_id[1]].p;
      v1 = v1/std::sqrt(v1.squared_length());
      poly.point_start.p = rr.p + rr.r*coeff_for_bif*v1;
      poly.point_start.r = rr.r;
    }

    poly.plane_start = Plane(poly.point_start.p,
                            net.get_Point(poly.points_id[1]) - poly.point_start.p);
    poly.plane_start_ready = 1;
    
 //build plane in the end, the same thing
    const int prelast_i = poly.points_id[poly.points_id.size()-2];
    if (poly.is_end_stub) {
      poly.point_end = net.points[poly.points_id.back()];
    } else {
      const Point_n_Rad & ff = net.points[poly.points_id.back()];
      Vector v2 = ff.p - net.points[poly.points_id.rbegin()[1]].p;
      v2 = v2/std::sqrt(v2.squared_length());
      poly.point_end.p = ff.p + ff.r*coeff_for_bif*v2;
      poly.point_end.r = ff.r;
    }
    
    poly.plane_end = Plane(poly.point_end.p,
                        poly.point_end.p - net.get_Point(prelast_i));
    poly.plane_end_ready = 1;
//done
    assert(poly.plane_start_ready && poly.plane_end_ready);
    
    //1. generate points on plane_start
    gen_base_points(poly.point_start, poly.plane_start, 6, poly.points_plane_start);
    //insert in surface_mesh
    for (auto &point: poly.points_plane_start) {
      Vertex_index vi = tmp_sm.add_vertex(point);
      assert(vi != Surface_mesh::null_vertex());
      poly.points_start_indices.push_back(vi);
    }
    Pbase a(poly.points_plane_start, poly.points_start_indices, poly.point_start);
    Pbase b;
    Pbase *start_base_i = &a;
    Pbase *end_base_i = &b;
    //2. move points forward
    for (unsigned i = 1; ; i++) {
      //i is for next point in polyline!
      if (i == poly.points_id.size() - 1) {
        //3.stop at plane_end
        std::cout << "finishing this polyline" << std::endl;
        end_base_i->plane = poly.plane_end;
        end_base_i->center = poly.point_end;
        end_base_i->new_mode_propagate_from(*start_base_i);
        insert_faces_in_surface_spec(*start_base_i, *end_base_i, tmp_sm, tmp_bunches_indices);
        poly.points_end_indices = end_base_i->indices;
        poly.points_plane_end = end_base_i->points;
        break;
      } else {
        //keep going
        std::cout << "making segment" << std::endl;
        const Point_n_Rad & new_center = net.points[poly.points_id[i]];
        const Point_n_Rad & next_center = (i != poly.points_id.size() - 2
                                           ? net.points[poly.points_id[i+1]]
                                           : poly.point_end);
        const Point_n_Rad & prev_center = start_base_i->center;
        //now find ellipse plane
        FT coef = -1;
        end_base_i->plane = find_ellipse_plane(prev_center, new_center, next_center, coef);
        end_base_i->center = new_center;
        std::cout << "ellipse plane found" << std::endl;
        end_base_i->new_mode_propagate_from(*start_base_i, coef);
        std::cout << "inserting faces in surface" << std::endl;
        insert_faces_in_surface_spec(*start_base_i, *end_base_i, tmp_sm, tmp_bunches_indices);
      }
      start_base_i->reset();
      std::swap(start_base_i, end_base_i);
    }
    if (poly.is_start_stub)
      add_stub_spec(poly.point_start.p, poly.points_start_indices, tmp_sm);
    else
      add_fake_stub_spec_no_center(poly.point_start.p, poly.points_start_indices, tmp_sm);
 
    if (poly.is_end_stub)
      add_stub_spec(poly.point_end.p, poly.points_end_indices, tmp_sm);
    else
      add_fake_stub_spec_no_center(poly.point_end.p, poly.points_end_indices, tmp_sm);
 
    //ok tmp_sm is ready to be merged
    //
    if (first_time) {
      first_time = 0;
      vm += tmp_sm;
      std::cout << "just first time" << std::endl;
    } else {
      bool res = CGAL::Polygon_mesh_processing::corefine_and_compute_union(vm, tmp_sm, vm,  CGAL::Polygon_mesh_processing::parameters::visitor(Visitor_with_bt()) );
      if (!res) std::cout << "BAD COREFINEMENT!111111111111" << std::endl;
      assert(res);
    }
      int d = 0;
      for (auto &fi: vm.faces()) {
        if (bunches_indices[fi] > -1)
	      d++;
      }
      overo.push_back(d);



  }
  //end propagating edges


  std::cout << "PRINTING numbers of main facets" << std::endl;
  for (auto &r :overo)
	  std::cout << r << std::endl;
  std::cout << "DONE" << std::endl;

//new!
//lets try to get property map!
  bool is_ok;
  boost::tie(bunches_indices, is_ok) = vm.property_map<Face_index, int>("f:bunch");
  std::vector<std::string> props = vm.properties<Face_index> ();
  for (auto &s : props)
	  std::cout << s << std::endl;
  std::cout << "ENDOFPROPS" << std::endl;
  assert(is_ok);
//end new!

  assert(vm.is_valid());
  std::ofstream dump("dump_surface_mesh.off");
  dump << vm;
  dump.close();
 
//this works  for simplicial complexes
#ifdef NOT_PREC_ARITHM
  bool intersecting1 = PMP::does_self_intersect(vm,
      PMP::parameters::vertex_point_map(get(CGAL::vertex_point, vm)));
  std::cout
    << (intersecting1 ? "There are self-intersections." : "There is no self-intersection.")
    << std::endl;
  assert(!intersecting1);
#endif




/*
  double rad_cc;
  std::cout << "Enter rad_coef(>=1): ";
  std::cin >> rad_cc;
  std::cout << "merging CH starts" << std::endl;
  //now lets walk over bifurcations and merge convex hulls to smoothen
  for (auto &n: net.nodes) {
    if (n.points_id.size() > 2) {
      const Point & np = net.points[n.points_id[0]].p;
      std::cout << np << std::endl;
      std::vector<Point> nearest_points;
      double biggest_rad = 0;
      for (int &i: n.points_id) {
        if (biggest_rad < net.points[i].r)
		biggest_rad = net.points[i].r;
      }
      std::cout << biggest_rad << std::endl;
      biggest_rad*=biggest_rad;
      for(Vertex_index & vi : vm.vertices()) {
        const Point & p = vm.point(vi);
        if (CGAL::to_double(CGAL::squared_distance(np, p)) < rad_cc*biggest_rad)
          nearest_points.push_back(p);
      }
      assert(nearest_points.size() > 0);
      Surface_mesh ch_res;
      std::cout << "ch eval" << std::endl;
      CGAL::convex_hull_3(nearest_points.begin(), nearest_points.end(), ch_res);
      std::cout << "corefinement start" << std::endl;
      bool res;
      try{
      res = CGAL::Polygon_mesh_processing::corefine_and_compute_union(vm, ch_res, vm);
      }
      catch (...) {
	 std::cout << "BAD CATCH EXCEPTION DURING COREFINEMENT" << std::endl;
         break;
      }
      if (!res) std::cout << "BAD CONVEX_HULL COREFINEMENT!111111111" << std::endl;
      std::cout << "coref ok" << std::endl;
    }
  }
  std::cout << "merging CH complete" << std::endl;
*/




  std::cout << "Before stitching : " << std::endl;
  std::cout << "\t Number of vertices  :\t" << vm.number_of_vertices() << std::endl;
  std::cout << "\t Number of halfedges :\t" << vm.number_of_halfedges() << std::endl;
  std::cout << "\t Number of facets    :\t" << vm.number_of_faces() << std::endl;
  CGAL::Polygon_mesh_processing::stitch_borders(vm);
  std::cout << "Stitching done : " << std::endl;
  std::cout << "\t Number of vertices  :\t" << vm.number_of_vertices() << std::endl;
  std::cout << "\t Number of halfedges :\t" << vm.number_of_halfedges() << std::endl;
  std::cout << "\t Number of facets    :\t" << vm.number_of_faces() << std::endl;

/*doesnt work well globally
  std::cout << "Enter do_project, num_of_relax, num_of_iters, target_len, is_relaxed" << std::endl;
  bool do_proj, is_relaxed;
  int num_relax, num_iter;
  double target_len;
  std::cin >> do_proj >> num_relax >> num_iter >> target_len >> is_relaxed;
  CGAL::Polygon_mesh_processing::isotropic_remeshing(vm.faces(), target_len, vm,  CGAL::Polygon_mesh_processing::parameters::do_project(do_proj)
		                                               .number_of_relaxation_steps(num_relax)
		                                               .number_of_iterations(num_iter)
							       .relax_constraints(is_relaxed));


*/

  std::cout << "Enter coef(so target_len = coef*big_rad), rad_buf(so inside if < rad_buf*big_rad), do_project(0,1), num_of_relax(>=1), num_of_iters(>=1), is_relaxed" << std::endl;
  bool do_proj, is_relaxed;
  int num_relax, num_iter;
  double coef, rad_buf;
  std::cin >> coef >> rad_buf >> do_proj >> num_relax >> num_iter >> is_relaxed;
 
  for (auto &n: net.nodes) {
    if (n.points_id.size() > 2) {
      const Point & np = net.points[n.points_id[0]].p;
      //std::cout << np << std::endl;
      std::vector<Vertex_index> nearest_points;
      double big_rad = 0;
      for (int &i: n.points_id) {
        if (big_rad < net.points[i].r)
		big_rad = net.points[i].r;
      }
   //   std::cout << biggest_rad << std::endl;
      double save_rad = big_rad;
      big_rad*=rad_buf;
       
      std::vector<Face_index> nearest_faces;
      for (Face_index & fi : vm.faces()) {
        Surface_mesh::Halfedge_index hf = vm.halfedge(fi);
//	std::cout << "new face" << std::endl;
	int cc = 0;
        for (Surface_mesh::Halfedge_index hi : halfedges_around_face(hf, vm))
        {
          const Point & p = vm.point(target(hi, vm));
//	  std::cout << p << std::endl;
          if (CGAL::to_double(CGAL::squared_distance(np, p)) > big_rad*big_rad)
		  break;
	  cc++;
        }
	if (cc == 3)
		nearest_faces.push_back(fi);
      }
      assert(nearest_faces.size() > 0);
      //now collect nearest facets 
//      for (Face_index & fi: nearest_faces) {
//	vm.remove_face(vm.face(vm.opposite(vm.halfedge(vi))));
        //nearest_faces.insert(vm.face(vm.opposite(vm.halfedge(vi))));
//	try{
// CGAL::Euler::remove_face( vm.halfedge(fi), vm );
//	}
//	catch (...) {std::cout << "ERRR" << std::endl;}
 //     }
     try { 
       CGAL::Polygon_mesh_processing::isotropic_remeshing(nearest_faces, coef*save_rad, vm,  CGAL::Polygon_mesh_processing::parameters::do_project(do_proj)
		                                               .number_of_relaxation_steps(num_relax)
		                                               .number_of_iterations(num_iter)
							       .relax_constraints(is_relaxed));
   
     } catch (...) {
       std::cout << "EX" << std::endl;
     }
    }
  }
  //lets try
  //SM_Simply(vm);
  
  
  
  
  std::cout << "GLOBAL" << std::endl;
  std::cout << "0 - break, 1 - Mid_Lind, 2 - Lind_Lind, 3 - Mid_Len, 4 - Lind_Len, 5 - Fair: " << std::endl;
  int ch;
  std::string dumpfile("dump");
  while (1) {
    std::cin >> ch;
    switch (ch) {
	    case 1:
		    dumpfile += "_Mi";
		    deal_with_optimizer_global<Pl_Mid, Lind_cost>(dumpfile);
		    break;
	    case 2:
		    dumpfile += "_Li";
		    deal_with_optimizer_global<Pl_Lind, Lind_cost>(dumpfile);
		    break;
            case 3:
                    dumpfile += "_Me";
                    deal_with_optimizer_global<Pl_Mid, Len_cost>(dumpfile);
                    break;
            case 4:
                    dumpfile += "_Le";
                    deal_with_optimizer_global<Pl_Lind, Len_cost>(dumpfile);
                    break;
            case 5:
		    dumpfile += "_F";
		    fair_global(dumpfile);
	    default:
		    break;
     }
     if (!ch) break;
  }

  std::cout << "LOCAL" << std::endl;
  std::cout << "0 - break, 1 - Mid_Lind, 2 - Lind_Lind, 3 - Mid_Len, 4 - Lind_Len" << std::endl;
  dumpfile = "dump";
  while (1) {
    std::cin >> ch;
    switch (ch) {
	    case 1:
		    dumpfile += "_Mi";
		    deal_with_optimizer_local<Pl_Mid, Lind_cost>(dumpfile);
		    break;
	    case 2:
		    dumpfile += "_Li";
		    deal_with_optimizer_local<Pl_Lind, Lind_cost>(dumpfile);
		    break;
            case 3:
                    dumpfile += "_Me";
                    deal_with_optimizer_local<Pl_Mid, Len_cost>(dumpfile);
                    break;
            case 4:
                    dumpfile += "_Le";
                    deal_with_optimizer_local<Pl_Lind, Len_cost>(dumpfile);
                    break;
	    default:
		    break;
     }
     if (!ch) break;
  }


  assert(vm.is_valid());
  std::ofstream dump2("dump_surface_mesh_CH.off");
  dump2 << vm;
  dump2.close();

//this works  for simplicial complexes
#ifdef NOT_PREC_ARITHM
  bool intersecting = PMP::does_self_intersect(vm,
      PMP::parameters::vertex_point_map(get(CGAL::vertex_point, vm)));
  std::cout
    << (intersecting ? "There are self-intersections." : "There is no self-intersection.")
    << std::endl;
//  assert(!intersecting);
//  sometimes it fails :(
#endif

//if you use AABB_oracle then it's time to init it
  oracle = AABB_oracle(vm);
  std::cout << "Oracle complete" << std::endl;
  //inside =  CGAL::Side_of_triangle_mesh<Surface_mesh, K>(vm);
}



















#include "super_domain.h"
typedef CGAL::Super_LIMD<CGAL::Image_3, K, Vessel> Super_domain;

typedef CGAL::New_mesh_criteria_3<Tr, Super_domain, Vessel> Super_mesh_criteria;
typedef CGAL::New_mesh_facet_criteria_3<Tr, Super_domain, Vessel> Super_mesh_facet_criteria;
typedef CGAL::New_mesh_cell_criteria_3<Tr, Super_domain> Super_mesh_cell_criteria;


//additional refinement step
//1.find sliver
//2.insert refinement point right in the middle
//3.update
//4.repeat for another sliver

#include <CGAL/Mesh_3/min_dihedral_angle.h>
#include <CGAL/Mesh_3/dihedral_angle_3.h>


template<typename C3T3, typename Domain>
void
remove_slivers_near_vessels(C3T3& c3t3, Domain& domain, int vessel_index)
{
  typedef typename C3T3::Triangulation::Geom_traits K;
  typedef typename K::FT FT;

  typedef typename C3T3::Triangulation                       Tr;
  typedef typename Tr::Weighted_point                        Weighted_point;
  typedef typename Tr::Bare_point                            Bare_point;
  typedef typename Tr::Cell_handle                           Cell_handle;
  typedef typename Tr::Facet                                 Facet;
  typedef typename Tr::Vertex_handle                         Vertex_handle;
  typedef typename Weighted_point::Weight                    Weight;

//  typedef typename Tr::Geom_traits                           Gt;
//  typedef typename Gt::Tetrahedron_3                         Tetrahedron_3;

  typedef typename C3T3::Cells_in_complex_iterator           Cell_iterator;
  typedef std::vector<Facet>                                 Facet_vector;

  typedef typename C3T3::Surface_patch_index                 Surface_patch_index;
  typedef typename C3T3::Subdomain_index                     Subdomain_index;
  typedef typename C3T3::Index                               Index;

  Tr & tr_ = c3t3.triangulation();

  std::vector<Cell_handle> slivers;
  for (Cell_iterator cit = c3t3.cells_in_complex_begin();
      cit != c3t3.cells_in_complex_end();
      ++cit)
  {
    //check if all 4 points are on vessel surface

    if (c3t3.in_dimension(cit->vertex(0)) != 2 ||
        c3t3.in_dimension(cit->vertex(1)) != 2 ||
        c3t3.in_dimension(cit->vertex(2)) != 2 ||
        c3t3.in_dimension(cit->vertex(3)) != 2) continue;

    int i;
    for (i = 0; i < 4; i++) {
      Index vi = c3t3.index(cit->vertex(i));
      //get  Surface_patch_index from Index
      //check if only one Subdomain is for vessel
      if (Surface_patch_index* pi = boost::get<Surface_patch_index>( &vi ) ) {
        if (pi->first != vessel_index && pi->second != vessel_index)
          break;
//        assert(!(pi->first == 5 && pi->second == 5));
      } else {//not surface
        break;
      }
    }
    if (i != 4) continue;//it's not sliver near vessel
    
    //now find two diagonal edges
    const Bare_point p0 = cit->vertex(0)->point().point();
    const Bare_point p1 = cit->vertex(1)->point().point();
    const Bare_point p2 = cit->vertex(2)->point().point();
    const Bare_point p3 = cit->vertex(3)->point().point();


   // CGAL::abs(CGAL::Mesh_3::dihedral_angle(p1, p2, p0, p3));
   

//    const FT min_angle = 
  //    CGAL::Mesh_3::minimum_dihedral_angle(c3t3.triangulation().tetrahedron(cit));
   // if (min_angle > 1e-4) continue;
   
    const Bare_point proj = (typename K::Plane_3(p0, p1, p2)).projection(p3);
    if (CGAL::squared_distance(proj, p3) > 1e-10)
      continue;

    slivers.push_back(cit);
  }
  std::cout << "Slivers to refine: " << slivers.size() << std::endl;
  for (auto cit:slivers) {
    const Bare_point p0 = cit->vertex(0)->point().point();
    const Bare_point p1 = cit->vertex(1)->point().point();
    const Bare_point p2 = cit->vertex(2)->point().point();
    const Bare_point p3 = cit->vertex(3)->point().point();
 
    FT x = 0, y = 0, z = 0;
    for (int i = 0; i < 4; i++) {
      x += cit->vertex(i)->point().x();
      y += cit->vertex(i)->point().y();
      z += cit->vertex(i)->point().z();
    }
    const Point center(x/4, y/4, z/4);
  
    int a,b,c,d;
    if (CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0, p1, p2, p3)) > 90) {
      //diagonals are po-p1 and p2-p3
      a = 0; b = 1; c = 2; d = 3;
    } else if (CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0, p2, p1, p3)) > 90) {
      a = 0; b = 2; c = 1; d = 3;
    } else if (CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0, p3, p1, p2)) > 90) {
      a = 0; b = 3; c = 1; d = 2;
    } else {
      assert(0);
    }
//now create star
//find all cells with diagonals
    std::vector<Cell_handle> cells_to_delete;
    cells_to_delete.push_back(cit);
    typename Tr::Cell_circulator cell_circ = tr_.incident_cells(cit, a, b, cit);
    for (++cell_circ; cell_circ != cit; ++cell_circ) {
      cells_to_delete.push_back(cell_circ);
    }
    cell_circ = tr_.incident_cells(cit, c, d, cit);
    for (++cell_circ; cell_circ != cit; ++cell_circ) {
      cells_to_delete.push_back(cell_circ);
    }
 
//remove cells from c3t3  
    for (auto &c:cells_to_delete)
    {
      // Remove cell from complex
      c3t3.remove_from_complex(c);
    }

//now insert center point
  Cell_handle neigh_owner;
  int ni = -1;
  for (auto &ctd: cells_to_delete) {
    for (int i = 0; i < 4; i++) {
      Cell_handle tmp = ctd->neighbor(i);
      unsigned u;
      for (u = 0; u < cells_to_delete.size(); u++) {
        if (cells_to_delete[u] == tmp) break;
      }
      if (u == cells_to_delete.size()) {
        neigh_owner = ctd;
        ni = i;
        break;
      }
    }
    if (ni != -1) break;
  }
  assert(ni != -1);
  Vertex_handle v = tr_.insert_in_hole(Weighted_point(center, 0),
                                       cells_to_delete.begin(),
                                       cells_to_delete.end(),
                                       neigh_owner,
                                       ni);

  // Set index and dimension of v
  // it is on surface!
//    c3t3.set_index(v, 6);//for tests
//    v->set_dimension(3); //for tests
    v->set_dimension(2);

    typename Mesh_domain::Is_in_domain is_in_dom =
      domain.is_in_domain_object();
    auto os = is_in_dom(center); 
    Subdomain_index a_d = (os ? *os : 0);
     
    Surface_patch_index index_v =
        (vessel_index < a_d ? Surface_patch_index(vessel_index, a_d) :
                              Surface_patch_index(a_d, vessel_index));
    
    c3t3.set_index(v, index_v);


    
//now update info
  // Get the star of v
    std::vector<Cell_handle> incident_cells;
    tr_.incident_cells(v, std::back_inserter(incident_cells));
  // Scan tets of the star of v
    for(auto cell_it = incident_cells.begin();
        cell_it != incident_cells.end();
        ++cell_it )
    {
      assert(!tr_.is_infinite(*cell_it));
      //use domain to find real index of subdomain
      //maybe we have to deal with facets, see cgal code for this cases
      //c3t3.add_to_complex(*cell_it, 6);//tmp for tests only
      c3t3.add_to_complex(*cell_it, *domain.is_in_domain_object()(tr_.dual(*cell_it)));
    }
  }
}
