#pragma once
#define WTF_DEF


#include <CGAL/Image_3.h>
#include <CGAL/ImageIO.h>
#include <CGAL/read_vtk_image_data.h>
#include <iostream>
#include <map>

#include <vtk/vtkSmartPointer.h>
#include <vtk/vtkPoints.h>
#include <vtk/vtkCellArray.h>
#include <vtk/vtkIntArray.h>
#include <vtk/vtkType.h>
#include <vtk/vtkCellData.h>
#include <vtk/vtkImageData.h>
#include <vtk/vtkPointData.h>
#include <vtk/vtkImageReader2.h>
#include <vtk/vtkImageReader2Factory.h>
#include <vtk/vtkUnstructuredGrid.h>
#include <vtk/vtkXMLUnstructuredGridWriter.h>

#ifdef WTF_DEF
#include <vtk/vtkNIFTIImageReader.h>
#endif

#include <vtk/vtkTransform.h>
#include <vtk/vtkTransformFilter.h>

namespace CGAL_to_VTK {

template <class Point>
vtkIdType add_vtk_point(const Point& p, vtkPoints& vtk_points)
{
    return vtk_points.InsertNextPoint(CGAL::to_double(p.x()), CGAL::to_double(p.y()), CGAL::to_double(p.z()));
}

template <class Vertex_handle>
vtkIdType get_vertex_index(const Vertex_handle v, std::map<Vertex_handle, vtkIdType>& V, vtkPoints& vtk_points)
{
  std::pair<typename std::map<Vertex_handle, vtkIdType>::iterator, bool> res = V.insert(std::make_pair(v, -1));
  if (res.second)  res.first->second = add_vtk_point(res.first->first->point(), vtk_points);
  return res.first->second;
}

template <typename C3T3>
vtkUnstructuredGrid* 
output_c3t3_to_vtk(const C3T3& c3t3, vtkUnstructuredGrid* grid = 0)
{
  vtkSmartPointer<vtkPoints> vtk_points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray> vtk_cells = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkIntArray> cell_material = vtkSmartPointer<vtkIntArray>::New();

  std::map<typename C3T3::Triangulation::Vertex_handle, vtkIdType> V;
  
  cell_material->SetNumberOfComponents(1);
  cell_material->SetName("Material");
  
  for(typename C3T3::Cells_in_complex_iterator 
        cit = c3t3.cells_in_complex_begin(),
        end = c3t3.cells_in_complex_end();
      cit != end; ++cit) 
  {
    vtkIdType cell[4];
    for (int i = 0; i < 4; ++i)
      cell[i] = get_vertex_index(cit->vertex(i), V, *vtk_points);
    vtk_cells->InsertNextCell(4, cell);
    cell_material->InsertNextValue(cit->subdomain_index());
  }

  if(!grid) {
    grid = vtkUnstructuredGrid::New();
  }

  grid->SetPoints(vtk_points);
  grid->SetCells(VTK_TETRA, vtk_cells);
  grid->GetCellData()->AddArray(cell_material);
  return grid;
}

} // end namespace CGAL_to_VTK


struct VTK_to_ImageIO_type_mapper {
  WORD_KIND wordKind;
  SIGN sign;
  unsigned int wdim;
};

static const VTK_to_ImageIO_type_mapper VTK_to_ImageIO_type[VTK_ID_TYPE] =
  { { WK_UNKNOWN, SGN_UNKNOWN,  0}, //  0=VTK_VOID
    { WK_UNKNOWN, SGN_UNKNOWN,  0}, //  1=VTK_BIT
    { WK_FIXED,   SGN_SIGNED,   1}, //  2=VTK_CHAR
    { WK_FIXED,   SGN_UNSIGNED, 1}, //  3=VTK_UNSIGNED_CHAR
    { WK_FIXED,   SGN_SIGNED,   2}, //  4=VTK_SHORT
    { WK_FIXED,   SGN_UNSIGNED, 2}, //  5=VTK_UNSIGNED_SHORT
    { WK_FIXED,   SGN_SIGNED,   4}, //  6=VTK_INT
    { WK_FIXED,   SGN_UNSIGNED, 4}, //  7=VTK_UNSIGNED_INT
    { WK_FIXED,   SGN_SIGNED,   8}, //  8=VTK_LONG
    { WK_FIXED,   SGN_UNSIGNED, 8}, //  9=VTK_UNSIGNED_LONG
    { WK_FLOAT,   SGN_SIGNED,   4}, // 10=VTK_FLOAT
    { WK_FIXED,   SGN_SIGNED,   8}  // 11=VTK_DOUBLE
 };

_image *
spec_read_vtk_image_data(vtkImageData* vtk_image)
{
  if(!vtk_image)
    return NULL;

  _image* image = _initImage();
  const int* dims = vtk_image->GetDimensions();
  const double* spacing = vtk_image->GetSpacing();
  image->vectMode = VM_SCALAR;
  image->xdim = dims[0];
  image->ydim = dims[1];
  image->zdim = dims[2];
  image->vdim = 1;
  image->vx = spacing[0];
  image->vy = spacing[1];
  image->vz = spacing[2];
  //vtk_image->Update();
  image->endianness = _getEndianness();
  int vtk_type = vtk_image->GetScalarType();
  if(vtk_type == VTK_SIGNED_CHAR) vtk_type = VTK_CHAR;
  if(vtk_type < 0 || vtk_type > VTK_DOUBLE)
    vtk_type = VTK_DOUBLE;
  const VTK_to_ImageIO_type_mapper& imageio_type =
    VTK_to_ImageIO_type[vtk_type];
  image->wdim = imageio_type.wdim;
  image->wordKind = imageio_type.wordKind;
  image->sign = imageio_type.sign;
  image->data = ImageIO_alloc(dims[0]*dims[1]*dims[2]*image->wdim);
  std::cerr << "GetNumberOfTuples()=" << vtk_image->GetPointData()->GetScalars()->GetNumberOfTuples()
            << "\nimage->size()=" << dims[0]*dims[1]*dims[2]
            << "\nwdim=" << image->wdim << '\n';
  assert(vtk_image->GetPointData()->GetScalars()->GetNumberOfTuples() == dims[0]*dims[1]*dims[2]);
  vtk_image->GetPointData()->GetScalars()->ExportToVoidPointer(image->data);

  return image;
}

int WriteVTU(const std::string filename, const vtkSmartPointer<vtkUnstructuredGrid> ugrid)
{
    vtkSmartPointer<vtkXMLUnstructuredGridWriter>  writer = vtkXMLUnstructuredGridWriter::New();
    writer->SetInputData(ugrid);
    writer->SetFileName(filename.c_str());
    writer->SetDataModeToAppended();
    writer->EncodeAppendedDataOff();
    int r = writer->Write();
    writer->Delete();
    return r;
}


struct Danilov_reader_n_exporter
{
//  vtkImageReader2 * imageReader;
  vtkSmartPointer<vtkNIFTIImageReader> imageReader;

  CGAL::Image_3 nifti_reader(const char * inputFilename)
  {
    imageReader = vtkSmartPointer<vtkNIFTIImageReader>::New();

    if (!imageReader->CanReadFile(inputFilename)) {
      std::cout << "Can't read file" << std::endl;
      throw 100;
    }

    imageReader->SetFileName(inputFilename);
//    imageReader->PlanarRGBOn(); //default is off
//    imageReader->SetDataOrigin(0,0,0);

    imageReader->Update();
    double *v =   imageReader->GetDataOrigin(); 
    std::cout << "@@@@" <<v[0] << " " << v[1] << " " << v[2]<< std::endl;
//	double qfac = imageReader->GetQFac();
    vtkMatrix4x4 *qform = imageReader->GetQFormMatrix();
    vtkMatrix4x4 *sform = imageReader->GetSFormMatrix();
    if (qform)  qform->Print(std::cout);
    if (sform)  sform->Print(std::cout);
    //imageReader->Print(std::cerr);
    CGAL::Image_3 image(CGAL::read_vtk_image_data(imageReader->GetOutput()));
//    CGAL::Image_3 image(spec_read_vtk_image_data(imageReader->GetOutput()));
 
    return image;
  }


  template <typename C3T3>
  void vtu_exporter(const C3T3 & c3t3, std::string datafile)
  {
#if 0
//strangely it's doesnt work correct
    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    transform->Translate(imageReader->GetDataOrigin());

//#ifdef WTF_DEF
   // vtkNIFTIImageReader *niftiimageReader = vtkNIFTIImageReader::SafeDownCast(imageReader);
  //  if (niftiimageReader)
    ///{
	double qfac = imageReader->GetQFac();
	vtkMatrix4x4 *qform = imageReader->GetQFormMatrix();
	vtkMatrix4x4 *sform = imageReader->GetSFormMatrix();
	if (qform)  qform->Print(std::cout);
	if (sform)  sform->Print(std::cout);
	if (qform)  transform->SetMatrix(qform);
	else if (sform)  transform->SetMatrix(sform);
    //}
//#endif

//    imageReader->Delete();
    
    vtkSmartPointer<vtkUnstructuredGrid> vtkmesh = CGAL_to_VTK::output_c3t3_to_vtk(c3t3);

    vtkSmartPointer<vtkTransformFilter> transformModel = vtkSmartPointer<vtkTransformFilter>::New();
    transformModel->SetTransform(transform);
    transformModel->SetInputDataObject(vtkmesh);
    transformModel->Update();

    vtkSmartPointer<vtkUnstructuredGrid> newmesh = transformModel->GetUnstructuredGridOutput();

    if (!WriteVTU(datafile, newmesh))
    {
	  std::cout << "Failed to write file: " << datafile << std::endl;
    }

#else
    vtkSmartPointer<vtkUnstructuredGrid> vtkmesh = CGAL_to_VTK::output_c3t3_to_vtk(c3t3);
    if (!WriteVTU(datafile, vtkmesh))
    {
	  std::cout << "Failed to write file: " << datafile << std::endl;
    }
#endif
  }

  //Danilov_reader_n_exporter
};
