#pragma once

template<typename C3t3>
std::vector<int>
create_histogram(const C3t3& c3t3, double& min_value, double& max_value, int subdom_index=0)
{//collects info about dihedral angles
 //use export_histfile function to get histogram
  typedef typename C3t3::Triangulation::Bare_point Point_3;
  
  std::vector<int> histo(181,0);
  
  min_value = 180.;
  max_value = 0.;
  
	for (typename C3t3::Cells_in_complex_iterator cit = c3t3.cells_in_complex_begin() ;
       cit != c3t3.cells_in_complex_end() ;
       ++cit)
	{
		if( !c3t3.is_in_complex(cit) || (subdom_index && 
            cit->subdomain_index() != subdom_index) )
			continue;
		
#ifdef CGAL_MESH_3_DEMO_DONT_COUNT_TETS_ADJACENT_TO_SHARP_FEATURES_FOR_HISTOGRAM
    if (c3t3.in_dimension(cit->vertex(0)) <= 1
     || c3t3.in_dimension(cit->vertex(1)) <= 1
     || c3t3.in_dimension(cit->vertex(2)) <= 1
     || c3t3.in_dimension(cit->vertex(3)) <= 1)
      continue;
#endif //CGAL_MESH_3_DEMO_DONT_COUNT_TETS_ADJACENT_TO_SHARP_FEATURES_FOR_HISTOGRAM
		
		const Point_3& p0 = cit->vertex(0)->point().point();
		const Point_3& p1 = cit->vertex(1)->point().point();
		const Point_3& p2 = cit->vertex(2)->point().point();
		const Point_3& p3 = cit->vertex(3)->point().point();
		
		double a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0,p1,p2,p3)));
		histo[static_cast<int>(std::floor(a))] += 1;
    min_value = (std::min)(min_value, a);
    max_value = (std::max)(max_value, a);
    
		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0, p2, p1, p3)));
		histo[static_cast<int>(std::floor(a))] += 1;
    min_value = (std::min)(min_value, a);
    max_value = (std::max)(max_value, a);
    
		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0, p3, p1, p2)));
		histo[static_cast<int>(std::floor(a))] += 1;
    min_value = (std::min)(min_value, a);
    max_value = (std::max)(max_value, a);
    
		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p1, p2, p0, p3)));
		histo[static_cast<int>(std::floor(a))] += 1;
    min_value = (std::min)(min_value, a);
    max_value = (std::max)(max_value, a);
    
		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p1, p3, p0, p2)));
		histo[static_cast<int>(std::floor(a))] += 1;
    min_value = (std::min)(min_value, a);
    max_value = (std::max)(max_value, a);
    
		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p2, p3, p0, p1)));
		histo[static_cast<int>(std::floor(a))] += 1;
    min_value = (std::min)(min_value, a);
    max_value = (std::max)(max_value, a);
    
	}
  
	return histo;	
}
/*
template<typename C3t3>
void
mark_bad_tets(const C3t3& c3t3, int threshold, int subdom_index=0)
{
  typedef typename C3t3::Triangulation::Point Point_3;
	for (typename C3t3::Cells_in_complex_iterator cit = c3t3.cells_in_complex_begin() ;
       cit != c3t3.cells_in_complex_end() ;
       ++cit)
	{
		if( !c3t3.is_in_complex(cit) || (subdom_index && 
            cit->subdomain_index() != subdom_index) )
			continue;
		
#ifdef CGAL_MESH_3_DEMO_DONT_COUNT_TETS_ADJACENT_TO_SHARP_FEATURES_FOR_HISTOGRAM
    if (c3t3.in_dimension(cit->vertex(0)) <= 1
     || c3t3.in_dimension(cit->vertex(1)) <= 1
     || c3t3.in_dimension(cit->vertex(2)) <= 1
     || c3t3.in_dimension(cit->vertex(3)) <= 1)
      continue;
#endif //CGAL_MESH_3_DEMO_DONT_COUNT_TETS_ADJACENT_TO_SHARP_FEATURES_FOR_HISTOGRAM
		
		const Point_3& p0 = cit->vertex(0)->point();
		const Point_3& p1 = cit->vertex(1)->point();
		const Point_3& p2 = cit->vertex(2)->point();
		const Point_3& p3 = cit->vertex(3)->point();
	     	
        int tt;
		double a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0,p1,p2,p3)));
        tt = static_cast<int>(std::floor(a));
        if (tt <= threshold) cit->set_subdomain_index(3);
    
		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0, p2, p1, p3)));
        tt = static_cast<int>(std::floor(a));
        if (tt <= threshold) cit->set_subdomain_index(3);

		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0, p3, p1, p2)));
        tt = static_cast<int>(std::floor(a));
        if (tt <= threshold) cit->set_subdomain_index(3);
    
		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p1, p2, p0, p3)));
        tt = static_cast<int>(std::floor(a));
        if (tt <= threshold) cit->set_subdomain_index(3);
    
		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p1, p3, p0, p2)));
        tt = static_cast<int>(std::floor(a));
        if (tt <= threshold) cit->set_subdomain_index(3);
    
		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p2, p3, p0, p1)));
        tt = static_cast<int>(std::floor(a));
        if (tt <= threshold) cit->set_subdomain_index(3);
    
	}
}
*/
template <typename C3t3>
void export_histfile(const char *file, C3t3 & c3t3, int subdom=0)
{//use it to get dihedral angle histogram
  double min_angle, max_angle;
  std::vector<int> histogram_dihedral_angle = create_histogram(c3t3, min_angle, max_angle, subdom);
  std::ofstream hist(file);
  int f = 0;
  hist << "%" << min_angle << " " << max_angle << std::endl;
  for (auto &i: histogram_dihedral_angle) {
    hist << f << " " << i << std::endl; f++;
    //hist << "(" << f << ", " << i << ")" << std::endl; f++;
  }
}

template <typename C3t3>
void export_q_factors(const char *file, C3t3 & c3t3, int subdom=0, int to_mark=0)
{//quality histogram
 const int d = 100;
 std::vector<int> res(d+1,0);
 const double coef = 1832.8207768;//6^4*sqrt(2)
 double min = 2, max = -1;
 std::ofstream ff(file);
 std::vector<double> vr;
 for (auto a = c3t3.cells_in_complex_begin(); 
        a!= c3t3.cells_in_complex_end(); a++)
 {
   if (subdom && a->subdomain_index() != subdom) continue;

    auto v0 = a->vertex(0)->point().point();
    auto v1 = a->vertex(1)->point().point();
    auto v2 = a->vertex(2)->point().point();
    auto v3 = a->vertex(3)->point().point();
     double P = sqrt(CGAL::squared_distance(v0,v1)) + sqrt(CGAL::squared_distance(v0,v2))
              + sqrt(CGAL::squared_distance(v0,v3)) + sqrt(CGAL::squared_distance(v1,v2))
              + sqrt(CGAL::squared_distance(v1,v3)) + sqrt(CGAL::squared_distance(v2,v3));
     double V = abs(CGAL::volume(v0,v1,v2,v3));
     double res = coef*V/(P*P*P);
     if (to_mark && res > 0.2) a->set_subdomain_index(4);
     min = (res < min) ? res : min;
     max = (res > max) ? res : max;
     vr.push_back(res);
 }
 double ln = 1.0, step = ln / d;
 for (auto &r:vr) {
   res[std::floor(r/step)]++;
 }
 ff << "%/num: " << vr.size() << std::endl;
 ff << "%" <<  min << " " << max << std::endl;
 std::cout << "NUM: " << vr.size() << std::endl;
 std::cout << "MIN: " << min << std::endl;
 int i = 0;
 for (auto &r:res) {
//  ff << "(" << i*step << ", " << r  << ")"<< std::endl;
 
  ff << i*step << " " << r << std::endl;
  i++;
 }
}


/*
void export_q_factors(const char *file, C3t3 & c3t3, int subdom=0)
{
 const int d = 20;
 std::vector<int> res(20,0);
 const double coef = 1832.8207768;//6^4*sqrt(2)
 double min = 2, max = -1;
 std::ofstream ff(file);
 std::vector<double> vr;
 for (auto a = c3t3.cells_in_complex_begin(); 
        a!= c3t3.cells_in_complex_end(); a++)
 {
   if (subdom && a->subdomain_index() != subdom) continue;

    auto v0 = a->vertex(0)->point();
    auto v1 = a->vertex(1)->point();
    auto v2 = a->vertex(2)->point();
    auto v3 = a->vertex(3)->point(); 
     double P = sqrt(CGAL::squared_distance(v0,v1)) + sqrt(CGAL::squared_distance(v0,v2))
              + sqrt(CGAL::squared_distance(v0,v3)) + sqrt(CGAL::squared_distance(v1,v2))
              + sqrt(CGAL::squared_distance(v1,v3)) + sqrt(CGAL::squared_distance(v2,v3));
     double V = abs(CGAL::volume(v0,v1,v2,v3));
     double res = coef*V/(P*P*P);
     min = (res < min) ? res : min;
     max = (res > max) ? res : max;
     vr.push_back(res);
 }
 double ln = max - min, step = ln / d;
 for (auto &r:vr) {
   res[std::floor((r - min)/step)]++;
 }

 ff << min << " " << max << std::endl;
 int i = 0;
 for (auto &r:res) {
  ff << min + i*step << " " << r << std::endl;
  i++;
 }
}
*/
