#pragma once
#include <CGAL/Labeled_image_mesh_domain_3.h>

namespace CGAL {
//labeled image plus vessel will be super class
template<class Image, class BGT, class Vessel_surface>
class Super_LIMD
: public Labeled_image_mesh_domain_3 <Image, BGT>
{
public:
  typedef Labeled_image_mesh_domain_3<Image, BGT> Base;
  typedef typename Base::FT FT;

  Base cleaned_domain;//domain with cleaned vessels for domain queries
  Vessel_surface & vs;

//constructor
  Super_LIMD(const Image& image,
             Vessel_surface & vs,
             const Image& cleaned_image,
             const FT& error_bound = FT(1e-3))
  : Base(image, error_bound), cleaned_domain(cleaned_image, error_bound), vs(vs)
  {}


//  using Base::bbox;
//  virtual ~Super_LIMD() {}
private:
  // Disabled copy constructor & assignment operator
  typedef Super_LIMD<Image, BGT, Vessel_surface> Self;
  Super_LIMD(const Self& src);
  Self& operator=(const Self& src);

/**
 *
 * Function f must take his values into N.
 * Let p be a Point.
 *  - f(p)=0 means that p is outside domain.
 *  - f(p)=a, a!=0 means that p is inside subdomain a.
 *
 *  Any boundary facet is labelled <a,b>, a<b, where a and b are the
 *  tags of it's incident subdomain.
 *  Thus, a boundary facet of the domain is labelled <0,b>, where b!=0.
 */
public:
  /// Geometric object types
  typedef typename BGT::Point_3    Point_3;
  typedef typename BGT::Segment_3  Segment_3;
  typedef typename BGT::Ray_3      Ray_3;
  typedef typename BGT::Line_3     Line_3;
  typedef typename BGT::Vector_3   Vector_3;
  typedef typename BGT::Sphere_3   Sphere_3;
  typedef CGAL::Bbox_3             Bbox_3;

protected:
  typedef typename BGT::Iso_cuboid_3 Iso_cuboid_3;

public:
  // Kernel_traits compatibility
  typedef BGT R;
  // access Function type from inherited class
//  typedef Function Fct;

  //-------------------------------------------------------
  // Index Types
  //-------------------------------------------------------
  /// Type of indexes for cells of the input complex
  typedef typename Base::Fct::return_type Subdomain_index;
  typedef boost::optional<Subdomain_index> Subdomain;
  /// Type of indexes for surface patch of the input complex
  typedef std::pair<Subdomain_index, Subdomain_index> Surface_patch_index;
  typedef boost::optional<Surface_patch_index> Surface_patch;
  /// Type of indexes to characterize the lowest dimensional face of the input
  /// complex on which a vertex lie
  typedef boost::variant<Subdomain_index, Surface_patch_index> Index;
  typedef CGAL::cpp11::tuple<Point_3,Index,int> Intersection;


//  typedef typename BGT::FT FT;
  typedef BGT Geom_traits;


//useless
  /**
   * Constructs  a set of \ccc{n} points on the surface, and output them to
   *  the output iterator \ccc{pts} whose value type is required to be
   *  \ccc{std::pair<Points_3, Index>}.
   */
  struct Construct_initial_points
  {
    Construct_initial_points(const Super_LIMD& domain)
      : r_domain_(domain) {}

/*
    template<class OutputIterator>
    OutputIterator operator()(OutputIterator pts, const int n = 12) const
    {}
*/
  private:
    const Super_LIMD& r_domain_;
  };
  /// Returns Construct_initial_points object
  Construct_initial_points construct_initial_points_object() const
  {
    assert(0);
    return Construct_initial_points(*this);
  }


  /**
   * Returns a bounding box of the domain
   */
 /* Bbox_3 bbox() const {
    return bbox_.bbox();
  }*/

//end useless


  /**
   * Returns true if point~\ccc{p} is in the domain. If \ccc{p} is in the
   *  domain, the parameter index is set to the index of the subdomain
   *  including $p$. It is set to the default value otherwise.
   */
  Subdomain get_subdomain_using_vessel(const Point_3& p) const
  {
    Subdomain vs_sub = vs.get_subdomain(p);
    return vs_sub;
//    if (vs_sub) return vs_sub;
//    Subdomain cleaned_sub = cleaned_domain.is_in_domain_object()(p);
//    return cleaned_sub;
  }
  Subdomain get_cleaned_subd(const Point_3& p) const
  {
    return cleaned_domain.is_in_domain_object()(p);
  }

  struct Is_in_domain
  : public Base::Is_in_domain
  {
    Is_in_domain(const Super_LIMD& domain)
    : Base::Is_in_domain(domain), r_domain_(domain)
    {}

    Subdomain operator()(const Point_3& p) const
    {
      Subdomain index = r_domain_.get_subdomain_using_vessel(p);
      if (index)
        return index;
      index = Base::Is_in_domain::operator()(p);
      if (index && index == 5)
        index = r_domain_.get_cleaned_subd(p);
      return index;
//        return Base::Is_in_domain::operator()(p);


      // null(f(p)) means p is outside the domain
     // Subdomain_index index = (r_domain_.function_)(p);
     // if ( r_domain_.null(index) )
       // return Subdomain();
      //else
       // return Subdomain(index);
    }
  private:
    const Super_LIMD& r_domain_;
  };

  /// Returns Is_in_domain object
  Is_in_domain is_in_domain_object() const { return Is_in_domain(*this); }



//useless!!
  /**
   * Returns true is the element \ccc{type} intersect properly any of the
   * surface patches describing the either the domain boundary or some
   * subdomain boundary.
   * \ccc{Type} is either \ccc{Segment_3}, \ccc{Ray_3} or \ccc{Line_3}.
   * Parameter index is set to the index of the intersected surface patch
   * if \ccc{true} is returned and to the default \ccc{Surface_patch_index}
   * value otherwise.
   */
  struct Do_intersect_surface
  : public Base::Do_intersect_surface
  {//useless
    Do_intersect_surface(const Super_LIMD& domain)
        : Base::Do_intersect_surface(domain), r_domain_(domain) {
std::cout << "CONST" << std::endl;
    }

    template<typename Object>
    Surface_patch operator()(const Object& q) const
    {
        std::cout <<  "ERR" << std::endl;
        return Surface_patch();
        //todo!!
        //q can intersect vessel
        //q can be fully inside vessel
        //q can be fully outside vessel
    }
    
    Surface_patch operator()(const Segment_3& s) const
    {
      std::cout << "F " << std::endl;
      return Surface_patch();
      //return this->operator()(s.source(), s.target());
    }
     /*
    Surface_patch operator()(const Ray_3& r) const
    {
      return clip_to_segment(r);
    }

    Surface_patch operator()(const Line_3& l) const
    {
      return clip_to_segment(l);
    }
    */
  private:
/*
    /// Returns true if points \c a & \c b do not belong to the same subdomain
    /// \c index is set to the surface index of subdomains f(a), f(b)
    Surface_patch operator()(const Point_3& a, const Point_3& b) const
    {
      // If f(a) != f(b), then [a,b] intersects some surface. Here we consider
      // [a,b] intersects surface_patch labelled <f(a),f(b)> (or <f(b),f(a)>).
      // It may be false, further rafinement will improve precision
      const Subdomain_index value_a = r_domain_.function_(a);
      const Subdomain_index value_b = r_domain_.function_(b);

      if ( value_a != value_b ) {
        if( r_domain_.null(value_a) && r_domain_.null(value_b) )
          return Surface_patch();
        else
          return Surface_patch(r_domain_.make_surface_index(value_a, value_b));
      }
      else
        return Surface_patch();
    }


    template<typename Query>
    Surface_patch clip_to_segment(const Query& query) const
    {
      typename cpp11::result_of<typename BGT::Intersect_3(Query, Iso_cuboid_3)>::type
        clipped = CGAL::intersection(query, r_domain_.bbox_);

      if(clipped)
#if CGAL_INTERSECTION_VERSION > 1
        if(const Segment_3* s = boost::get<Segment_3>(&*clipped))
          return this->operator()(*s);
#else
        if(const Segment_3* s = object_cast<Segment_3>(&clipped))
          return this->operator()(*s);
#endif

      return Surface_patch();
    }

*/
  private:
    const Super_LIMD& r_domain_;
  };

  /// Returns Do_intersect_surface object
  Do_intersect_surface do_intersect_surface_object() const
  {//useless
    std::cout << "EEEEEEEE" << std::endl;
    assert(0);
    return Do_intersect_surface(*this);
  }
//end useless


  /**
   * Returns a point in the intersection of the primitive \ccc{type}
   * with some boundary surface.
   * \ccc{Type1} is either \ccc{Segment_3}, \ccc{Ray_3} or \ccc{Line_3}.
   * The integer \ccc{dimension} is set to the dimension of the lowest
   * dimensional face in the input complex containing the returned point, and
   * \ccc{index} is set to the index to be stored at a mesh vertex lying
   * on this face.
   */


#if 1
  template<typename Object>
  bool get_intersection_from_vessel(Intersection &is, const Object & s) const
  {
    return vs.get_intersection(is,s);
  }

  struct Construct_intersection
  : public Base::Construct_intersection
  {
    Construct_intersection(const Super_LIMD& domain)
      : Base::Construct_intersection(domain), r_domain_(domain) {}

    template<typename Object>
    Intersection operator()(const Object& s) const
    {
      Intersection vessel_test;
      bool res = r_domain_.get_intersection_from_vessel(vessel_test, s);
      if (!res) {
        //return Base::Construct_intersection::operator()(s);
        Intersection inters = Base::Construct_intersection::operator()(s);
        //if it intersects vessel then return Intersection()
        Surface_patch_index *sip = boost::get<Surface_patch_index>(&(std::get<1>(inters)));
        if (sip && (sip->second == 5 || sip->first == 5))
          return Intersection();
        return inters;
      } else {
        //assert(0);//for testing
        return vessel_test;
      }
    }
  private:
    const Super_LIMD& r_domain_;
  };
#else
//this is CGAL slightly modified version which uses only is_in_domain calls
  struct Construct_intersection
  {
    Construct_intersection(const Super_LIMD& domain)
      : r_domain_(domain) {}

    Intersection operator()(const Segment_3& s) const
    {
      return this->operator()(s.source(),s.target());
    }

    Intersection operator()(const Ray_3& r) const
    {
      return clip_to_segment(r);
    }

    Intersection operator()(const Line_3& l) const
    {
      return clip_to_segment(l);
    }

  private:
    /**
     * Returns a point in the intersection of [a,b] with the surface
     * \c a must be the source point, and \c b the out point. It's important
     * because it drives bisection cuts.
     * Indeed, the returned point is the first intersection from \c [a,b]
     * with a subdomain surface.
     */
    Intersection operator()(const Point_3& a, const Point_3& b) const
    {
      // Functors
      typename BGT::Compute_squared_distance_3 squared_distance =
                                      BGT().compute_squared_distance_3_object();
      typename BGT::Construct_midpoint_3 midpoint =
                                      BGT().construct_midpoint_3_object();

      // Non const points
      Point_3 p1 = a;
      Point_3 p2 = b;
      Point_3 mid = midpoint(p1, p2);

      // Cannot be const: those values are modified below.
      Is_in_domain ind = r_domain_.is_in_domain_object();
      Subdomain vvalue_at_p1 = ind(p1);//r_domain_.function_(p1);
      Subdomain vvalue_at_p2 = ind(p2);//r_domain_.function_(p2);
      Subdomain vvalue_at_mid = ind(mid);//r_domain_.function_(mid,true);

      Subdomain_index value_at_p1 = (vvalue_at_p1 ? *vvalue_at_p1 : 0);
      Subdomain_index value_at_p2 = (vvalue_at_p2 ? *vvalue_at_p2 : 0);
      Subdomain_index value_at_mid = (vvalue_at_mid ? *vvalue_at_mid : 0);

      // If both extremities are in the same subdomain,
      // there is no intersection.
      // This should not happen...
      if( value_at_p1 == value_at_p2 )
      {
        return Intersection();
      }
      if( r_domain_.null(value_at_p1) && r_domain_.null(value_at_p2) ) {
        return Intersection();
      }

      // Else lets find a point (by bisection)
      // Bisection ends when the point is near than error bound from surface
      while(true)
      {
        // If the two points are enough close, then we return midpoint
        if ( squared_distance(p1, p2) < r_domain_.squared_error_bound_ )
        {
          CGAL_assertion(value_at_p1 != value_at_p2 &&
             ! ( r_domain_.null(value_at_p1) && r_domain_.null(value_at_p2) ) );
          const Surface_patch_index sp_index =
            r_domain_.make_surface_index(value_at_p1, value_at_p2);
          const Index index = r_domain_.index_from_surface_patch_index(sp_index);
          return Intersection(mid, index, 2);
        }

        // Else we must go on
        // Here we consider that p1(a) is the source point. Thus, we keep p1 and
        // change p2 if f(p1)!=f(p2).
        // That allows us to find the first intersection from a of [a,b] with
        // a surface.
        if ( value_at_p1 != value_at_mid &&
             ! ( r_domain_.null(value_at_p1) && r_domain_.null(value_at_mid) ) )
        {
          p2 = mid;
          value_at_p2 = value_at_mid;
        }
        else
        {
          p1 = mid;
          value_at_p1 = value_at_mid;
        }

        mid = midpoint(p1, p2);
        value_at_mid = ind(mid);//r_domain_.function_(mid,true);
      }
    }

    /// Clips \c query to a segment \c s, and call operator()(s)
    template<typename Query>
    Intersection clip_to_segment(const Query& query) const
    {
      typename cpp11::result_of<typename BGT::Intersect_3(Query, Iso_cuboid_3)>::type
        clipped = CGAL::intersection(query, r_domain_.bbox_);

      if(clipped)
#if CGAL_INTERSECTION_VERSION > 1
        if(const Segment_3* s = boost::get<Segment_3>(&*clipped))
          return this->operator()(*s);
#else
        if(const Segment_3* s = object_cast<Segment_3>(&clipped))
          return this->operator()(*s);
#endif
      
      return Intersection();
    }

  private:
    const Super_LIMD& r_domain_;
  };
#endif

  /// Returns Construct_intersection object
  Construct_intersection construct_intersection_object() const
  {
    return Construct_intersection(*this);
  }



///below are unmodified versions
///maybe they should be even removed


  /**
   * Returns the index to be stored in a vertex lying on the surface identified
   * by \c index.
   */
  Index index_from_surface_patch_index(const Surface_patch_index& index) const
  { return Index(index); }

  /**
   * Returns the index to be stored in a vertex lying in the subdomain
   * identified by \c index.
   */
  Index index_from_subdomain_index(const Subdomain_index& index) const
  { return Index(index); }

  /**
   * Returns the \c Surface_patch_index of the surface patch
   * where lies a vertex with dimension 2 and index \c index.
   */
  Surface_patch_index surface_patch_index(const Index& index) const
  { return boost::get<Surface_patch_index>(index); }

  /**
   * Returns the index of the subdomain containing a vertex
   *  with dimension 3 and index \c index.
   */
  Subdomain_index subdomain_index(const Index& index) const
  { return boost::get<Subdomain_index>(index); }
  
  // -----------------------------------
  // Backward Compatibility
  // -----------------------------------
#ifndef CGAL_MESH_3_NO_DEPRECATED_SURFACE_INDEX
  typedef Surface_patch_index   Surface_index;
  
  Index index_from_surface_index(const Surface_index& index) const
  { return index_from_surface_patch_index(index); }
  
  Surface_index surface_index(const Index& index) const
  { return surface_patch_index(index); }
#endif // CGAL_MESH_3_NO_DEPRECATED_SURFACE_INDEX
  // -----------------------------------
  // End backward Compatibility
  // -----------------------------------

private:
  /// Returns Surface_patch_index from \c i and \c j
  Surface_patch_index make_surface_index(const Subdomain_index i,
                                   const Subdomain_index j) const
  {
    if ( i < j ) return Surface_patch_index(i,j);
    else return Surface_patch_index(j,i);
  }
};// end class Labeled_mesh_domain_3

}  // end namespace CGAL
