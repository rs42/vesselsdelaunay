#pragma once
#include <iostream>

#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkXMLPolyDataReader.h>

#include <vtkCell.h>
#include <vtkCellData.h>
#include <vtkDataSet.h>
#include <vtkDataSetAttributes.h>
#include <vtkProperty.h>
#include <vtkSmartPointer.h>
#include <vtkTubeFilter.h>

#include <vtkDataSetMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCamera.h>
#include <vtkInteractorStyleTrackballCamera.h>

//#include <vtkMath.h>
//#include <vtkDecimatePolylineFilter.h>
//#include <vtkCleanPolyData.h>

void render(vtkSmartPointer<vtkPolyData> polyData)
{
/*
  // no effect
  // Clean the polydata. This will remove duplicate points that may be
  // present in the input data.
  vtkSmartPointer<vtkCleanPolyData> cleaner =
    vtkSmartPointer<vtkCleanPolyData>::New();
  cleaner->SetInputConnection (reader->GetOutputPort());
  cleaner->Update();
*/

 polyData->GetPointData()->SetActiveScalars("Radius");
 std::cout << polyData->GetNumberOfVerts() << " " << polyData->GetNumberOfLines()
  << " " <<  polyData->GetNumberOfPolys() << std::endl; 
 

  vtkSmartPointer<vtkTubeFilter> tube
    = vtkSmartPointer<vtkTubeFilter>::New();
  tube->SetInputData(polyData);
  tube->SetNumberOfSides(6);
  tube->SidesShareVerticesOn();
  tube->CappingOn();
  tube->SetVaryRadiusToVaryRadiusByAbsoluteScalar();//correct one!
//  tube->SetVaryRadius(1);
//  tube->SetVaryRadiusToVaryRadiusByScalar();


//rendering
  vtkSmartPointer<vtkPolyDataMapper> mapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(tube->GetOutputPort());
  mapper->ScalarVisibilityOn();
//  mapper->SetScalarModeToUsePointFieldData();
//  mapper->SelectColorArray("Colors");

  vtkSmartPointer<vtkActor> actor =
    vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);

  actor->GetProperty()->SetEdgeColor(1.0, 0.0, 0.0); //(R,G,B)
  actor->GetProperty()->EdgeVisibilityOn();


  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor(actor);
  renderer->SetBackground(.2, .3, .4);

  // Make an oblique view
  renderer->GetActiveCamera()->Azimuth(30);
  renderer->GetActiveCamera()->Elevation(30);
  renderer->ResetCamera();

  vtkSmartPointer<vtkRenderWindow> renWin =
    vtkSmartPointer<vtkRenderWindow>::New();
  vtkSmartPointer<vtkRenderWindowInteractor>
    iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();

  iren->SetRenderWindow(renWin);
  renWin->AddRenderer(renderer);
  renWin->SetSize(500, 500);
  renWin->Render();

  vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
    vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
  iren->SetInteractorStyle(style);

  iren->Start();
}
