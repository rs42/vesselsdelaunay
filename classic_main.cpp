//#define STOP_RIGHT_AFTER_SCANNING_TETS
//#define CGAL_MESH_3_VERBOSE
//these are cgal's macros
#define CLLASSICC_MESHS
//you must define this to enable default (classical) cgal algorithm
#define NOT_PREC_ARITHM
//please enable it, there's no need to use an exact computation mode


#include <iostream>
#undef NDEBUG//comment it to disable assert checks
#include <assert.h>

#include <vector>
#include <list>

#include "geometry.h"
//#include "use_tetgen.h"

//#include "write_c3t3_to_vtk_xml_file.h"

#include "IO/Complex_3_in_triangulation_3_to_vtk.h"
//this shit is for exporting to vtu file
#include <vtk/vtkVersion.h>
#include <vtk/vtkSmartPointer.h>
//#include <vtk/vtkTetra.h>
//#include <vtk/vtkCellArray.h>
//#include <vtk/vtkXMLUnstructuredGridReader.h>
//#include <vtk/vtkDataSetMapper.h>
//#include <vtk/vtkActor.h>
//#include <vtk/vtkRenderer.h>
//#include <vtk/vtkRenderWindow.h>
//#include <vtk/vtkRenderWindowInteractor.h>
#include <vtk/vtkXMLUnstructuredGridWriter.h>
#include <vtk/vtkUnstructuredGrid.h>
//#include <vtk/vtkPointData.h>
//#include <vtk/vtkVertexGlyphFilter.h>

#include "fixed_danilov_IO.h"

#include <CGAL/exude_mesh_3.h>
#include <CGAL/Mesh_3/initialize_triangulation_from_labeled_image.h>
#include "histogram.h"

/*
void export2vtu(const C3t3 &c3t3, const char* fname)
{
  //this version use vtk library but do not preserve colors yet
  //New output to vtu
  vtkUnstructuredGrid* xxx = output_c3t3_to_vtk_unstructured_grid(c3t3);
   // Write file
  vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer =
    vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
  writer->SetFileName(fname);
#if VTK_MAJOR_VERSION <= 5
  writer->SetInput(xxx);
#else
  writer->SetInputData(xxx);
#endif
  writer->Write();

}

void export2vtu_ver2(const C3t3 &c3t3, const char* fname)
{
  //this version preserve subdomain colors
;//  write_c3t3_to_vtk_xml_file(c3t3, fname);
}

void classic_meshgen(const char* fname)
{
  //this version opens only "*.inr.gz";
  CGAL::Image_3 image;
  if (!image.read(fname)) {
    std::cerr << "Error: Cannot read file " <<  fname << std::endl;
  }
  // Domain
  Mesh_domain domain(image);
  // Mesh criteria
  Mesh_criteria criteria(facet_angle=30, facet_size=6, facet_distance=4,
                         cell_radius_edge_ratio=3, cell_size=8);
  C3t3 c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria, no_features(), no_lloyd(), no_odt(), no_perturb(), no_exude());
  // Output
  export2vtu_ver2(c3t3, "out_classic.vtu");
  //export2vtu(c3t3, "out_classic.vtu");
//  std::ofstream medit_file("out_classic.mesh");
//  c3t3.output_to_medit(medit_file);
}*/

typedef CGAL::Mesh_constant_domain_field_3<Mesh_domain::R,
                                           Mesh_domain::Index> Sizing_field;

typedef Mesh_domain::Index Index;
typedef Mesh_domain::Surface_patch_index SPIndex;

void classic_meshgen_danilov_io(const char* fname, bool custom_init, double sz, double ves_distance, double f_distance)
{
  //fname - file with voxel image
  //custom_init - set 1 for better mesh initialization
  //sz - size of elements in mesh
  //ves_distance - upper bound for representation of vessel facets
  //f_distance - upper bound for representation of facets in general 
  

  //this version opens only nifti files
  Danilov_reader_n_exporter dre; 
  CGAL::Image_3 image = dre.nifti_reader(fname);
  // Domain
  Mesh_domain domain(image, 1e-4);
  // Mesh criteria
  Mesh_criteria criteria;

  Sizing_field facet_distance_f(f_distance);
  for (int i = 0; i < 5; i++)
    facet_distance_f.set_size(ves_distance, 2, domain.index_from_surface_patch_index(SPIndex(i,5)));
 
  if (ves_distance) {
    // Mesh criteria
    criteria = Mesh_criteria(facet_angle=30, facet_size=sz, facet_distance=facet_distance_f,
                             cell_radius_edge_ratio=3, cell_size=sz);
  } else {
    std::cout << "dont use me for tests!" << std::endl;
    assert(0);//tmp
    criteria = Mesh_criteria(facet_angle=30, facet_size=sz, facet_distance=f_distance,
                             cell_radius_edge_ratio=3, cell_size=sz);
  }
//  Mesh_criteria criteria(facet_angle=1, facet_size=6, facet_distance=1,
  //                       cell_radius_edge_ratio=3, cell_size=6);

  C3t3 c3t3;
  if (custom_init) {
    initialize_triangulation_from_labeled_image(c3t3,
                                                domain,
                                                image,
                                                criteria,
                                                (unsigned char)0);

    CGAL::refine_mesh_3<C3t3>(c3t3, domain, criteria, no_lloyd(),
                              no_odt(), no_perturb(), no_exude());
  } else {
    assert(0);//tmp
    c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria, no_features(), no_lloyd(), no_odt(), no_perturb(), no_exude());
  }

  // Output
  // both of these preserve colors but files differ
  // export2vtu_ver2(c3t3, "out_classic_hehe.vtu");
  
  //mesh without any improvement
  dre.vtu_exporter(c3t3, "out_classic.vtu");

  //additional possible mesh improvement
  auto retvalue = CGAL::exude_mesh_3(c3t3);
  dre.vtu_exporter(c3t3, "out_classic_with_exuder.vtu");
  switch (retvalue) {

    case CGAL::BOUND_REACHED:
    std::cout << "Exuder returned BOUND_REACHED" << std::endl;
    break;

    case CGAL::TIME_LIMIT_REACHED:
    std::cout << "Exuder returned TIME_LIMIT_REACHED" << std::endl;
    break;

    case CGAL::CANT_IMPROVE_ANYMORE:
    std::cout << "Exuder returned CANT_IMPROVE_ANYMORE" << std::endl;
    break;

    default:
    std::cout << "Exuder return unknown status" << std::endl;

  }

  //you can check mesh quality
  export_q_factors("classic-quality-all.dat", c3t3);
  export_histfile("classic-dihedral-all.dat", c3t3);
  export_q_factors("classic-quality-ves.dat", c3t3, 5);
  export_histfile("classic-dihedral-ves.dat", c3t3, 5);
  std::cout << "complete" << std::endl;
}


/*
#include <CGAL/Mesh_3/search_for_connected_components_in_labeled_image.h>

template <typename Point>
struct Get_point
{
  const double vx, vy, vz;
  Get_point(const CGAL::Image_3* image)
    : vx(image->vx())
    , vy(image->vy())
    , vz(image->vz())
  {}
  Point operator()(const std::size_t i,
                   const std::size_t j,
                   const std::size_t k) const
  {
    return Point(double(i) * vx, double(j) * vy, double(k) * vz);
  }
};

void gen_initial_mesh(C3t3 & c3t3, Network & net, const Mesh_domain & domain,
                      const CGAL::Image_3 & image)
{
  
  typedef typename C3t3::Triangulation       Tr;
  typedef typename Tr::Weighted_point        Weighted_point;
  typedef typename Tr::Bare_point            Bare_point;
  typedef typename Tr::Segment               Segment_3;
  typedef typename Tr::Geom_traits::Vector_3 Vector_3;
  typedef typename Tr::Vertex_handle         Vertex_handle;
  typedef typename Tr::Cell_handle           Cell_handle;
  
  Tr& tr = c3t3.triangulation();
  typename Tr::Geom_traits::Construct_point_3 wp2p =
    tr.geom_traits().construct_point_3_object();
  typename Tr::Geom_traits::Construct_weighted_point_3 p2wp =
    tr.geom_traits().construct_weighted_point_3_object();
  const double max_v = (std::max)((std::max)(image.vx(),
                                             image.vy()),
                                  image.vz());
  typedef std::vector<std::pair<Bare_point, std::size_t> > Seeds;
  Seeds seeds;
  Get_point<Bare_point> get_point(&image);
  std::cout << "Searching for connected components..." << std::endl;
  CGAL::Identity<unsigned char> no_transformation;
  search_for_connected_components_in_labeled_image(image,
                                                   std::back_inserter(seeds),
                                                   CGAL::Emptyset_iterator(),
                                                   no_transformation,
                                                   get_point,
                                                   (unsigned char)0);
  std::cout << "  " << seeds.size() << " components were found." << std::endl;
  std::cout << "Construct initial points..." << std::endl;
  for(typename Seeds::const_iterator it = seeds.begin(), end = seeds.end();
      it != end; ++it)
  {
    const double radius = double(it->second + 1)* max_v;
    CGAL::Random_points_on_sphere_3<Bare_point> points_on_sphere_3(radius);
    typename Mesh_domain::Construct_intersection construct_intersection =
      domain.construct_intersection_object();
    std::vector<Vector_3> directions;
    if(it->second < 2) {
      // shoot in six directions
      directions.push_back(Vector_3(-radius, 0, 0));
      directions.push_back(Vector_3(+radius, 0, 0));
      directions.push_back(Vector_3(0, -radius, 0));
      directions.push_back(Vector_3(0, +radius, 0));
      directions.push_back(Vector_3(0, 0, -radius));
      directions.push_back(Vector_3(0, 0, +radius));
    } else {
      for(int i = 0; i < 20; ++i)
      {
        // shoot 20 random directions
        directions.push_back(*points_on_sphere_3++ - CGAL::ORIGIN);
      }
    }
    BOOST_FOREACH(const Vector_3& v, directions)
    {
      const Bare_point test = it->first + v;
      const typename Mesh_domain::Intersection intersect =
        construct_intersection(Segment_3(it->first, test));
      if (CGAL::cpp11::get<2>(intersect) != 0)
      {
        Weighted_point pi = p2wp(CGAL::cpp11::get<0>(intersect));
        std::cout << pi << std::endl;
//  typename Mesh_domain::Is_in_domain is_in_dom =
  //    domain.is_in_domain_object();

        // This would cause trouble to optimizers
        // check pi will not be hidden
        typename Tr::Locate_type lt;
        Cell_handle c;
        int li, lj;
        Cell_handle pi_cell = tr.locate(pi, lt, li, lj);
        if(lt != Tr::OUTSIDE_AFFINE_HULL) {
          switch (tr.dimension())
          { //skip dimension 0
          case 1:
            if (tr.side_of_power_segment(pi_cell, pi, true) != CGAL::ON_BOUNDED_SIDE)
              continue;
            break;
          case 2:
            if (tr.side_of_power_circle(pi_cell, 3, pi, true) != CGAL::ON_BOUNDED_SIDE)
              continue;
            break;
          case 3:
            if (tr.side_of_power_sphere(pi_cell, pi, true) != CGAL::ON_BOUNDED_SIDE)
              continue;
          }
        }
        //check pi is not inside a protecting ball
        std::vector<Vertex_handle> conflict_vertices;
        if (tr.dimension() == 3)
        {
          tr.vertices_on_conflict_zone_boundary(pi, pi_cell
            , std::back_inserter(conflict_vertices));
        }
        else
        {
          for (typename Tr::Finite_vertices_iterator vit = tr.finite_vertices_begin();
               vit != tr.finite_vertices_end(); ++vit)
          {
            if (vit->point().weight() > 0.)
              conflict_vertices.push_back(vit);
          }
        }
        bool pi_inside_protecting_sphere = false;
        BOOST_FOREACH(Vertex_handle cv, conflict_vertices)
        {
          if (cv->point().weight() == 0.)
            continue;
          if (CGAL::compare_squared_distance(pi.point(), wp2p(cv->point()), cv->point().weight())
              != CGAL::LARGER)
          {
            pi_inside_protecting_sphere = true;
            break;
          }
        }
        if (pi_inside_protecting_sphere)
          continue;
        const typename Mesh_domain::Index index = CGAL::cpp11::get<1>(intersect);
        Vertex_handle v = tr.insert(pi);
        // `v` could be null if `pi` is hidden by other vertices of `tr`.
        

        assert(domain.is_in_domain_object().operator()(pi.point()));
CGAL_assertion(v != Vertex_handle());
        c3t3.set_dimension(v, 2); // by construction, points are on surface
        c3t3.set_index(v, index);
      }
    }
  }
  std::cout << "  " << tr.number_of_vertices() << " initial points." << std::endl;
  if ( c3t3.triangulation().dimension() != 3 )
  {
    std::cout << "  not enough points: triangulation.dimension() == "
              << c3t3.triangulation().dimension() << std::endl;
  }  
}
*/

int main(int argc, char** argv)
{
  if (argc != 6) {
    std::cout << "Usage: " << argv[0] << " nifti-file " <<  " bool-flag-is_custom_init" << " size-bound"  << " precision-vessels-facets"  << " precision-another-facets" << std::endl;
    return 0;
  }
  classic_meshgen_danilov_io(argv[1], argv[2], atof(argv[3]), atof(argv[4]), atof(argv[5]));
  return 0;
}
